module Adicionales08 where
--import PilaConListas
import G04_p03_stack_e02_stack_usando_listas

{-
Ejercicios adicionales de Pilas
Roman Garcia
-}

-- Dada una lista, pone el contenido en una Pila. El primer elemento en la lista
-- deberia ser el primero en poder ser sacado de la pila
--Test: ponerEnPila [1,2,3,4] -> {><1,2,3,4}
ponerEnPila :: [a] -> Stack a
ponerEnPila [] = emptyS
ponerEnPila (x:xs) = push x (ponerEnPila xs)

-- Dada una pila, determinar la cantidad de elementos que tiene
--Test: tamanioPila (push 5 (push 3 (push 8 emptyS))) -> 3
tamanioPila :: Stack a -> Int
tamanioPila s =
  if (isEmptyS s)
    then 0
    else 1 + tamanioPila (pop s)

-- Dadas dos pilas, determinar si son iguales
--Test: pilasIguales emptyS emptyS -> True
--Test: pilasIguales (push 3 emptyS) (push 5 (push 3 emptyS)) -> False
--Test: pilasIguales (push 3 emptyS) (pop (push 5 (push 3 emptyS))) -> True
pilasIguales :: Eq a => Stack a -> Stack a -> Bool
pilasIguales s1 s2
  = ((isEmptyS s1) && (isEmptyS s2))
  || (((top s1) == (top s2)) && pilasIguales (pop s1) (pop s2))

-- Dado un item y una pila, determinar en que posicion está. Si está en el
-- tope devuelve 0, si es el segundo devuelve 1,.. Si no está devolver -1
--Test: posEnPila 2 emptyS -> -1
--Test: posEnPila 8 (push 5 (push 3 (push 8 emptyS))) -> 2
posEnPila :: Eq a => a -> Stack a -> Int
posEnPila e s = posEnPilaRec e s 0

posEnPilaRec :: Eq a => a -> Stack a -> Int -> Int
posEnPilaRec e s n =
  if isEmptyS s
    then -1
    else if e == top s
      then n
      else posEnPilaRec e (pop s) (n + 1)

-- Dada una secuencia de caracteres, donde una letra es un push y un asterisco es
-- un pop. Ejecutar la secuencia dada sobre un stack vacio y devolver el estado
-- del stack resultante
--Test: secuencia "AF*LU*C*OKKK***H" -> {><'H','O','L','A'}
secuencia :: String -> Stack Char
secuencia s = secuenciaRec s emptyS

secuenciaRec :: String -> Stack Char -> Stack Char
secuenciaRec [] s = s
secuenciaRec ('*':xs) s = secuenciaRec xs (pop s)
secuenciaRec (x:xs) s = secuenciaRec xs (push x s)

-- otra
secuencia' :: String -> Stack Char
secuencia' xs = secuenciaRec' (reverse xs)

secuenciaRec' :: String -> Stack Char
secuenciaRec' [] = emptyS
secuenciaRec' ('*':xs) = pop (secuenciaRec' xs)
secuenciaRec' (x:xs) = push x (secuenciaRec' xs)

-- Modificar el ejercicio de pilas de la practica 4 para verificar que un
-- string tenga balanceados los parentesis, corchetes y llaves
--Test: superBalanceado "{sfs((s()ds)3)}" -> True
--Test: superBalanceado "{sfs((s()ds)3[)]}" -> False
superBalanceado :: String -> Bool
superBalanceado cs = superBalanceadoRec cs emptyS

superBalanceadoRec :: String -> Stack Char -> Bool
superBalanceadoRec [] s = isEmptyS s
superBalanceadoRec (c:cs) s =
  if esAgrupadorAbierto c
    then superBalanceadoRec cs (push c s)
    else
      if esAgrupadorCerrado c
        then
          if (isEmptyS s)
            then False
            else
              if esCierre (top s) c
                then superBalanceadoRec cs (pop s)
                else False
        else superBalanceadoRec cs s

esAgrupadorAbierto :: Char -> Bool
esAgrupadorAbierto c = pertenece c ['(', '[', '{']

esAgrupadorCerrado :: Char -> Bool
esAgrupadorCerrado c = pertenece c [')', ']', '}']

pertenece :: Eq a => a -> [a] -> Bool
pertenece e [] = False
pertenece e (x:xs) =
  if e == x
    then True
    else pertenece e xs

esCierre :: Char -> Char -> Bool
esCierre '(' ')' = True
esCierre '[' ']' = True
esCierre '{' '}' = True
esCierre c1 c2 = False
