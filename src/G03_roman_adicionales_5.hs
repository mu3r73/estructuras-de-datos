module Adicionales05 where

{-
Ejercicios adicionales
Practica 1 + Practica 2 + Practica 3
Roman Garcia
-}

{-
Definición de arboles un poco tuneada  
-}
data Tree a
  = Empty
  | Node a
         (Tree a)
         (Tree a)
  deriving (Show)

ej1 :: Tree Int
ej1 = Node 1 Empty
            (Node 8 (Node 2 Empty Empty)
                    (Node 5 Empty Empty))
--                  1
--                      8
--                    2   5

ej2 :: Tree Int
ej2 = Node 2 Empty
            (Node 4 Empty
                    (Node 8 Empty Empty))
--                  2
--                      4
--                          8

----------------------------- EJERCICIOS ---------------------------

-- Dado un arbol de enteros, determinar la cantidad de numeros pares que tiene
-- Test: cantPares ej1 ---> 2
cantPares :: Tree Int -> Int
cantPares Empty = 0
cantPares (Node n ti td) =
  if esPar n
    then 1 + cantPares ti + cantPares td
    else cantPares ti + cantPares td

esPar :: Int -> Bool
esPar n = (n `mod` 2) == 0

-- Dado un arbol de enteros, devolver True si todos los elementos son pares
-- Test: todosPares ej1 -> False
-- Test: todosPares ej2 -> True
todosPares :: Tree Int -> Bool
todosPares Empty = True
todosPares (Node n ti td) = (esPar n) && (todosPares ti) && (todosPares td)

-- Dado un arbol que tiene lista de enteros, devolver un arbol de igual
-- estructuras donde cada nodo indica si la lista de entero tiene algun
-- numero par
mapTieneAlgunPar :: Tree [Int] -> Tree Bool
mapTieneAlgunPar Empty = Empty
mapTieneAlgunPar (Node n ti td) = Node (tieneAlgunPar n) (mapTieneAlgunPar ti) (mapTieneAlgunPar td)

tieneAlgunPar :: [Int] -> Bool
tieneAlgunPar [] = False
tieneAlgunPar (x:xs) = (esPar x) || (tieneAlgunPar xs)

-- Calcular la cantidad de subarboles derechos que tiene el arbol
-- Test: cantArbolesDer ej1 -> 2
cantArbolesDer :: Tree a -> Int
cantArbolesDer Empty = 0
cantArbolesDer (Node n ti td) =
  if not (esVacio td)
    then 1 + (cantArbolesDer ti) + (cantArbolesDer td)
    else (cantArbolesDer ti) + (cantArbolesDer td)

esVacio :: Tree a -> Bool
esVacio Empty = True
esVacio t = False

-- Calcular la cantidad de subarboles izquierdos que tiene el arbol
-- Test: cantArbolesIzq ej1 -> 1
cantArbolesIzq :: Tree a -> Int
cantArbolesIzq Empty = 0
cantArbolesIzq (Node n ti td) =
  if not (esVacio ti)
    then 1 + (cantArbolesIzq ti) + (cantArbolesIzq td)
    else (cantArbolesIzq ti) + (cantArbolesIzq td)

-- Un arbol es como una lista, si es una sucesion de todos arboles izquierdos
-- o todos arboles derechos. Determinar si un arbol es como una lista
-- Test: esComoUnaLista ej1 --> False
-- Test: esComoUnaLista ej2 --> True
esComoUnaLista :: Tree a -> Bool
esComoUnaLista t =
  (cantArbolesDer t) == 0
  || (cantArbolesIzq t) == 0

-- Dado un arbol, devolver el subarbol que contiene al elemento dado como raiz
-- Test subArbolCon 8 ej1 -> Node 8 (Node 2 Empty Empty) (Node 5 Empty Empty)
-- Test subArbolCon 2 ej1 -> Node 2 Empty Empty
-- Test subArbolCon 7 ej1 -> Empty
subArbolCon :: Eq a => a -> Tree a -> Tree a
subArbolCon e Empty = Empty
subArbolCon e (Node n ti td) =
  if n == e
    then Node n ti td
    else
      if not (esVacio (subArbolCon e ti))
        then subArbolCon e ti
        else subArbolCon e td

{-
Definamos un paso para recorrer un arbol, que puede ser Derecho o Izquierdo 
-}
data Paso
  = Izq
  | Der

-- Dada una lista de pasos, o sea elegir el arbol izquierdo o derecho, encontrar
-- el elemento del arbol indicado
-- Suponer que la lista es una secuencia valida de pasos
-- Test: buscarEnArbol [Der] ej --> 8
buscarEnArbol :: [Paso] -> Tree a -> a
buscarEnArbol [] (Node n ti td) = n
buscarEnArbol (Izq:xs) (Node n ti td) = buscarEnArbol xs ti
buscarEnArbol (Der:xs) (Node n ti td) = buscarEnArbol xs td
buscarEnArbol xs Empty = error "no se encontro el elemento"
