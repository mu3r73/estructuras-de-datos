module G05_p04_arboles_de_busqueda_5_1_BST_n_hi_hd_sin_balancear (
  Tree, insertBST, deleteBST, perteneceBST, splitMinBST, splitMaxBST,
  elMaximoMenorA, elMinimoMayorA,
  emptyBST, isEmptyBST, isBST, bstFromList, prettyprint
) where

import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe

-- arbol binario
data Tree a
  = EmptyT
  | NodeT a
          (Tree a)
          (Tree a)
  deriving (Show)

-- invariantes:
-- EmptyT es un BST
-- NodeT n ti td es un BST si:
-- * todo elemento de ti es menor que n
-- * todo elemento de td es mayor que n
-- * ti y td son BST

-- no es balanceado => no cumple con la condicion O(log n)

-- datos para tests
{-
nota: para arboles aleatorios, usar bstFromList (shuffle <lista>),
      ej: bstFromList (shuffle [1..6]), -- sin repetidos
      y asignar expresion resultante a una var
-}

tchar1 :: Tree Char
tchar1 = bstFromList (shuffle ['a', 'c', 'e', 'g', 'i', 'k', 'm'])

tint1 :: Tree Int
tint1 = bstFromList (shuffle [1..7])


-- dado un BST, inserta un elemento en el arbol
insertBST :: Ord a => a -> Tree a -> Tree a
insertBST e EmptyT = NodeT e EmptyT EmptyT
insertBST e (NodeT n ti td) =
  if e <= n -- permite repetidos, se insertan a la izquierda
    then NodeT n (insertBST e ti) td
    else NodeT n ti (insertBST e td)


-- dado un BST, borra un elemento del arbol
deleteBST :: Ord a => a -> Tree a -> Tree a
deleteBST e EmptyT = EmptyT
deleteBST e (NodeT n ti td) =
  if e == n
    then
      rearmarBST ti td
    else
      if e < n
        then NodeT n (deleteBST e ti) td
        else NodeT n ti (deleteBST e td)

-- rearma el BST a partir de sus HI, HD anteriores
rearmarBST :: Ord a => Tree a -> Tree a -> Tree a
rearmarBST EmptyT td = td
rearmarBST ti EmptyT = ti
rearmarBST ti td = rearmarBSTmaxHI ti td

-- promueve maximo del subarbol izquierdo, usando splitMaxBST
rearmarBSTmaxHI :: Ord a => Tree a -> Tree a -> Tree a
rearmarBSTmaxHI ti td = NodeT m ti_sin_m td
  where (m, ti_sin_m) = splitMaxBST ti

-- promueve minimo del subarbol derecho, usando splitMinBST
rearmarBSTminHD :: Ord a => Tree a -> Tree a -> Tree a
rearmarBSTminHD ti td = NodeT m ti td_sin_m
  where (m, td_sin_m) = splitMinBST td


-- dado un BST, dice si el elemento pertenece o no al arbol
perteneceBST :: Ord a => a -> Tree a -> Bool
perteneceBST e EmptyT = False
perteneceBST e (NodeT n ti td) =
  if e == n
    then True
    else
      if e < n
        then perteneceBST e ti
        else perteneceBST e td


-- dado un BST, devuelve un par con el minimo elemento
-- y el arbol sin el mismo

-- sin fn aux, con where
splitMinBST :: Ord a => Tree a -> (a, Tree a)
splitMinBST EmptyT = error "arbol vacio"
splitMinBST (NodeT n EmptyT td) = (n, td)
splitMinBST (NodeT n ti td) = (m, NodeT n ti_sin_m td)
  where (m, ti_sin_m) = splitMinBST ti

-- con fn aux
splitMinBST' :: Ord a => Tree a -> (a, Tree a)
splitMinBST' t = (minBST t, sinMinBST t)

minBST :: Ord a => Tree a -> a
minBST EmptyT = error "arbol vacio"
minBST (NodeT n EmptyT td) = n
minBST (NodeT n ti td) = minBST ti

sinMinBST :: Ord a => Tree a -> Tree a
sinMinBST EmptyT = error "arbol vacio"
sinMinBST (NodeT n EmptyT td) = td
sinMinBST (NodeT n ti td) = NodeT n (sinMinBST ti) td

-- sin fn aux
splitMinBST'' :: Ord a => Tree a -> (a, Tree a)
splitMinBST'' EmptyT = error "arbol vacio"
splitMinBST'' (NodeT n EmptyT td) = (n, td)
splitMinBST'' (NodeT n ti td) =
  (fst (splitMinBST'' ti), NodeT n (snd (splitMinBST'' ti)) td)


-- dado un BST, devuelve un par con el maximo elemento
-- y el arbol sin el mismo

-- sin fn aux, con where
splitMaxBST :: Ord a => Tree a -> (a, Tree a)
splitMaxBST EmptyT = error "arbol vacio"
splitMaxBST (NodeT n ti EmptyT) = (n, ti)
splitMaxBST (NodeT n ti td) = (m, NodeT n ti td_sin_m)
  where (m, td_sin_m) = splitMaxBST td

-- con fn aux
splitMaxBST' :: Ord a => Tree a -> (a, Tree a)
splitMaxBST' t = (maxBST t, sinMaxBST t)

maxBST :: Ord a => Tree a -> a
maxBST EmptyT = error "arbol vacio"
maxBST (NodeT n ti EmptyT) = n
maxBST (NodeT n ti td) = maxBST td

sinMaxBST :: Ord a => Tree a -> Tree a
sinMaxBST EmptyT = error "arbol vacio"
sinMaxBST (NodeT n ti EmptyT) = ti
sinMaxBST (NodeT n ti td) = NodeT n ti (sinMaxBST td)

-- sin fn aux
splitMaxBST'' :: Ord a => Tree a -> (a, Tree a)
splitMaxBST'' EmptyT = error "arbol vacio"
splitMaxBST'' (NodeT n ti EmptyT) = (n, ti)
splitMaxBST'' (NodeT n ti td) =
  (fst (splitMaxBST'' td), NodeT n ti (snd (splitMaxBST'' td)))


-- dado un BST y un elemento, devuelve el maximo elemento
-- que sea menor al elemento dado
elMaximoMenorA :: Ord a => a -> Tree a -> a
elMaximoMenorA e t = fromJust (maxMenorQue e t Nothing)

maxMenorQue :: Ord a => a -> Tree a -> Maybe a -> Maybe a
maxMenorQue e EmptyT x0 = x0
maxMenorQue e (NodeT n ti td) x0 = 
  if e <= n
    then maxMenorQue e ti x0
    else maxMenorQue e td (Just n)

fromJust :: Maybe a -> a
fromJust (Just e) = e


-- dado un BST y un elemento, devuelve el minimo elemento
-- que sea mayor al elemento dado
elMinimoMayorA :: Ord a => a -> Tree a -> a
elMinimoMayorA e t = fromJust (minMayorQue e t Nothing)

minMayorQue :: Ord a => a -> Tree a -> Maybe a -> Maybe a
minMayorQue e EmptyT x0 = x0
minMayorQue e (NodeT n ti td) x0 =
  if e >= n
    then minMayorQue e td x0
    else minMayorQue e ti (Just n)


-- mias

-- retorna un arbol vacio
emptyBST :: Tree a
emptyBST = EmptyT

-- indica si el arbol es vacio
isEmptyBST :: Tree a -> Bool
isEmptyBST EmptyT = True
isEmptyBST t = False

-- indica si el arbol es un BST
isBST :: Ord a => Tree a -> Bool
isBST EmptyT = True
isBST (NodeT n ti td) =
  (isBST ti)
  && (isBST td)
  && (todosMenoresBST ti n)
  && (todosMayoresBST td n)

todosMenoresBST :: Ord a => Tree a -> a -> Bool
todosMenoresBST EmptyT n = True
todosMenoresBST t n = (maxBST t) < n

todosMayoresBST :: Ord a => Tree a -> a -> Bool
todosMayoresBST EmptyT n = True
todosMayoresBST t n = (minBST t) > n

-- genera un BST a partir de una lista
bstFromList :: Ord a => [a] -> Tree a
bstFromList = foldr insertBST EmptyT

-- hace shuffle de una lista
shuffle :: [a] -> [a]
shuffle xs = list2shuffle (genHorT (length xs)) xs []

list2shuffle :: [Char] -> [a] -> [a] -> [a]
list2shuffle ds [] ys = ys
list2shuffle (d:ds) (x:xs) ys =
  if d == 'h'
    then list2shuffle ds xs (x : ys)
    else list2shuffle ds xs (ys ++ [x])

genHorT :: Int -> [Char]
genHorT n = map int2ht (nums n)

int2ht :: Int -> Char
int2ht i =
  if i < 0
    then 'h'
    else 't'

-- genera N numeros aleatorios entre 0 y 9 (repeticiones permitidas)
genNums :: Int -> [Int]
genNums n = map (`mod` 10) (nums n)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]


-----------------------------------------------------
-- impresion de arboles
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Show a => Tree a -> IO ()
prettyprint t = putStrLn (unlines (prettyprint_helper t))
-- unlines concats a list with newlines

prettyprint_helper :: Show a => Tree a -> [String]
prettyprint_helper (EmptyT) = ["e"]
prettyprint_helper (NodeT e ti td)
  = (show e) : (prettyprint_subtree ti td)

prettyprint_subtree :: Show a => Tree a -> Tree a -> [String]
prettyprint_subtree ti td
  = ((pad "├──" "│  ") (prettyprint_helper td))
  ++ ((pad "╰──" "   ") (prettyprint_helper ti))

pad :: String -> String -> [String] -> [String]
pad first rest = zipWith (++) (first : repeat rest)
