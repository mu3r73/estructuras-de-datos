module Parcial_03_e02_multiset_v2_usando_lista_de_pares (
  MultiSet, emptyMS, addMS, occurMS, unionMS, interMS, toList, list2ms
) where

data MultiSet a
  = MS [(a, Int)]
  deriving (Show)

-- invariantes:
-- (e, n) en (MS ps) => n > 0; e no esta repetido en ps

{-
un MultiSet es un tipo abstracto de datos que consta de las siguientes
operaciones:
-}

-- crea un multiconjunto vacio
emptyMS :: MultiSet a
emptyMS = MS []

-- dados un elemento y un multiconjunto, agrega una copia del elemento
-- al multiconjunto
addMS :: Eq a => a -> MultiSet a -> MultiSet a
addMS e (MS ps) = MS (addLP e ps)

addLP :: Eq a => a -> [(a, Int)] -> [(a, Int)]
addLP e [] = [(e, 1)]
addLP e ((x, n) : ps) =
  if e == x
    then (x, n + 1) : ps
    else (x, n) : (addLP e ps)

-- dados un elemento y un multiconjunto, retorna la cantidad de copias
-- del elemento en el multiconjunto
occurMS :: Eq a => a -> MultiSet a -> Int
occurMS e (MS ps) = occurLP e ps

occurLP :: Eq a => a -> [(a, Int)] -> Int
occurLP e [] = 0
occurLP e ((x, n) : ps) =
  if e == x
    then n
    else occurLP e ps

-- dados dos multiconjuntos, devuelve un multiconjunto con todos los
-- elementos de ambos multiconjuntos
-- la cantidad de copias de cada elemento en el multiconjunto resultante
-- es la suma de la cantidad de copias en cada multiconjunto
unionMS :: Eq a => MultiSet a -> MultiSet a -> MultiSet a
unionMS (MS ps) ms = agregarTodos ps ms

agregarTodos :: Eq a => [(a, Int)] -> MultiSet a -> MultiSet a
agregarTodos [] ms = ms
agregarTodos ((x, n) : ps) ms = agregarTodos ps (agregarParMS (x, n) ms)

agregarParMS :: Eq a => (a, Int) -> MultiSet a -> MultiSet a
agregarParMS (x, n) (MS ps) = MS (agregarParLP (x, n) ps)

agregarParLP :: Eq a => (a, Int) -> [(a, Int)] -> [(a, Int)]
agregarParLP (e, n) [] = [(e, n)]
agregarParLP (e, n) ((x, m) : ps) =
  if e == x
    then (x, (n + m)) : ps
    else (x, m) : agregarParLP (e, n) ps

-- mejor, Viso
unionMS' :: Eq a => MultiSet a -> MultiSet a -> MultiSet a
unionMS' (MS ps) (MS qs) = MS (unionLP ps qs)

unionLP :: Eq a => [a, Int] -> [a, Int] -> [a, Int]
unionLP [] qs = qs
unionLP (p:ps) qs =
  if occurLP (fst p) qs > 0
    then (fst p, snd p + occurLP (fst p) qs) : (unionLP ps (sacarLP (fst p) qs))
    else unionLP ps qs

sacarLP :: Eq a => a -> [a, Int] -> [a, Int]
sacarLP e [] = []
sacarLP e (p:ps) =
  if (e == fst p)
    then ps
    else p : (sacarLP e ps)

-- dados dos multiconjuntos, devuelve el conjunto de elementos que ambos
-- multiconjuntos tienen en comun
-- la cantidad de copias de cada elemento en el multiconjunto resultante
-- es la minima entre la cantidad de copias en cada multiconjunto
interMS :: Eq a => MultiSet a -> MultiSet a -> MultiSet a
interMS (MS ps) ms = elemComunes ps ms

elemComunes :: Eq a => [(a, Int)] -> MultiSet a -> MultiSet a
elemComunes [] ms = emptyMS
elemComunes ((e, n) : ps) ms =
  if perteneceMS e ms
    then agregarNuevoParMS (e, minimo n (occurMS e ms)) (elemComunes ps ms)
    else elemComunes ps ms

perteneceMS :: Eq a => a -> MultiSet a -> Bool
perteneceMS e (MS ps) = perteneceLP e ps

perteneceLP :: Eq a => a -> [(a, Int)] -> Bool
perteneceLP e [] = False
perteneceLP e ((x, n) : ps) =
  if e == x
    then True
    else perteneceLP e ps

-- mejor, Viso
interMS' :: Eq a => MultiSet a -> MultiSet a -> MultiSet a
interMS' (MS ps) (MS qs) = MS (interLP ps qs)

interLP :: Eq a => [a, Int] -> [a, Int] -> [a, Int]
interLP [] qs = []
interLP (p:ps) qs =
  if occurLP (fst p) qs > 0
    then (fst p, min (snd p) (occurLP (fst p) qs)) : (interLP ps (sacarLP (fst p) qs))
    else interLP ps qs
-- en este caso, sacarLP no es ne'sario => el then de interLP podria ser:
--    then (fst p, min (snd p) (occurLP (fst p) qs)) : (interLP ps qs)

-- diferencia con agregarParMS:
-- en este caso, sabemos que e no aparece en ps
agregarNuevoParMS :: Eq a => (a, Int) -> MultiSet a -> MultiSet a
agregarNuevoParMS (e, n) (MS ps) = MS ((e, n) : ps)

minimo :: Ord a => a -> a -> a
minimo n m =
  if n < m
    then n
    else m

-- dado un multiconjunto, devuelve una lista con todos los elementos del
-- multiconjunto, respetando la cantidad de copias de cada uno
toList :: MultiSet a -> [a]
toList (MS ps) = expandirLP ps

expandirLP :: [(a, Int)] -> [a]
expandirLP [] = []
expandirLP ((e, n) : ps) = (expandirPar (e, n)) ++ (expandirLP ps)

expandirPar :: (a, Int) -> [a]
expandirPar (e, 0) = []
expandirPar (e, n) = e : expandirPar (e, n - 1)

-- mias

list2ms :: Eq a => [a] -> MultiSet a
list2ms [] = emptyMS
list2ms (x:xs) = addMS x (list2ms xs)
