{-
Para los que quieran hacer algunos ejercios adicionales de Estructuras de Datos
PRIMEROS PASOS EN HASKELL - Recursion y listas
Roman Garcia

Funciones que se pueden usar:

mod    :: Int -> Int -> Int      Devuelve el resto de dividir un numero por otro
length :: [a] -> Int             Devuelve la cantidad de elementos de una lista
&&     :: Bool -> Bool -> Bool   "y" logico, como el "and" de Wollok (es infijo) 
reverse:: [a] -> [a]             Devuelve la lista ordenada al reves
++     :: [a] -> [a] -> [a]      Concatena las dos listas (es infijo)

Nota1: Un operador es infijo cuando se escribe entre sus operandos: ejemplo: 1 + 2 
Nota2: String es sinonimo de lista de caracteres, o sea type String = [Char]

Definir las siguientes funciones teniendo en cuenta que:

- no es necesario usar recursion en todas
- pueden definir funciones auxiliares para descomponer el problema 
- pueden usar pattern matching, if-then-else y guardas, seg�n les parezca
- prueben las funciones con ejemplos simples

Nota3 : Sacar los -- para descomentar las signaturas de las funciones,
que estan comentadas para que no de errores el Haskell/WinHugs  
-}

{-
Toma un entero y dice si es o no par
-}
esPar :: Int -> Bool
esPar n = (n `mod` 2) == 0

{-
iff se comporta como el if-then-else: si la condicion (el primer parametro)
es verdadera devuelve su segundo argumento, sino el tercero
ejemplo: iff (3 >2) "Casa" "Auto"  deberia devolver "Casa"
-}
iff :: Bool -> a -> a -> a
iff True n m = n
iff False n m = m 

{-
Definir la funcion logica XOR, que es verdadera cuando sus argumentos son diferentes
No esta prohibido abrir la carpeta de Organizacion del a�o pasado
-}
xor :: Bool -> Bool -> Bool
xor b1 b2 = (b1 /= b2)

{-
Determina si un String es capicua, o sea que se lee igual de izquierda a derecha que de 
derecha a izquierda
-}
esCapicua :: String -> Bool  
esCapicua s = (s == (reverse s))

{-
Dada una lista con mas de dos elementos, devuelve los primeros dos
-}
primerosDos :: [a] -> (a, a)
primerosDos [] = error "lista vacia"
primerosDos (x:[]) = error "lista con 1 solo elemento"
primerosDos (x:(y:ys)) = (x, y)
 
{-
devuelve solamente los numeros pares de una lista dada
-}
soloPares :: [Int] -> [Int]
soloPares [] = []
soloPares (n:ns) =
  if esPar n
    then n : (soloPares ns)
    else soloPares ns

{-
devuelve True si hay mas numeros pares que impares en la lista dada
-}
hayMasParesQueImpares :: [Int] -> Bool
hayMasParesQueImpares xs = (length (soloPares xs)) > ((length xs) `div` 2)

{-
Dada una lista con mas de dos elementos, devuelve los ultimos dos.
No vale usar reverse
-}
ultimosDos :: [a] -> (a, a)
ultimosDos [] = error "lista vacia"
ultimosDos (x:[]) = error "lista con 1 solo elemento"
ultimosDos (x:(y:[])) = (x, y)
ultimosDos (x:xs) = ultimosDos xs

{-
Dado un elemento, otro elemento y una lista; la funcion reemplaza todas las ocurrencias
del primer elemento por el segundo
Asi: reemplazar 1 0 [1,4,1,6] devuelve [0,4,0,6]
-}
reemplazar :: Eq a => a -> a -> [a] -> [a]
reemplazar x y [] = []
reemplazar x y (e:es) =
  if e == x
    then y : (reemplazar x y es)
    else e : (reemplazar x y es)

{-
Inserta en la posicion dada un elemento en la lista. La primera posicion en cero.
Si la lista es m�s corta que la posici�n, que explote
por ejemplo: insertarEnPosicion 2 10 [1..10] debe dar [1,2,10,3,4,5,6,7,8,9,10]
-}
insertarEnPosicion :: Int -> a -> [a] -> [a]
insertarEnPosicion n e xs = insertarEnPosVoyPor n 0 e xs
  where
    insertarEnPosVoyPor n m e (x:xs) =
      if n == m
        then e : (x:xs)
        else x : (insertarEnPosVoyPor n (m + 1) e xs)

{-
Dada una lista de numeros, devuelve True si estan ordenados de menor a mayor. Las listas
vacias y con un solo elemento se consideran ordenadas
-}
estanOrdenados :: [Int] -> Bool
estanOrdenados [] = True
estanOrdenados (x:[]) = True
estanOrdenados (x:(y:ys)) = (x <= y) && (estanOrdenados ys)

{-
Dada una lista con elementos repetidos consecutivos, deja solo uno de ellos,
por ejemplo sacarRepetidos "Mississippi" da "Misisipi"
-}
sacarRepetidos :: Eq a => [a] -> [a]
sacarRepetidos [] = []
sacarRepetidos (x:[]) = [x]
sacarRepetidos (x:(y:ys)) =
  if x == y
    then sacarRepetidos (y:ys)
    else x : (sacarRepetidos (y:ys))

{-
Dado un numero y un elemento, devuelve una lista con n veces ese elemento
por ejemplo: repetir 5 True  devuelve [True,True,True,True,True,]
-}
repetir :: Int -> a -> [a]
repetir 0 e = []
repetir n e = e : (repetir (n - 1) e)
 
{-
Dado un texto comprimido como una lista pares (letra,cantidad), descomprimir el texto
por ejemplo descomprimir [('a',3),('c',2)] debe devolver "aaacc"
-}
descomprimir :: [(Char, Int)] -> String
descomprimir [] = []
descomprimir ((e,n):xs) = (repetir n e) ++ (descomprimir xs)

{-
Dadas dos listas ordenadas, devuelve una lista ordenada
por ejemplo, combinar [23,76,101] [15, 16, 80] devuelve [15,16,23,76,80,101]
-}
combinar :: [Int] -> [Int] -> [Int]
combinar xs [] = xs
combinar [] ys = ys
combinar (x:xs) (y:ys) =
  if x <= y
    then x : (combinar xs (y:ys))
    else y : (combinar (x:xs) ys)
