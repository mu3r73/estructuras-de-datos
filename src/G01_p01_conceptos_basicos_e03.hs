module G01_p01_conceptos_basicos_e03 (
  loMismo, siempreSiete, duplicar, singleton
) where

-- dado un elemento de algun tipo, devuelve ese mismo elemento
loMismo :: a -> a
loMismo x = x

-- dado un elemento de algun tipo, devuelve el numero 7
siempreSiete :: a -> Int
siempreSiete x = 7

-- dado un elemento de algun tipo, devuelve un par con ese elemento en ambas componentes
duplicar :: a -> (a, a)
duplicar x = (x, x)

-- dado un elemento de algun tipo, devuelve una lista con este unico elemento
singleton :: a -> [a]
singleton x = [x]
