import G05_p02_map_2_map_1_lista_de_pares_k_v_con_claves_repetidas

-- datos para tests

mNPersATel :: Map String Int
mNPersATel = assocM (assocM (assocM (emptyM) "pepe" 450000) "toto" 451111) "mimi" 452222

-- dada una lista de elementos, construye un Map que relaciona
-- cada elemento con su posicion en la lista
-- orden N si se permiten claves repetidas
-- orden N^2 si no se permiten claves repetidas
indexar :: [a] -> Map Int a
indexar xs = indexarLrec xs 1

indexarLrec :: [a] -> Int -> Map Int a
indexarLrec [] n = emptyM
indexarLrec (x:xs) n = assocM (indexarLrec xs n) n x

-- dados una lista de nombres de personas y un Map que relaciona
-- nombres con numeros de telefono, devuelve una lista con los
-- numeros de las personas de la lista, o Nothing en caso de
-- que no posean numero
-- orden N^2
pedirTelefonos :: [String] -> Map String Int -> [Maybe Int]
pedirTelefonos [] m = []
pedirTelefonos (n:ns) m = (lookupM m n) : (pedirTelefonos ns m)

-- dado un string, cuenta las ocurrencias de cada caracter utilizando
-- un Map
-- orden N^3 si se permiten claves repetidas
-- orden N^4 si no se permiten claves repetidas
ocurrencias :: String -> Map Char Int
ocurrencias s = ocurrenciasRec s emptyM

ocurrenciasRec :: String -> Map Char Int -> Map Char Int
ocurrenciasRec [] m = m
ocurrenciasRec (c:cs) m = 
  ocurrenciasRec cs (assocM (deleteM m c) c (incMB (lookupM m c)))

incMB :: Maybe Int -> Int
incMB Nothing = 1
incMB (Just n) = n + 1
