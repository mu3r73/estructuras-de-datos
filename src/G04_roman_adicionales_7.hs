module Adicionales07 where

--import ColaConListas
import G04_p02_queue_e02_queue_usando_listas
--import Practica02(Persona (..), nombre, edad)
import G02_p01_tipos_def_por_usr_e02_persona

{-
Ejercicios adicionales de Colas
Roman Garcia
-}

-- Vamos a tratar de modelar las colas del banco Provincia.
-- Para cada cliente, nos interesa saber cuanto tiempo
-- va a tardar su tramite, por lo que podemos reducir el modelo a:

data Cliente
  = C Persona -- Persona de la practica 2
      Tiempo
  deriving (Show)

type Tiempo = Int -- cantidad de minutos

type Cola = Queue Cliente


-- valores de ejemplo
pedro = C (Pers "Pedro" 19) 5
juan  = C (Pers "Juan" 34)  3
pablo = C (Pers "Pablo" 15) 8
fede  = C (Pers "Federico" 16) 10
luis  = C (Pers "Luis" 44)  5

q1 = queue juan (queue pedro (queue pablo (queue fede (queue luis emptyQ))))


-- Definir las funciones de acceso a Cliente
tiempo :: Cliente -> Tiempo
tiempo (C p t) = t

persona :: Cliente -> Persona
persona (C p t) = p

--fn de uso

-- Cuanto tiempo llevara atender la cola?
-- Test: tiempoAtencion q1 -> 31
tiempoAtencion :: Cola -> Int
tiempoAtencion q =
  if isEmptyQ q
    then 0
    else tiempo (firstQ q) + tiempoAtencion (dequeue q)

-- Devuelve los primeros n clientes de la cola en una lista.
-- Suponer que hay al menos n elementos en la cola
-- Test: primerosN 2 q1 == [luis, fede] -> True
primerosN :: Int -> Cola -> [Cliente]
primerosN 0 q = []
primerosN n q = (firstQ q) : (primerosN (n-1) (dequeue q))

-- Devuelve True si un cliente de cierto nombre esta en la Cola
-- Test: estaClienteLlamado "Juan" q1 -> True
estaClienteLlamado :: String -> Cola -> Bool
estaClienteLlamado n q =
  if isEmptyQ q
    then False
    else (nombre (persona (firstQ q)) == n)
          || estaClienteLlamado n (dequeue q)

-- Los primeros seran los ultimos: dar vuela la cola
darVueltaLaCola :: Cola -> Cola
darVueltaLaCola q =
  if isEmptyQ q
    then q
    else encolarTodos (recuperarClientes q)

recuperarClientes :: Cola -> [Cliente]
recuperarClientes q =
  if isEmptyQ q
    then []
    else (firstQ q) : recuperarClientes (dequeue q)

encolarTodos :: [a] -> Queue a
encolarTodos [] = emptyQ
encolarTodos (x:xs) = queue x (encolarTodos xs)

-- Cuantas personas se puede atender en n minutos?
-- Suponer que el tiempo que tarda en atenderse la cola
-- es mayor que el tiempo consultado
-- Test: cuantosPuedoAtenderEn 20 q1 -> 2
cuantosPuedoAtenderEn :: Tiempo -> Cola -> Int
cuantosPuedoAtenderEn t q = contarCuantosSePuedeAtender t q 0

contarCuantosSePuedeAtender :: Tiempo -> Cola -> Int -> Int
contarCuantosSePuedeAtender t q n =
  if t < tiempo (firstQ q)
    then n
    else contarCuantosSePuedeAtender (t - tiempo (firstQ q)) (dequeue q) (n + 1)


-- Ejercicios un poquito mas dificiles.
-- Se pueden usar funciones auxiliares

-- Cual es el cliente que menos tiempo lleva su tramite.
-- Suponer que al menos hay un cliente en la cola.
-- Sugerencia: usar una funcion auxiliar
-- Test: elDeTramiteMasCorto q1 == juan  -> True
elDeTramiteMasCorto :: Cola -> Cliente
elDeTramiteMasCorto q = 
  if isEmptyQ q
    then error "cola vacia"
    else clienteConTramiteMasCorto (dequeue q) (firstQ q)

clienteConTramiteMasCorto :: Cola -> Cliente -> Cliente
clienteConTramiteMasCorto q c =
  if isEmptyQ q
    then c
    else
      if tiempo (firstQ q) < tiempo c
        then clienteConTramiteMasCorto (dequeue q) (firstQ q)
        else clienteConTramiteMasCorto (dequeue q) c

-- Dada una cola, saca todos los menores de 18 años
-- Ojo!! la cola deberia quedar en el mismo orden que estaba
sacarMenores :: Cola -> Cola
sacarMenores q =
  if isEmptyQ q
    then q
    else
      if edad (persona (firstQ q)) < 18
        then sacarMenores (dequeue q)
        else queue (firstQ q) (sacarMenores (dequeue q))
