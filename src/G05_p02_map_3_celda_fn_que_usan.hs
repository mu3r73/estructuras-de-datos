import G05_p02_map_3_celda

-- devuelve True si hay mas de n bolitas de ese color
-- orden
nroBolitasMayorA :: Color -> Int -> Celda -> Bool
nroBolitasMayorA col n cel = (nroBolitas col cel) > n

-- agrega n bolitas de ese color a la celda
-- orden
ponerN :: Int -> Color -> Celda -> Celda
ponerN 0 col cel = cel
ponerN n col cel = ponerN (n - 1) col (poner col cel)

-- indica si existe al menos una bolita de cada color posible
-- orden
hayBolitasDeCadaColor :: Celda -> Bool
hayBolitasDeCadaColor cel =
  (hayBolitas Azul cel)
  && (hayBolitas Negro cel)
  && (hayBolitas Rojo cel)
  && (hayBolitas Verde cel)
