module Parcial_04_sentido (
  Sentido(Adelante, Atras)
) where

data Sentido
  = Adelante
  | Atras
  deriving (Eq, Show)
