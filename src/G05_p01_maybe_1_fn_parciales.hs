import G03_p01_arboles_binarios

-- datos para tests

tint1 :: Tree Int
tint1 = treeFromList agregarTal (shuffle [1..6])

tint2 :: Tree Int
tint2 = treeFromList agregarTal (shuffle [4..9])

-- dada una lista, quita su ultimo elemento
-- precondicion: la lista no es vacia
init' :: [a] -> [a]
init' [] = error "lista vacia"
init' (x:[]) = []
init' (x:xs) = x : (init' xs)

-- dada una lista, devuelve su ultimo elemento
-- precondicion: la lista no es vacia
last' :: [a] -> a
last' [] = error "lista vacia"
last' (x:[]) = x
last' (x:xs) = last' xs

-- dados un elemento y una lista, devuelve la posicion de la lista
-- en la que se encuentra dicho elemento
-- precondicion: el elemento se encuentra en la lista
indiceDe :: Eq a => a -> [a] -> Int
indiceDe e xs = posicionEnLista e xs 1

-- precondicion: el elemento se encuentra en la lista
posicionEnLista :: Eq a => a -> [a] -> Int -> Int
posicionEnLista e (x:xs) n =
  if (e == x)
    then n
    else posicionEnLista e xs (n + 1)

-- dadas una lista de pares (clave, valor) y una clave, devuelve
-- el valor asociado a la clave
-- precondicion: la clave se encuentra en la lista
valorParaClave :: Eq k => [(k, v)] -> k -> v
valorParaClave ((k, v) : ps) k0 =
  if (k == k0)
    then v
    else valorParaClave ps k0

-- dada una lista de elementos, devuelve el maximo
-- precondicion: la lista no es vacia
maximum' :: Ord a => [a] -> a
maximum' (x:[]) = x
maximum' (x:xs) = max' x (maximum' xs)

max' :: Ord a => a -> a -> a
max' x y =
  if x > y
    then x
    else y

-- dado un arbol, devuelve su elemento minimo
minT :: Ord a => Tree a -> a
minT (NodeT EmptyT e EmptyT) = e -- hoja
minT (NodeT ti e td) = min' e (min' (minT ti) (minT td))

min' :: Ord a => a -> a -> a
min' x y =
  if x < y
    then x
    else y

-- dada una lista de maybes, retorna los valores de los elementos
-- que sean Just,
-- pero si se encuentra un Nothing, el valor resultante es Nothing
fromJusts :: [Maybe a] -> Maybe [a] 
fromJusts ms =
  if hasNothings ms
    then Nothing
    else Just (fromJustL ms)

hasNothings :: [Maybe a] -> Bool
hasNothings [] = False
hasNothings (Nothing : ms) = True
hasNothings (m:ms) = hasNothings ms

-- precondicion: la lista no contiene Nothing
fromJustL :: [Maybe a] -> [a]
fromJustL [] = []
fromJustL (Nothing : ms) = error "lista contiene Nothing"
fromJustL ((Just x) : ms) = x : (fromJustL ms)
