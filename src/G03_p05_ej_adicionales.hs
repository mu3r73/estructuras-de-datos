import G02_p01_tipos_def_por_usr_e03_pokemon
import G03_p01_arboles_binarios

-- datos para tests
p1 = Poke Agua 15
p2 = Poke Fuego 20
p3 = Poke Planta 18
p4 = Poke Fuego 14
p5 = Poke Agua 13
p6 = Poke Planta 17
p7 = Poke Planta 19
p8 = Poke Fuego 16
p9 = Poke Fuego 12

e1 = Entr "Red" [p1, p2, p3]
e2 = Entr "Blue" [p4, p5, p6]
e3 = Entr "Yellow" [p7, p8]
e4 = Entr "Ash" [p9]

tpoke = treeFromList agregarTbal [p1, p2, p3, p4, p5, p6, p7, p8, p9]
tentr = treeFromList agregarTbal [e1, e2, e3, e4]

t1 = treeFromList agregarTbal ['a' .. 'f']
t2 = treeFromList agregarTbal ['g' .. 'k']
t3 = treeFromList agregarTbal ['l' .. 'r']
t4 = treeFromList agregarTbal ['s' .. 'u']
t5 = treeFromList agregarTbal ['v' .. 'z']

lt = [t1, t2, t3, t4, t5]

-- dados un tipo de pokemon y un arbol de pokemones, devuelve
-- la cantidad de pokemones de ese tipo en el arbol
cantidadDePokemonesDe :: TipoDePokemon -> Tree Pokemon -> Int
cantidadDePokemonesDe t EmptyT = 0
cantidadDePokemonesDe t (NodeT t1 p t2) =
  if esDeTipo t p
    then 1 + (cantidadDePokemonesDe t t1) + (cantidadDePokemonesDe t t2)
    else (cantidadDePokemonesDe t t1) + (cantidadDePokemonesDe t t2)

-- dado un arbol de entrenadores pokemon, devuelve la lista
-- con todos los entrenadores considerados expertos
-- un entrenador es experto si posee al menos un pokemon de cada tipo
entrenadoresExpertos :: Tree Entrenador -> [Entrenador]
entrenadoresExpertos EmptyT = []
entrenadoresExpertos (NodeT t1 e t2) =
  if esExperto e
    then (e : (entrenadoresExpertos t1)) ++ (entrenadoresExpertos t2)
    else (entrenadoresExpertos t1) ++ (entrenadoresExpertos t2)

-- dada una listas de arboles, devuelve la lista
-- con todos los elementos de todos los arboles
-- no importa el orden de los elementos en la lista resultante
concatenarArboles :: [Tree a] -> [a]
concatenarArboles [] = []
concatenarArboles (t:ts) = (listPreOrder t) ++ (concatenarArboles ts)
