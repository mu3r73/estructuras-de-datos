-- tipo de dato Persona, como nombre y edad de la persona
module G02_p01_tipos_def_por_usr_e02_persona (
  Persona(Pers), Nombre, Edad, nombre, edad, crecer, cambioDeNombre,
  esMenorQueLaOtra, mayoresA, promedioEdad, elMasViejo
) where

import G01_p02_recursion_e01_sobre_listas

type Nombre = String

type Edad = Int

data Persona =
  Pers Nombre
       Edad
  deriving (Show)

-- datos para tests
p1 = Pers "Titi" 22
p2 = Pers "Pepe" 20
p3 = Pers "Toto" 24
p4 = Pers "Mimi" 26

-- devuelve el nombre de una persona
-- observador
nombre :: Persona -> String
nombre (Pers n e) = n

-- devuelve la edad de una persona
-- observador
edad :: Persona -> Int
edad (Pers n e) = e

-- dada una persona, la devuelve con su edad aumentada en 1
crecer :: Persona -> Persona
crecer p = Pers (nombre p) (1 + edad p)

crecer' :: Persona -> Persona
crecer' (Pers n e) = Pers n (e + 1)

-- dados un nombre y una persona, reemplaza el nombre de la persona
-- por este otro
cambioDeNombre :: String -> Persona -> Persona
cambioDeNombre n p = Pers n (edad p)

cambioDeNombre' :: String -> Persona -> Persona
cambioDeNombre' n (Pers n1 e1) = Pers n e1

-- dadas dos personas, indica si la primera es mas joven que la segunda
esMenorQueLaOtra :: Persona -> Persona -> Bool
esMenorQueLaOtra p1 p2 = (edad p1) < (edad p2)

esMenorQueLaOtra' :: Persona -> Persona -> Bool
esMenorQueLaOtra' (Pers n1 e1) (Pers n2 e2) = e1 < e2

-- dados una edad y una lista de personas, devuelve todas las personas
-- que son mayores a esa edad
mayoresA :: Int -> [Persona] -> [Persona]
mayoresA e [] = []
mayoresA e (p:ps) =
  if (edad p) > e
    then p : mayoresA e ps
    else mayoresA e ps
{-
mayoresA :: Int -> [Persona] -> [Persona]
mayoresA e ps = filter (esMayorA e) ps

esMayorA Int -> Persona -> Boolean
esMayorA e p = (edad p) > e
-}

-- dada una lista de personas, devuelve el promedio de edad
-- entre esas personas
-- la lista al menos posee una persona
promedioEdad :: [Persona] -> Int
promedioEdad ps = (sumarEdades ps) `div` (length ps)

sumarEdades :: [Persona] -> Int
sumarEdades [] = 0
sumarEdades (p:ps) = (edad p) + (sumarEdades ps)
{-
promedioEdad :: [Persona] -> Int
promedioEdad ps = (sum (map edad ps)) `div` (length ps)
-}

-- v de Viso
promedioEdadV :: [Persona] -> Int
promedioEdadV ps = (sumatoria (mapEdad ps)) `div` (length ps)

mapEdad :: [Persona] -> [Int]
mapEdad [] = []
mapEdad (p:ps) = (edad p) : (mapEdad ps)

-- dada una lista de personas, devuelve la persona mas vieja de la lista
-- la lista al menos posee una persona
elMasViejo :: [Persona] -> Persona
elMasViejo (p:[]) = p
elMasViejo (p:ps) = mayor p (elMasViejo ps)

mayor :: Persona -> Persona -> Persona
mayor p1 p2 =
  if esMenorQueLaOtra p1 p2
    then p2
    else p1

elMasViejo' :: [Persona] -> Persona
elMasViejo' [] = error "lista vacia"
elMasViejo' (p:[]) = p
elMasViejo' (p:(q:qs)) =
  if (edad p) > (edad q)
    then elMasViejo' (p:qs)
    else elMasViejo' (q:qs)
