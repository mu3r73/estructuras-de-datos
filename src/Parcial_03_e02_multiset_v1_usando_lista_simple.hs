module Parcial_03_e02_multiset_v1_usando_lista_simple (
  MultiSet, emptyMS, addMS, occurMS, unionMS, interMS, toList, list2ms
) where

data MultiSet a
  = MS [a]
  deriving (Show)

-- invariantes: no hay

{-
un MultiSet es un tipo abstracto de datos que consta de las siguientes
operaciones:
-}

-- crea un multiconjunto vacio
emptyMS :: MultiSet a
emptyMS = MS []

-- dados un elemento y un multiconjunto, agrega una copia del elemento
-- al multiconjunto
addMS :: Eq a => a -> MultiSet a -> MultiSet a
addMS e (MS xs) = MS (e:xs)

-- dados un elemento y un multiconjunto, retorna la cantidad de copias
-- del elemento en el multiconjunto
occurMS :: Eq a => a -> MultiSet a -> Int
occurMS e (MS xs) = contarOcurrencias e xs

contarOcurrencias :: Eq a => a -> [a] -> Int
contarOcurrencias e [] = 0
contarOcurrencias e (x:xs) =
  if e == x
    then 1 + (contarOcurrencias e xs)
    else contarOcurrencias e xs

-- dados dos multiconjuntos, devuelve un multiconjunto con todos los
-- elementos de ambos multiconjuntos
-- la cantidad de copias de cada elemento en el multiconjunto resultante
-- es la suma de la cantidad de copias en cada multiconjunto
unionMS :: Eq a => MultiSet a -> MultiSet a -> MultiSet a
unionMS (MS xs) ms = agregarTodos xs ms

agregarTodos :: Eq a => [a] -> MultiSet a -> MultiSet a
agregarTodos [] ms = ms
agregarTodos (x:xs) ms = agregarTodos xs (addMS x ms)

-- mejor, Viso
unionMS' :: Eq a => MultiSet a -> MultiSet a -> MultiSet a
unionMS' (M xs) (M ys) = MS (xs ++ ys)

-- dados dos multiconjuntos, devuelve el conjunto de elementos que ambos
-- multiconjuntos tienen en comun
-- la cantidad de copias de cada elemento en el multiconjunto resultante
-- es la minima entre la cantidad de copias en cada multiconjunto
interMS :: Eq a => MultiSet a -> MultiSet a -> MultiSet a
interMS (MS xs) (MS ys) = MS (elemComunesL xs ys)

elemComunesL :: Eq a => [a] -> [a] -> [a]
elemComunesL [] ys = []
elemComunesL (x:xs) ys =
  if perteneceL x ys
    then x : (elemComunesL xs (quitarElemL x ys))
    else elemComunesL xs ys

-- ver fn elem de haskell
perteneceL :: Eq a => a -> [a] -> Bool
perteneceL e [] = False
perteneceL e (x:xs) = (e == x) || (perteneceL e xs)

quitarElemL :: Eq a => a -> [a] -> [a]
quitarElemL e [] = error "nunca deberia pasar"
quitarElemL e (x:xs) =
  if (e == x)
    then xs
    else x : (quitarElemL e xs)

-- dado un multiconjunto, devuelve una lista con todos los elementos del
-- multiconjunto, respetando la cantidad de copias de cada uno
toList :: MultiSet a -> [a]
toList (MS ps) = ps

-- mias

list2ms :: Eq a => [a] -> MultiSet a
list2ms [] = emptyMS
list2ms (x:xs) = addMS x (list2ms xs)
