module G01_p02_recursion_e01_sobre_listas (
  sumatoria, longitud, promedio, mapSucesor, mapSumaPar, mapMaxDelPar,
  todoVerdad, algunaVerdad, pertenece, apariciones,
  filtrarMenoresA, filtrarElemento, mapLongitudes, longitudMayorA,
  intercalar, snoc, append, aplanar, reversa, zipMaximos, zipSort
) where

import G01_p01_conceptos_basicos_e01
import G01_p01_conceptos_basicos_e02

-- dada una lista de enteros, devuelve la suma de todos sus elementos
sumatoria :: [Int] -> Int
sumatoria [] = 0
sumatoria (x:xs) = x + sumatoria xs

-- dada una lista de elementos de algun tipo, devuelve el largo de esa lista,
-- es decir, la cantidad de elementos que posee
longitud :: [a] -> Int
longitud [] = 0
longitud (x:xs) = 1 + longitud xs

-- dada una lista de enteros, devuelve un numero que es el promedio
-- entre todos los elementos de la lista
-- ¿pudo resolverla utilizando recursion estructural?
promedio :: [Int] -> Int
promedio [] = error "lista vacia"
promedio xs = (sumatoria xs) `div` (longitud xs)

-- dada una lista de enteros, devuelve la lista de los sucesores de cada entero
mapSucesor :: [Int] -> [Int]
mapSucesor [] = []
mapSucesor (x:xs) = (sucesor x) : (mapSucesor xs)

-- dada una lista de pares de enteros, devuelve una nueva lista
-- en la que cada elemento es la suma de los elementos de cada par
mapSumaPar :: [(Int,Int)] -> [Int]
mapSumaPar [] = []
mapSumaPar (p:ps) = (sumaPar p) : (mapSumaPar ps)

-- dada una lista de pares, devuelve una nueva lista en la que
-- cada elemento es el mayor de las componentes de cada par
mapMaxDelPar :: [(Int,Int)] -> [Int]
mapMaxDelPar [] = []
mapMaxDelPar (p:ps) = (maxDelPar p) : (mapMaxDelPar ps)

-- dada una lista de booleanos, devuelve True si todos sus elementos son True
todoVerdad :: [Bool] -> Bool
todoVerdad [] = True
todoVerdad (x:xs) = x && (todoVerdad xs)

-- dada una lista de booleanos, devuelve True si alguno de sus elementos es True
algunaVerdad :: [Bool] -> Bool
algunaVerdad [] = False
algunaVerdad (x:xs) = x || (algunaVerdad xs)

-- dados un elemento e y una lista xs, devuelve True 
-- si existe un elemento en xs que sea igual a e
pertenece :: Eq a => a -> [a] -> Bool
pertenece e [] = False
pertenece e (x:xs) =
  if e == x
    then True
    else pertenece e xs

-- dados un elemento e y una lista xs, cuenta la cantidad de apariciones de e en xs
apariciones :: Eq a => a -> [a] -> Int
apariciones e [] = 0
apariciones e (x:xs) =
  if e == x
    then 1 + apariciones e xs
    else apariciones e xs

-- dados un numero n y una lista xs, devuelve todos los elementos de xs
-- que son menores a n
filtrarMenoresA :: Int -> [Int] -> [Int]
filtrarMenoresA n [] = []
filtrarMenoresA n (x:xs) =
  if x < n
    then x : (filtrarMenoresA n xs)
    else filtrarMenoresA n xs

-- dados un elemento y una lista, filtra (elimina) todas las ocurrencias 
-- de ese elemento en la lista
filtrarElemento :: Eq a => a -> [a] -> [a]
filtrarElemento n [] = []
filtrarElemento n (x:xs) =
  if x == n
    then filtrarElemento n xs
    else x : (filtrarElemento n xs)

-- dada una lista de listas, devuelve la lista de sus longitudes
-- aplique esta funcion a la lista de strings ["Estructuras", "de", "datos"]
-- y observe el resultado
mapLongitudes :: [[a]] -> [Int]
mapLongitudes [] = []
mapLongitudes (xs:xss) = (longitud xs) : (mapLongitudes xss)

-- dados un numero n y una lista de listas, devuelve la lista
-- de aquellas listas que tienen mas de n elementos.
longitudMayorA :: Int -> [[a]] -> [[a]]
longitudMayorA n [] = []
longitudMayorA n (xs:xss) =
  if (longitud xs) > n
    then xs : (longitudMayorA n xss)
    else longitudMayorA n xss

-- dado un elemento e y una lista xs, ubica a e entre medio de todos los elementos de xs
-- Ejemplo:
-- intercalar ',' "abcde" == "a,b,c,d,e"
intercalar :: a -> [a] -> [a]
intercalar e [] = []
intercalar e (x:[]) = [x]
intercalar e (x:xs) = x : e : (intercalar e xs)

-- dados una lista y un elemento, devuelve una lista con ese elemento
-- agregado al final de la lista
snoc :: [a] -> a -> [a]
snoc [] e = [e]
snoc (x:xs) e = x : (snoc xs e)

-- dadas dos listas, devuelve la lista con todos los elementos de la primera lista
-- y todos los elementos de la segunda a continuacion
-- definida en Haskell como ++
append :: [a] -> [a] -> [a]
append [] ys = ys
append (x:xs) ys = x : (append xs ys)

append' :: [a] -> [a] -> [a]
append' xs [] = xs
append' xs (y:ys) = append' (snoc xs y) ys

-- dada una lista de listas, devuelve una unica lista con todos sus elementos
aplanar :: [[a]] -> [a]
aplanar [] = []
aplanar (xs:xss) = append xs (aplanar xss)

-- dada una lista, devuelve la lista con los mismos elementos de atras para adelante
-- definida en Haskell como reverse
reversa :: [a] -> [a]
reversa [] = []
reversa (x:xs) = snoc (reversa xs) x

-- dadas dos listas de enteros, devuelve una lista
-- donde el elemento en la posicion n es el maximo
-- entre el elemento n de la primera lista y de la segunda lista,
-- teniendo en cuenta que las listas no necesariamente tienen la misma longitud
zipMaximos :: [Int] -> [Int] -> [Int]
zipMaximos xs [] = xs
zipMaximos [] ys = ys
zipMaximos (x:xs) (y:ys) = (maximo x y) : (zipMaximos xs ys)

-- dadas dos listas de enteros de igual longitud, devuelve
-- una lista de pares (min, max), donde min y max son el minimo y el maximo
-- entre los elementos de ambas listas en la misma posicion
zipSort :: [Int] -> [Int] -> [(Int, Int)]
zipSort [] [] = []
zipSort (x:xs) (y:ys) = (ordenar x y) : (zipSort xs ys)
zipSort xs ys = error "listas de distinta longitud"

ordenar :: Int -> Int -> (Int, Int)
ordenar x y =
  if x < y
    then (x, y)
    else (y, x)
