module Adicionales02 where

{-
Para los que quieran hacer algunos ejercios adicionales de Estructuras de Datos
TIPOS DE DATOS ALGEBRAICOS
Roman Garcia

Nota: Sacar los -- para descomentar las signaturas de las funciones, que estan comentadas
para que no de errores el Haskell/WinHugs
-}


----------------- Repaso de la Practica 1  ---------------------
{-
Dado un String, devolver un string con el pasado repetido n veces
-}
repetir :: String -> Int -> String
repetir _ 0 = ""
repetir s n = s ++ repetir s (n - 1)

{-
Dado un elemento y una lista, determinar si el elemento está en la lista
Test: esta 4 [2, 3, -5, 4, 0] --> True
-}
estaEn :: Eq a => a -> [a] -> Bool
estaEn e [] = False
estaEn e (x:xs) = (e == x) || estaEn e xs

{-
Dada una lista, determinar si tiene elementos repetidos
Test: tieneRepetidos [3, 4, 5, 3, 2] --> True
-}
tieneRepetidos :: Eq a => [a] -> Bool
tieneRepetidos [] = False
tieneRepetidos (x:xs) = (estaEn x xs) || tieneRepetidos xs


----------------- Vamos a jugar un poco. ---------------------
{-
Para ello podemos construir mapas (cuadrados) de un cierto tamaño y donde
puede haber varias cosas desperdigadas.
-}
data Mapa =
  Mapa Tamanio
       [POI]
  deriving (Show)

type POI = (Posicion, Cosa)

data Cosa
  = Pocito
  | Agua
  | Crater
  | Tesoro Int
  | Salida
  deriving (Eq, Show)

type Posicion = (Fila, Columna)

type Tamanio = Int

type Fila = Int

type Columna = Int

-- Este es un mapa de ejemplo
mapa = Mapa 4 [((1, 1), Pocito), ((3, 3), Agua), ((4, 3), Tesoro 100), ((4, 4), Salida)]

{-
Determina si la posicion dada esta dentro del mapa
Test: esPosicionValida mapa (4, 7) --> False
-}
esPosicionValida :: Mapa -> Posicion -> Bool
esPosicionValida (Mapa t pois) (f, c) =
  (f > 0) && (f <= t)
  && (c > 0) && (c <= t)

{-
Dado un mapa, devolver la lista de cosas que hay en el
Test: cosas mapa --> [., ~, X, !]  
-}
cosas :: Mapa -> [Cosa]
cosas (Mapa t pois) = (mapCosas pois)

mapCosas :: [POI] -> [Cosa]
mapCosas [] = []
mapCosas ((p,c):pois) = c : (mapCosas pois)

{-
Dado un mapa, devolver la lista de posiciones con cosas
Test: posCosas --> [(1, 1), (3, 3), (4, 3), (4, 4)]  
-}
posDeLasCosas :: Mapa -> [Posicion]
posDeLasCosas (Mapa t pois) = mapPosiciones pois

mapPosiciones :: [POI] -> [Posicion]
mapPosiciones [] = []
mapPosiciones ((p,c):pois) = p : (mapPosiciones pois)

posDeUnTipoDeCosas :: Mapa -> Cosa -> [Posicion]
posDeUnTipoDeCosas (Mapa t pois) c = mapPosiciones (filtrarCosa c pois)

filtrarCosa :: Cosa -> [POI] -> [POI]
filtrarCosa c [] = []
filtrarCosa c ((p1,c1):pois) =
  if (c1 == c)
    then (p1, c1) : (filtrarCosa c pois)
    else filtrarCosa c pois

{-
Dado una lista de cosas determinar si hay una Salida
Test: haySalida (cosas mapa) ---> True 
-}
mapaTieneSalida :: Mapa -> Bool
mapaTieneSalida m = (posDeUnTipoDeCosas m Salida) /= []

{-
Dado un mapa, determinar si es un mapa valido: o sea, que:
- El tamaño es un numero positivo
- El mapa tiene al menos una Salida
- El mapa no tiene mas de una cosa en la misma Posicion
-}
esMapaValido :: Mapa -> Bool
esMapaValido m = (esTamanioOK m)
  && (mapaTieneSalida m)
  && (unaCosaPorPosicion m)

esTamanioOK :: Mapa -> Bool
esTamanioOK (Mapa t pois) = t > 1

unaCosaPorPosicion :: Mapa -> Bool
unaCosaPorPosicion m = not (tieneRepetidos (posDeLasCosas m))

haySalidaEn :: Mapa -> Posicion -> Bool
haySalidaEn m p = estaEn p (posDeUnTipoDeCosas m Salida)


------ Hagamos una version criolla de Maybe y lo usamos ------------
data TalVez a
  = Nada
  | Algo a
  deriving (Show)

{-
Definamos una función que busca si hay algo en una posicion del mapa
Test: cosaEnPosicion mapa (4, 3) --> Algo Tesoro 
-}
cosaEnPosicion :: Mapa -> Posicion -> TalVez Cosa
cosaEnPosicion (Mapa t pois) p = buscarCosaEnPosicion pois p

buscarCosaEnPosicion :: [POI] -> Posicion -> TalVez Cosa
buscarCosaEnPosicion [] p = Nada
buscarCosaEnPosicion ((p1, c1):ps) p =
  if p1 == p
    then Algo c1
    else buscarCosaEnPosicion ps p


----------------- Emiliano, esta es para vos! ---------------------
{-
Un ninja va a tratar de recorrer nuestros mapas. Nuestro ninja entiende
las siguientes direcciones:
-}
data Dir
  = Norte
  | Sur
  | Este
  | Oeste

{-
No se debe "caer del mapa", por lo que dado una posicion actual, debemos
calcular la nueva Posicion. Supongamos que está en una posicion valida
Test: nuevaPosicion mapa Norte (1, 2) --> (1, 2) 
-}
nuevaPosicion :: Mapa -> Dir -> Posicion -> Posicion
nuevaPosicion m d p =
  if esPosicionValida m (calcNuevaPos p d)
    then calcNuevaPos p d
    else p

calcNuevaPos :: Posicion -> Dir -> Posicion
calcNuevaPos (f,c) Norte = (f - 1, c)
calcNuevaPos (f,c) Sur = (f + 1, c)
calcNuevaPos (f,c) Este = (f, c + 1)
calcNuevaPos (f,c) Oeste = (f, c - 1)


-------------- Definamos nuestros ninjas ----------------
data Ninja =
  Ninja Posicion
        Energia
        Riqueza
  deriving (Show)

type Energia = Int

type Riqueza = Int

-- defino los proyectores
posicion (Ninja p _ _) = p

energia (Ninja _ e r) = e

riqueza (Ninja _ _ r) = r

estaMuerto :: Ninja -> Bool
estaMuerto ninja = (energia ninja) <= 0

-- Un ninja de ejemplo
rafael = Ninja (3, 2) 100 100

{-
Vamos a tratar de mover nuestro ninja en una direccion, dividiendo esa
tarea en dos mas sencillas:
Primero: cambiamos su posicion. Si se movió, pierde tres de Energia
-}
cambiarPosicion :: Mapa -> Ninja -> Dir -> Ninja
cambiarPosicion m (Ninja p e r) d =
  Ninja (nuevaPosicion m d p) (ajustarEnergia e p (nuevaPosicion m d p)) r

ajustarEnergia :: Energia -> Posicion -> Posicion -> Energia
ajustarEnergia e oldp newp =
  if newp == oldp
    then e
    else e - 3

{-
luego, sufre las consecuencias de haberse movido a ese lugar, tambien lo
dividimos en dos tareas:
- Si encuentra un Pocito no le pasa Nada
- Si encuentra Agua aumenta la enegia en 10 puntos
- Si se cae en un Crater, se muere (pierde toda su energia)
- Si Encuentra un tesoro, su riqueza aumenta en el valor del tesoro y
  su energia al doble   
-}
sufrirCosa :: TalVez Cosa -> Ninja -> Ninja
sufrirCosa (Algo Agua) (Ninja p e r) = Ninja p (e + 10) r
sufrirCosa (Algo Crater) (Ninja p e r) = Ninja p 0 r
sufrirCosa (Algo (Tesoro n)) (Ninja p e r) = Ninja p (2 * e) (r + n)
sufrirCosa c n = n

{-
La componemos con la anterior funcion
-}
sufrirCosaDelLugar :: Mapa -> Ninja -> Ninja
sufrirCosaDelLugar m n = sufrirCosa (cosaEnPosicion m (posicion n)) n

{-
Para finalmente componer la solucion:
Test: mover mapa Este rafael -> Ninja (3,3) 107 100 
-}
mover :: Mapa -> Dir -> Ninja -> Ninja
mover m d n = sufrirCosaDelLugar m (cambiarPosicion m n d)

{-
Y para terminar, vamos a hacer que nuestro ninja siga una lista de
direcciones. Para de procesar las direcciones si esta muerto
(no tiene energia) o si encontro una Salida
Test: seguirDirecciones mapa [Este] rafael -> Ninja (3,3) 107 100
Test: seguirDirecciones mapa [Este,Sur] rafael -> Ninja (4,3) 208 200
Test: seguirDirecciones mapa [Este,Sur,Este] rafael -> Ninja (4,4) 205 200
Test: seguirDirecciones mapa [Este,Sur,Este,Norte] rafael -> Ninja (4,4) 205 200
Test: seguirDirecciones mapa [Este,Sur,Este,Norte,Norte] rafael -> Ninja (4,4) 205 200 
-}
seguirDirecciones :: Mapa -> [Dir] -> Ninja -> Ninja
seguirDirecciones m [] n = n
seguirDirecciones m (d:ds) n =
  if esFinDeJuego m n
    then n
    else seguirDirecciones m ds (mover m d n)

esFinDeJuego :: Mapa -> Ninja -> Bool
esFinDeJuego m n = (estaMuerto n) || (haySalidaEn m (posicion n))


------------------------------ FIN ---------------------------------

--Por favor ignorar lo que sigue:
mostrarC :: Cosa -> String
mostrarC Pocito = "."
mostrarC Agua = "~"
mostrarC Crater = "O"
mostrarC (Tesoro x) = "X"
mostrarC Salida = "!"

mostrarM :: Mapa -> String
mostrarM (Mapa tamanio pois) =
  repetir "-" (tamanio * 2 + 1) ++ "\n" ++
  rectangulo pois tamanio tamanio ++
  repetir "-" (tamanio * 2 + 1) ++ "\n"

rectangulo :: [POI] -> Int -> Int -> String
rectangulo pois t 0 = ""
rectangulo pois t n = rectangulo pois t (n - 1) ++ "|" ++ linea pois n t ++ "\n"

linea :: [POI] -> Int -> Int -> String
linea pois t 0 = ""
linea pois t n = linea pois t (n - 1) ++ punto pois (t, n)

punto :: [POI] -> Posicion -> String
punto [] _ = " |"
punto ((p1, c):ps) p2
  | p1 == p2 = mostrarC c ++ "|"
  | otherwise = punto ps p2
