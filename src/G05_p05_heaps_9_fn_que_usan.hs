-- http://github.com/EspinolaAbel
import G05_p05_heaps_8_heap_usando_BST

import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe


-- datos para pruebas

lchar1 = shuffle ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p']

lint1 = shuffle [0..15]


-- dada una lista, la ordena de menor a mayor utilizando un Heap
-- como estructura auxiliar
heapSort :: Ord a => [a] -> [a]
heapSort xs = heap2list (list2heap xs)

list2heap :: Ord a => [a] -> Heap a
list2heap [] = emptyH
list2heap (x:xs) = insertH x (list2heap xs)

heap2list :: Ord a => Heap a -> [a]
heap2list h =
  if isEmptyH h
    then []
    else min : heap2list (h_sin_min)
  where
    (min, h_sin_min) = splitMin h


-- mias

-- hace shuffle de una lista
shuffle :: [a] -> [a]
shuffle xs = list2shuffle (genHorT (length xs)) xs []

list2shuffle :: [Char] -> [a] -> [a] -> [a]
list2shuffle ds [] ys = ys
list2shuffle (d:ds) (x:xs) ys =
  if d == 'h'
    then list2shuffle ds xs (x : ys)
    else list2shuffle ds xs (ys ++ [x])

genHorT :: Int -> [Char]
genHorT n = map int2ht (nums n)

int2ht :: Int -> Char
int2ht i =
  if i < 0
    then 'h'
    else 't'

-- genera N numeros aleatorios entre 0 y 9 (repeticiones permitidas)
genNums :: Int -> [Int]
genNums n = map (`mod` 10) (nums n)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]
