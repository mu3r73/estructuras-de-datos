module Adicionales09 where

--import ADT_ColaTamanioMaximo
import G04_roman_adicionales_9_p02_queue_con_tamanio_maximo
--import Practica02(Persona(..))
import G02_p01_tipos_def_por_usr_e02_persona
--import Practica04(unirQ)

{-
Ejercicios adicionales de tipos de datos Abstractos
Roman Garcia
-}

{-
PUNTO 1:
El Banco provincia se quedó con un solo cajero que debe 
atender varias colas de clientes porque los empleados fueron
agasajados por un cliente agradecido con comida en mal estado
y el que tiene que trabajar se salvó por celiaco.

Utilizaremos una implementacion de cola estandar con los
siguientes cambios:

-- Dada un numero, crea una pila vacia con el tamanio maximo dado
emptyQ :: Int -> Queue a

-- Devuelve true si la cola llego a su tamanio maximo
isFull :: Queue a -> Bool

-- Devuelve el tamaño maximo que puede tener la cola
maxSize :: Queue a -> Int

Ayuda: Usen el ADT que ya tienen definido y hagan los cambios necesarios
-}

juan  = Pers "Juan" 55
pedro = Pers "Pedro" 12
pablo = Pers "Pablo" 33
jose  = Pers "Jose" 10

qError = queue jose (queue juan (queue pedro (emptyQ 2)))

q1 = queue pablo (emptyQ 3)                               --{<pablo<}
q2 = queue jose (queue juan (queue pedro (emptyQ 3)))     --{<pedro,juan,jose<}
q3 = queue pedro (emptyQ 1)                               --{<pedro<}

colas = [q1, q2, q3]

-- Determina la cantidad de personas que estan esperando en las colas
--Test cantClientes colas -> 5
cantClientes :: [Queue Persona] -> Int
cantClientes [] = 0
cantClientes (q:qs) = (largoQ q) + (cantClientes qs)

-- tomado de pr.4 ej.2 p.1
largoQ :: Queue a -> Int
largoQ q =
  if isEmptyQ q
    then 0
    else 1 + largoQ (dequeue q)

-- Escoje a la primera persona de una cola llena para ser atendido
-- Si la lista esta vacia o no hay una cola llena, se rompe
--Test: elElegido colas -> pedro
elElegido :: [Queue Persona] -> Persona
elElegido (q:qs) =
  if isFull q
    then firstQ q
    else elElegido qs

-- Viene el policia y dice que se formen todos en una sola cola. la nueva cola
-- tiene todos los clientes de la primera cola, luego los de la segunda cola,
-- y asi sucesivamente. Hay al menos una cola
-- Ojo!!: La nueva cola debe tener el doble del maximo que las otras colas
-- Pista: usar funciones auxiliares
unaSolaCola :: [Queue Persona] -> Queue Persona
unaSolaCola qs = unaSolaColaRec ((length qs) * (maxDeMaxs qs)) (reverse qs)

unaSolaColaRec :: Int -> [Queue Persona] -> Queue Persona
unaSolaColaRec m [] = emptyQ m
unaSolaColaRec m (q:qs) = unirQ (unaSolaColaRec m qs) q

maxDeMaxs :: [Queue a] -> Int
maxDeMaxs [] = error "lista vacía"
maxDeMaxs [q] = maxSize q
maxDeMaxs (q:qs) = maximo (maxSize q) (maxDeMaxs qs)

maximo :: Ord a => a -> a -> a
maximo n m =
  if n > m
    then n
    else m

-- tomado de pr.4 ej.2 p.1
unirQ :: Queue a -> Queue a -> Queue a
unirQ q1 q2 =
  if isEmptyQ q2
    then q1
    else unirQ (queue (firstQ q2) q1) (dequeue q2)

{-
PUNTO 2: Implementar la nueva cola
-}
-- @ G04_roman_adicionales_9_p02_queue_con_tamanio_maximo
