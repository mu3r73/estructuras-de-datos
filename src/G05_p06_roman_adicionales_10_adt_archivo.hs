{-
PARTE DEL EJERCICIOS ADICIONALES 10
-}
module G05_p06_roman_adicionales_10_adt_archivo (
  Archivo, TipoDeArchivo(..), nombre, peso, tipo, crearArchivo
) where

{-
Vamos a modelar el tipo de datos abstracto Archivo.
De un Archivo nos interesa conocer el nombre, peso (en bytes) y el tipo,
que puede ser
-}
data TipoDeArchivo
  = Grafico
  | Ejecutable
  | Texto
  deriving (Show)

data Archivo =
  A String
    Int
    TipoDeArchivo
  deriving (Show)

nombre :: Archivo -> String
nombre (A n p t) = n

peso :: Archivo -> Int
peso (A n p t) = p

tipo :: Archivo -> TipoDeArchivo
tipo (A n p t) = t

crearArchivo :: String -> Int -> TipoDeArchivo -> Archivo
crearArchivo n p t = A n p t
