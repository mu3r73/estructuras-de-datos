import G05_p03_multiset_4_1_multiset_usando_map

-- dado un string, cuenta las ocurrencias de cada caracter utilizando
-- un MultiSet
ocurrencias :: String -> Map Char Int
ocurrencias [] = emptyMS
ocurrencias (x:xs) = addMS x (ocurrencias xs)
