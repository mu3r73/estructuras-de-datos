module G01_p02_recursion_e02_sobre_numeros (
  factorial, cuentaRegresiva, contarHasta, replicarN, desdeHasta, takeN, dropN, splitN
) where

import G01_p02_recursion_e01_sobre_listas

-- dado un numero n, devuelve la multiplicacion de este numero
-- y todos sus anteriores hasta llegar a 0
-- si n es 0, devuelve 1
-- la funcion es parcial si n es negativo
factorial :: Int -> Int
factorial n =
  if n > 0
    then facto n
    else error "solo para numeros > 0"

facto :: Int -> Int
facto 0 = 1
facto n = n * facto (n - 1)

-- dado un numero n, devuelve una lista cuyos elementos son
-- los numeros comprendidos entre n y 1 (incluidos)
-- si el numero es inferior a 1, devuelve la lista vacia
cuentaRegresiva :: Int -> [Int]
cuentaRegresiva n =
  if n > 0
    then creg n
    else []

creg :: Int -> [Int]
creg 1 = [1]
creg n = n : creg (n - 1)

-- dado un numero n, devuelve una lista cuyos elementos
-- son los numeros entre 1 y n (incluidos)
contarHasta :: Int -> [Int]
contarHasta n =
  if n > 0
    then contarh 1 n
    else []

contarh :: Int -> Int -> [Int]
contarh m n =
  if m == n
    then [m]
    else m : contarh (m+1) n

contarHasta' :: Int -> [Int]
contarHasta' n = reversa (cuentaRegresiva n)

-- dado un numero n y un elemento e, devuelve una lista
-- en la que el elemento e repite n veces
replicarN :: Int -> a -> [a]
replicarN n e =
  if n > 0
    then repli n e
    else []

repli :: Int -> a -> [a]
repli 1 e = [e]
repli n e = e : (repli (n - 1) e)

-- dados dos numeros n y m, devuelve una lista
-- cuyos elementos son los numeros entre n y m (incluidos)
desdeHasta :: Int -> Int -> [Int]
desdeHasta n m =
  if n <= m
    then dea n m
    else []

dea :: Int -> Int -> [Int]
dea n m =
  if n == m
    then [n]
    else n : dea (n + 1) m

-- dados un numero n y una lista xs, devuelve una lista
-- con los primeros n elementos de xs
-- si xs posee menos de n elementos, devuelve la lista completa
takeN :: Int -> [a] -> [a]
takeN n xs =
  if n >= 0
    then takeN' n xs
    else []

takeN' :: Int -> [a] -> [a]
takeN' 0 xs = []
takeN' n [] = []
takeN' n (x:xs) = x : (takeN' (n - 1) xs)

-- dados un numero n y una lista xs, devuelve una lista
-- sin los primeros n elementos de lista recibida
-- si la lista posee menos de n elementos, devuelve una lista vacia
dropN :: Int -> [a] -> [a]
dropN n xs =
  if n >= 0
    then dropN' n xs
    else []

dropN' :: Int -> [a] -> [a]
dropN' 0 xs = xs
dropN' n [] = []
dropN' n (x:xs) = dropN' (n - 1) xs

-- dados un numero n y una lista xs, devuelve un par donde
-- la primera componente es la lista que resulta de aplicar takeN a xs,
-- y la segunda componente el resultado de aplicar dropN a xs
-- ¿conviene utilizar recursion?
splitN :: Int -> [a] -> ([a], [a])
splitN n xs = (takeN n xs, dropN n xs)
