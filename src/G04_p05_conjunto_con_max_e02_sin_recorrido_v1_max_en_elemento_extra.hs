module G04_p05_conjunto_con_max_e02_sin_recorrido_v1_max_en_elemento_extra (
  Conjunto, vacioC, agregarC, perteneceC, cantidadC, borrarC, unionC,
  listaC, maximoC, list2set
) where

data Conjunto a =
  Set Int -- cant de elem
      [a] -- elem
      a -- max
  deriving (Show, Eq)

-- crea un conjunto vacio
vacioC :: Conjunto a
vacioC = Set 0 [] (error "conjunto vacío no tiene máximo")

-- dados un elemento y un conjunto, agrega el elemento al conjunto
agregarC :: Ord a => a -> Conjunto a -> Conjunto a
agregarC e (Set 0 [] err) =
  Set 1 [e] e
agregarC e (Set n es m) =
  if perteneceConMax e es m
    then Set n es m
    else Set (n + 1) (e : es) (maximo e m)

perteneceConMax :: Eq a => a -> [a] -> a -> Bool
perteneceConMax e [] m = False
perteneceConMax e (x:xs) m =
  (e == x) || (e == m) || perteneceConMax e xs m

maximo :: Ord a => a -> a -> a
maximo n m =
  if n > m
    then n
    else m

-- dados un elemento y un conjunto,
-- indica si el elemento pertenece al conjunto
perteneceC :: Eq a => a -> Conjunto a -> Bool
perteneceC e (Set n es m) = perteneceConMax e es m

-- devuelve la cantidad de elementos distintos de un conjunto
cantidadC :: Eq a => Conjunto a -> Int
cantidadC (Set n es m) = n

-- devuelve el conjunto sin el elemento dado
borrarC :: Ord a => a -> Conjunto a -> Conjunto a
borrarC e (Set n es m) =
  if (perteneceConMax e es m)
    then Set (n - 1) (borrar e es) (nuevoMax e m (borrar e es))
    else Set n es m

borrar :: Eq a => a -> [a] -> [a]
borrar e [] = []
borrar e (x:xs) =
  if e == x
    then xs
    else x : (borrar e xs)

nuevoMax :: Ord a => a -> a -> [a] -> a
nuevoMax e m es =
  if (e == m)
    then maximoL es
    else m

maximoL :: Ord a => [a] -> a
maximoL [] = error "lista vacia"
maximoL [x] = x
maximoL (x:xs) = maximo x (maximoL xs)

-- dados dos conjuntos, devuelve un conjunto
-- con todos los elementos de ambos conjuntos
unionC :: Ord a => Conjunto a -> Conjunto a -> Conjunto a
unionC c (Set n es m) = agregarListaAConj es c

agregarListaAConj :: Ord a => [a] -> Conjunto a -> Conjunto a
agregarListaAConj [] c = c
agregarListaAConj (x:xs) c = agregarC x (agregarListaAConj xs c)

-- dado un conjunto, devuelve una lista
-- con todos los elementos distintos del conjunto
listaC :: Eq a => Conjunto a -> [a]
listaC (Set n es m) = es

-- devuelve el maximo elemento en un conjunto
maximoC :: Ord a => Conjunto a -> a
maximoC (Set n es m) = m


-- mias

list2set :: Ord a => [a] -> Conjunto a
list2set [] = vacioC
list2set (x:xs) = agregarC x (list2set xs)
