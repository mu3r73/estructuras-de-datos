module G05_p04_arboles_de_busqueda_5_3_AVL_n_hi_hd_guardando_h (
  Tree, insertBST, deleteBST, perteneceBST, splitminBST, splitmaxBST,
  elMaximoMenorA, elMinimoMayorA,
  emptyBST, isEmptyBST, recuperarBST, isAVL, inOrderBST, avlFromList,
  prettyprint
) where

import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe

-- arbol binario
data Tree a
  = EmptyT
  | NodeT Int
          a
          (Tree a)
          (Tree a)
  deriving (Show)

-- invariantes:
-- AVLTree es un BST
-- => EmptyAVL es un BST
-- => NodeAVL h n ti td es un BST si:
--    * todo elemento de ti es menor que n
--    * todo elemento de td es mayor que n
--    * ti y td son BST (AVL)
-- ademas, para ser AVL, debe estar balanceado
-- * balanceado = diferencia de altura entre hijos es <= 1


-- datos para tests
{-
nota: para generar un arbol a partir de una lista en el orden correcto,
      ej: avlFromList (reverse [1..6])

nota: para arboles aleatorios, usar avlFromList (shuffle <lista>),
      ej: avlFromList (shuffle [1..6]), -- sin repetidos
      y asignar expresion resultante a una var
-}

tchar1 :: Tree Char
tchar1 = avlFromList (shuffle ['a', 'c', 'e', 'g', 'i', 'k', 'm'])

tint1 :: Tree Int
tint1 = avlFromList (shuffle [1..7])


-- dado un BST, inserta un elemento en el arbol
insertBST :: Ord a => a -> Tree a -> Tree a
insertBST e EmptyT = NodeT 1 e EmptyT EmptyT
insertBST e t@(NodeT h n ti td) =
  if e == n
--    then t -- conserva valor anterior
    then NodeT h e ti td -- reemplaza valor anterior
    else
      if e < n
        then balancearSiSeDesbalanceo (NodeT nh_con_e_en_ti n ti_con_e td)
        else balancearSiSeDesbalanceo (NodeT nh_con_e_en_td n ti td_con_e)
  where
    ti_con_e = insertBST e ti
    td_con_e = insertBST e td
    nh_con_e_en_ti = 1 + max (alturaT ti_con_e) (alturaT td)
    nh_con_e_en_td = 1 + max (alturaT ti) (alturaT td_con_e)


-- dado un BST, borra un elemento del arbol
deleteBST :: Ord a => a -> Tree a -> Tree a
deleteBST e EmptyT = EmptyT
deleteBST e (NodeT h n ti td) =
  if e == n
    then
      balancearSiSeDesbalanceo (rearmarBST ti td)
    else
      if e < n
        then balancearSiSeDesbalanceo (NodeT nh_sin_e_en_ti n ti_sin_e td)
        else balancearSiSeDesbalanceo (NodeT nh_sin_e_en_td n ti td_sin_e)
  where
    ti_sin_e = deleteBST e ti
    td_sin_e = deleteBST e td
    nh_sin_e_en_ti = 1 + max (alturaT ti_sin_e) (alturaT td)
    nh_sin_e_en_td = 1 + max (alturaT ti) (alturaT td_sin_e)

-- rearma el AVL a partir de sus HI, HD anteriores
rearmarBST :: Ord a => Tree a -> Tree a -> Tree a
rearmarBST EmptyT td = td
rearmarBST ti EmptyT = ti
rearmarBST ti td = rearmarBSTmaxHI ti td

-- promueve maximo del subarbol izquierdo, usando splitmaxBST
rearmarBSTmaxHI :: Ord a => Tree a -> Tree a -> Tree a
rearmarBSTmaxHI ti td = NodeT nh_sin_m m ti_sin_m td
  where
    (m, ti_sin_m) = splitmaxBST ti
    nh_sin_m = 1 + max (alturaT ti_sin_m) (alturaT td)

-- promueve minimo del subarbol derecho, usando splitminBST
rearmarBSTminHD :: Ord a => Tree a -> Tree a -> Tree a
rearmarBSTminHD ti td = NodeT nh_sin_m m ti td_sin_m
  where
    (m, td_sin_m) = splitminBST td
    nh_sin_m = 1 + max (alturaT ti) (alturaT td_sin_m)


-- dado un BST, dice si el elemento pertenece o no al arbol
perteneceBST :: Ord a => a -> Tree a -> Bool
perteneceBST e EmptyT = False
perteneceBST e (NodeT h n ti td) =
  if e == n
    then True
    else
      if e < n
        then perteneceBST e ti
        else perteneceBST e td


-- dado un BST, devuelve un par con el minimo elemento
-- y el arbol sin el mismo

-- sin fn aux, con where
splitminBST :: Ord a => Tree a -> (a, Tree a)
splitminBST EmptyT = error "arbol vacio"
splitminBST (NodeT h n EmptyT td) = (n, td)
splitminBST (NodeT h n ti td) =
  (m, balancearSiSeDesbalanceo (NodeT nh_sin_m n ti_sin_m td))
    where
      (m, ti_sin_m) = splitminBST ti
      nh_sin_m = 1 + max (alturaT ti_sin_m) (alturaT td)

-- con fn aux
splitminBST' :: Ord a => Tree a -> (a, Tree a)
splitminBST' t = (minBST t, sinminBST t)

minBST :: Ord a => Tree a -> a
minBST EmptyT = error "arbol vacio"
minBST (NodeT h n EmptyT td) = n
minBST (NodeT h n ti td) = minBST ti

sinminBST :: Ord a => Tree a -> Tree a
sinminBST EmptyT = error "arbol vacio"
sinminBST (NodeT h n EmptyT td) = td
sinminBST (NodeT h n ti td) =
  balancearSiSeDesbalanceo (NodeT nh_sin_m n ti_sin_m td)
    where
      ti_sin_m = sinminBST ti
      nh_sin_m = 1 + max (alturaT ti_sin_m) (alturaT td)


-- dado un BST, devuelve un par con el maximo elemento
-- y el arbol sin el mismo

-- sin fn aux, con where
splitmaxBST :: Ord a => Tree a -> (a, Tree a)
splitmaxBST EmptyT = error "arbol vacio"
splitmaxBST (NodeT h n ti EmptyT) = (n, ti)
splitmaxBST (NodeT h n ti td) =
  (m, balancearSiSeDesbalanceo (NodeT nh_sin_m n ti td_sin_m))
    where
      (m, td_sin_m) = splitmaxBST td
      nh_sin_m = 1 + max (alturaT ti) (alturaT td_sin_m)

-- con fn aux
splitmaxBST' :: Ord a => Tree a -> (a, Tree a)
splitmaxBST' t = (maxBST t, sinmaxBST t)

maxBST :: Ord a => Tree a -> a
maxBST EmptyT = error "arbol vacio"
maxBST (NodeT h n ti EmptyT) = n
maxBST (NodeT h n ti td) = maxBST td

sinmaxBST :: Ord a => Tree a -> Tree a
sinmaxBST EmptyT = error "arbol vacio"
sinmaxBST (NodeT h n ti EmptyT) = ti
sinmaxBST (NodeT h n ti td) =
  balancearSiSeDesbalanceo (NodeT nh_sin_m n ti td_sin_m)
    where
      td_sin_m = sinmaxBST td
      nh_sin_m = 1 + max (alturaT ti) (alturaT td_sin_m)


-- dado un BST y un elemento, devuelve el maximo elemento
-- que sea menor al elemento dado
elMaximoMenorA :: Ord a => a -> Tree a -> a
elMaximoMenorA e t = fromJust (maxMenorQue e t Nothing)

maxMenorQue :: Ord a => a -> Tree a -> Maybe a -> Maybe a
maxMenorQue e EmptyT x0 = x0
maxMenorQue e (NodeT h n ti td) x0 = 
  if e <= n
    then maxMenorQue e ti x0
    else maxMenorQue e td (Just n)

fromJust :: Maybe a -> a
fromJust (Just e) = e


-- dado un BST y un elemento, devuelve el minimo elemento
-- que sea mayor al elemento dado
elMinimoMayorA :: Ord a => a -> Tree a -> a
elMinimoMayorA e t = fromJust (minMayorQue e t Nothing)

minMayorQue :: Ord a => a -> Tree a -> Maybe a -> Maybe a
minMayorQue e EmptyT x0 = x0
minMayorQue e (NodeT h n ti td) x0 =
  if e >= n
    then minMayorQue e td x0
    else minMayorQue e ti (Just n)


-- balancea un arbol AVL, si se desbalanceo
balancearSiSeDesbalanceo :: Tree a -> Tree a
balancearSiSeDesbalanceo t =
  if seDesbalanceo t
    then balancearAVL t
    else t

-- indica si el arbol AVL se desbalanceo
seDesbalanceo :: Tree a -> Bool
seDesbalanceo t = abs (balanceT t) > 1

-- balancea un arbol AVL
balancearAVL :: Tree a -> Tree a
balancearAVL t@(NodeT h n ti td) =
  if bt == 2
    then
      if bti >= 0
        then rotarDer t -- izq izq
        else rotarDer (NodeT h n (rotarIzq ti) td) -- izq der
    else -- bst == -2
      if btd == -1
        then rotarIzq t -- der der
        else rotarIzq (NodeT h n ti (rotarDer td)) -- der izq
  where
    bt = balanceT (NodeT h n ti td)
    bti = balanceT ti
    btd = balanceT td  

-- devuelve la altura de un arbol
alturaT :: Tree a -> Int
alturaT EmptyT = 0
alturaT (NodeT h n ti td) = h

-- calcula el factor de balance de un arbol
balanceT :: Tree a -> Int
balanceT EmptyT = 0
balanceT (NodeT h n ti td) = (alturaT ti) - (alturaT td)

-- rota un arbol hacia la derecha (sentido horario)
{-
         |n|              |ni|
        /   \            /    \
      |ni|  td    =>   tii    |n|
      /  \                    / \
    tii  tid                tid   td
-}
rotarDer :: Tree a -> Tree a
rotarDer (NodeT ht n (NodeT hti ni tii tid) td) =
  NodeT nhti ni tii (NodeT nht n tid td)
    where
      nht = 1 + max (alturaT tid) (alturaT td)
      nhti = 1 + max (alturaT tii) nht

-- rota un arbol hacia la izquierda (sentido anti-horario)
{-
      |n|                  |nd|
     /   \                /    \
    ti  |nd|      =>    |n|    tdd
        /  \            / \
      tdi  tdd         ti tdi
-}
rotarIzq :: Tree a -> Tree a
rotarIzq (NodeT ht n ti (NodeT htd nd tdi tdd)) =
  NodeT nhtd nd (NodeT nht n ti tdi) tdd
    where
      nht = 1 + max (alturaT ti) (alturaT tdi)
      nhtd = 1 + max nht (alturaT tdd)


-- mias

-- retorna un arbol vacio
emptyBST :: Tree a
emptyBST = EmptyT

-- indica si el arbol es vacio
isEmptyBST :: Tree a -> Bool
isEmptyBST EmptyT = True
isEmptyBST t = False

-- dados un elemento e y un BST, si e pertenece al arbol, lo retorna
recuperarBST :: Ord a => a -> Tree a -> Maybe a
recuperarBST e EmptyT = Nothing
recuperarBST e (NodeT h n ti td) =
  if e == n
    then Just n
    else
      if e < n
        then recuperarBST e ti
        else recuperarBST e td

-- indica si el arbol es un AVL
isAVL :: Ord a => Tree a -> Bool
isAVL EmptyT = True
isAVL t = (isBST t) && (esBalanceado t)

-- indica si un arbol es balanceado
esBalanceado :: Tree a -> Bool
esBalanceado EmptyT = True
esBalanceado t@(NodeT h n ti td) =
  (abs (balanceT t) < 2)
  && (abs (balanceT ti) < 2)
  && (abs (balanceT td) < 2)

-- indica si el arbol es un BST
isBST :: Ord a => Tree a -> Bool
isBST EmptyT = True
isBST (NodeT h n ti td) =
  (isBST ti)
  && (isBST td)
  && (todosMenoresBST ti n)
  && (todosMayoresBST td n)

todosMenoresBST :: Ord a => Tree a -> a -> Bool
todosMenoresBST EmptyT n = True
todosMenoresBST t n = (maxBST t) < n

todosMayoresBST :: Ord a => Tree a -> a -> Bool
todosMayoresBST EmptyT n = True
todosMayoresBST t n = (minBST t) > n

-- retorna la lista resultante de recorrer inorder
inOrderBST :: Tree a -> [a]
inOrderBST EmptyT = []
inOrderBST (NodeT h n ti td) = (inOrderBST ti) ++ [n] ++ (inOrderBST td)

-- genera un AVL a partir de una lista
avlFromList :: Ord a => [a] -> Tree a
avlFromList = foldr insertBST EmptyT

-- hace shuffle de una lista
shuffle :: [a] -> [a]
shuffle xs = list2shuffle (genHorT (length xs)) xs []

list2shuffle :: [Char] -> [a] -> [a] -> [a]
list2shuffle ds [] ys = ys
list2shuffle (d:ds) (x:xs) ys =
  if d == 'h'
    then list2shuffle ds xs (x : ys)
    else list2shuffle ds xs (ys ++ [x])

genHorT :: Int -> [Char]
genHorT n = map int2ht (nums n)

int2ht :: Int -> Char
int2ht i =
  if i < 0
    then 'h'
    else 't'

-- genera N numeros aleatorios entre 0 y 9 (repeticiones permitidas)
genNums :: Int -> [Int]
genNums n = map (`mod` 10) (nums n)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]


-----------------------------------------------------
-- impresion de arboles
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Show a => Tree a -> IO ()
prettyprint t = putStrLn (unlines (prettyprint_helper t))
-- unlines concats a list with newlines

prettyprint_helper :: Show a => Tree a -> [String]
prettyprint_helper (EmptyT) = ["e"]
prettyprint_helper (NodeT h e ti td)
  = (show e) : (prettyprint_subtree ti td)
--  = ((show e) ++ ":" ++ (show h)) : (prettyprint_subtree ti td)

prettyprint_subtree :: Show a => Tree a -> Tree a -> [String]
prettyprint_subtree ti td
  = ((pad "├──" "│  ") (prettyprint_helper td))
  ++ ((pad "╰──" "   ") (prettyprint_helper ti))

pad :: String -> String -> [String] -> [String]
pad first rest = zipWith (++) (first : repeat rest)
