module G05_p06_adicionales_10_quizas (
  Quizas, nothing, just, fromJust, isNothing
) where

data Quizas a = Q (Maybe a)
  deriving (Show)

nothing :: Quizas a
nothing = Q Nothing

just :: a -> Quizas a
just x = Q (Just x)

fromJust :: Quizas a -> a -- parcial para Nothing
fromJust (Q (Just x)) = x

isNothing :: Quizas a -> Bool
isNothing (Q Nothing) = True
isNothing q = False
