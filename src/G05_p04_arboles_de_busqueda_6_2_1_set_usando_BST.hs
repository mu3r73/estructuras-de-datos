module G05_p04_arboles_de_busqueda_6_2_1_set_usando_BST (
  Conjunto, vacioC, agregarC, perteneceC, cantidadC, borrarC, unionC,
  listaC,
  interC, list2set,
  G05_p04_arboles_de_busqueda_6_2_1_set_usando_BST.prettyprint
) where

import G05_p04_arboles_de_busqueda_5_3_AVL_n_hi_hd_guardando_h as BST

import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe

data Conjunto a = S (Tree a)

-- invariantes:
-- Set no tiene elementos repetidos

-- datos para tests
{-
nota: para conjuntos aleatorios, usar list2set (shuffle <lista>),
      ej: list2set (shuffle [1..6]),
      y asignar expresion resultante a una var
-}

cint1 :: Conjunto Int
cint1 = list2set (shuffle (map (* 2) [1..9]))

cint2 :: Conjunto Int
cint2 = list2set (shuffle [1..9])


-- crea un conjunto vacio
vacioC :: Conjunto a
vacioC = S emptyBST


-- dados un elemento y un conjunto, agrega el elemento al conjunto
agregarC :: Ord a => a -> Conjunto a -> Conjunto a
agregarC e (S t) = S (insertBST e t)


-- dados un elemento y un conjunto,
-- indica si el elemento pertenece al conjunto
perteneceC :: Ord a => a -> Conjunto a -> Bool
perteneceC e (S t) = perteneceBST e t


-- devuelve la cantidad de elementos distintos de un conjunto
cantidadC :: Eq a => Conjunto a -> Int
cantidadC (S t) = length (inOrderBST t)


-- devuelve el conjunto sin el elemento dado
borrarC :: Ord a => a -> Conjunto a -> Conjunto a
borrarC e (S t) = S (deleteBST e t)


-- dados dos conjuntos, devuelve un conjunto
-- con todos los elementos de ambos conjuntos
unionC :: Ord a => Conjunto a -> Conjunto a -> Conjunto a
unionC (S t1) s2 = agregarL (inOrderBST t1) s2

agregarL :: Ord a => [a] -> Conjunto a -> Conjunto a
agregarL [] s = s
agregarL (e:es) s = agregarC e (agregarL es s)


-- dado un conjunto, devuelve una lista
-- con todos los elementos distintos del conjunto
listaC :: Eq a => Conjunto a -> [a]
listaC (S t) = inOrderBST t



-- mias

-- dados dos conjuntos, retorna su interseccion
interC :: Ord a => Conjunto a -> Conjunto a -> Conjunto a
interC (S t1) s2 = interLC (inOrderBST t1) s2

interLC :: Ord a => [a] -> Conjunto a -> Conjunto a
interLC [] s = vacioC
interLC (e:es) s =
  if perteneceC e s
    then agregarC e (interLC es s)
    else interLC es s


-- construye un conjunto a partir de los elementos de una lista
list2set :: Ord a => [a] -> Conjunto a
list2set [] = vacioC
list2set (x:xs) = agregarC x (list2set xs)


-- hace shuffle de una lista
shuffle :: [a] -> [a]
shuffle xs = list2shuffle (genHorT (length xs)) xs []

list2shuffle :: [Char] -> [a] -> [a] -> [a]
list2shuffle ds [] ys = ys
list2shuffle (d:ds) (x:xs) ys =
  if d == 'h'
    then list2shuffle ds xs (x : ys)
    else list2shuffle ds xs (ys ++ [x])

genHorT :: Int -> [Char]
genHorT n = map int2ht (nums n)

int2ht :: Int -> Char
int2ht i =
  if i < 0
    then 'h'
    else 't'

-- genera N numeros aleatorios entre 0 y 9 (repeticiones permitidas)
genNums :: Int -> [Int]
genNums n = map (`mod` 10) (nums n)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]


-----------------------------------------------------
-- impresion de arboles
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Show a => Conjunto a -> IO ()
prettyprint (S t) = BST.prettyprint t
