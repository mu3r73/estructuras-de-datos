module G05_p02_map_3_celda (
  Celda, Color(Azul, Negro, Rojo, Verde),
  celdaVacia, poner, sacar, nroBolitas, hayBolitas
) where

import G05_p02_map_2_map_2_lista_de_pares_k_v_sin_claves_repetidas

data Celda =
  MkCelda (Map Color Int)
  deriving (Show)

data Color
  = Azul
  | Negro
  | Rojo
  | Verde
  deriving (Eq, Show)

{- Inv. Rep.:
- Existe una clave para cada color existente
- El valor asociado a un color es un numero positivo
-}

-- crea una celda con cero bolitas de cada color
-- orden 1
celdaVacia :: Celda
celdaVacia = MkCelda (initCol Verde (initCol Rojo (initCol Negro (initCol Azul emptyM))))

initCol :: Color -> Map Color Int -> Map Color Int
initCol col m = assocM m col 0

-- agrega una bolita de ese color a la celda
-- orden
poner :: Color -> Celda -> Celda
poner col cel@(MkCelda m) = MkCelda (assocM m col ((nroBolitas col cel) + 1))

-- saca una bolita de ese color,
-- parcial cuando no hay bolitas de ese color
-- orden
sacar :: Color -> Celda -> Celda
sacar col cel@(MkCelda m) =
  if hayBolitas col cel
    then MkCelda (assocM m col ((nroBolitas col cel) - 1))
    else error "BOOM - no hay bolitas de ese color"

-- devuelve la cantidad de bolitas de ese color
-- orden
nroBolitas :: Color -> Celda -> Int
nroBolitas col (MkCelda m) = fromJust (lookupM m col)

fromJust :: Maybe a -> a
fromJust (Just x) = x

-- indica si hay bolitas de ese color
-- orden
hayBolitas :: Color -> Celda -> Bool
hayBolitas col cel = (nroBolitas col cel) > 0
