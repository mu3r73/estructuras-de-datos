-- http://github.com/EspinolaAbel
module G05_p05_heaps_7_heap_usando_arbol_binario_n_hi_hd (
  Heap, emptyH, isEmptyH, insertH, findMin, deleteMin, splitMin,
  heapFromList, prettyprint
) where


import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe


data Heap a =
  H [Dir]
    (Tree a)
  deriving (Show)

data Tree a
  = EmptyT
  | NodeT a
          (Tree a)
          (Tree a)
  deriving (Show)

data Dir
  = Izq
  | Der
  deriving (Show)

-- invariantes:
-- H [Dir] (Tree a) es un (min-)Heap si:
-- * el nodo en la raiz es el menor de todo el arbol
-- * los subarboles son Heaps
-- * el arbol es completo
-- * [Dir] conduce a la proxima posicion donde insertar un elemento
-- * [Dir] se lee al reves


-- datos para tests
{-
nota: para mapas aleatorios, usar mapFromList (shuffle <lista>),
      ej: heapFromList (shuffle ['a', 'c', 'd', 'f', 'h', 'k', 'm']),
      y asignar expresion resultante a una var
-}

hchar1 :: Heap Char
hchar1 = heapFromList (shuffle ['a', 'c', 'd', 'f', 'h', 'k', 'm'])

hint1 :: Heap Int
hint1 = heapFromList (shuffle [1 .. 7])


-- retorna un heap vacio
emptyH :: Heap a
emptyH = H [] EmptyT

-- indica si el heap esta vacio
isEmptyH :: Heap a -> Bool
isEmptyH (H ds EmptyT) = True
isEmptyH h = False

-- agrega un nuevo elemento e al heap
insertH :: Ord a => a -> Heap a -> Heap a
insertH e (H ds t) = H (proxDirs ds) (insertT e (reverse ds) t)

insertT :: Ord a => a -> [Dir] -> Tree a -> Tree a
insertT e [] EmptyT = NodeT e EmptyT EmptyT
insertT e (Izq : ds) (NodeT n ti td) = bubbleUpIzq n (insertT e ds ti) td
insertT e (Der : ds) (NodeT n ti td) = bubbleUpDer n ti (insertT e ds td)

bubbleUpIzq :: Ord a => a -> Tree a -> Tree a -> Tree a
bubbleUpIzq n ti@(NodeT ni tii tid) td =
  if n < ni
    then NodeT n ti td
    else NodeT ni (NodeT n tii tid) td

bubbleUpDer :: Ord a => a -> Tree a -> Tree a -> Tree a
bubbleUpDer n ti td@(NodeT nd tdi tdd) =
  if n < nd
    then NodeT n ti td
    else NodeT nd ti (NodeT n tdi tdd)

proxDirs :: [Dir] -> [Dir]
proxDirs [] = [Izq]
proxDirs (Izq : ds) = Der : ds
proxDirs (Der : ds) = Izq : (proxDirs ds)

-- retorna el elemento minimo del heap
findMin :: Ord a => Heap a -> a -- Parcial en emptyH
findMin (H ds EmptyT) = error "heap vacio"
findMin (H ds (NodeT n ti td)) = n

-- borra el minimo elemento del heap
deleteMin :: Ord a => Heap a -> Heap a -- Parcial en emptyH
deleteMin (H ds EmptyT) = error "heap vacio"
deleteMin (H ds t) = H prev_ds (deleteT (reverse prev_ds) t)
  where
    prev_ds = prevDirs ds

deleteT :: Ord a => [Dir] -> Tree a -> Tree a
deleteT ds t = extraerT (splitAtDir ds t)

splitAtDir :: [Dir] -> Tree a -> (a, Tree a)
splitAtDir [] (NodeT m EmptyT EmptyT) = (m, EmptyT)
splitAtDir (Izq : ds) (NodeT m ti td) = (n_m, NodeT m n_ti td)
  where (n_m, n_ti) = splitAtDir ds ti
splitAtDir (Der : ds) (NodeT m ti td) = (n_m, NodeT m ti n_td)
  where (n_m, n_td) = splitAtDir ds td

extraerT :: Ord a => (a, Tree a) -> Tree a
extraerT (e, EmptyT) = EmptyT
extraerT (e, NodeT m ti td) = bubbleDn e ti td

bubbleDn :: Ord a => a -> Tree a -> Tree a -> Tree a
bubbleDn n EmptyT EmptyT = NodeT n EmptyT EmptyT
bubbleDn n ti@(NodeT ni tii tid) EmptyT =
  if n <= ni
    then NodeT n ti EmptyT
    else NodeT ni (bubbleDn n tii tid) EmptyT
bubbleDn n ti@(NodeT ni tii tid) td@(NodeT nd tdi tdd) =
  if (n <= ni) && (n <= nd)
    then NodeT n ti td
    else
      if (ni <= nd)
        then NodeT ni (bubbleDn n tii tid) td
        else NodeT nd ti (bubbleDn n tdi tdd)

prevDirs :: [Dir] -> [Dir]
prevDirs [Izq] = []
prevDirs (Der : ds) = Izq : ds
prevDirs (Izq : ds) = Der : (prevDirs ds)

-- retorna un par con el min elem de un heap y
-- el heap resultante de borrar ese elem
splitMin :: Ord a => Heap a -> (a, Heap a) -- Parcial en emptyH
splitMin h = ((findMin h), (deleteMin h))


-- mias

-- genera un heap a partir de una lista
heapFromList :: Ord a => [a] -> Heap a
heapFromList [] = emptyH
heapFromList (x:xs) = insertH x (heapFromList xs)

-- hace shuffle de una lista
shuffle :: [a] -> [a]
shuffle xs = list2shuffle (genHorT (length xs)) xs []

list2shuffle :: [Char] -> [a] -> [a] -> [a]
list2shuffle ds [] ys = ys
list2shuffle (d:ds) (x:xs) ys =
  if d == 'h'
    then list2shuffle ds xs (x : ys)
    else list2shuffle ds xs (ys ++ [x])

genHorT :: Int -> [Char]
genHorT n = map int2ht (nums n)

int2ht :: Int -> Char
int2ht i =
  if i < 0
    then 'h'
    else 't'

-- genera N numeros aleatorios entre 0 y 9 (repeticiones permitidas)
genNums :: Int -> [Int]
genNums n = map (`mod` 10) (nums n)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]


-----------------------------------------------------
-- impresion de arboles
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Show a => Heap a -> IO ()
prettyprint (H ds t) = putStrLn (unlines (prettyprint_helper t))
-- unlines concats a list with newlines

prettyprint_helper :: Show a => Tree a -> [String]
prettyprint_helper (EmptyT) = ["e"]
prettyprint_helper (NodeT e ti td)
  = (show e) : (prettyprint_subtree ti td)

prettyprint_subtree :: Show a => Tree a -> Tree a -> [String]
prettyprint_subtree ti td
  = ((pad "├──" "│  ") (prettyprint_helper td))
  ++ ((pad "╰──" "   ") (prettyprint_helper ti))

pad :: String -> String -> [String] -> [String]
pad first rest = zipWith (++) (first : repeat rest)
