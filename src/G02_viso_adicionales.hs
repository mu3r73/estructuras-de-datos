import G01_p01_conceptos_basicos_e01
import G01_p01_conceptos_basicos_e02

data List a
  = Nil
  | Cons a
         (List a)
  deriving (Show)

-- datos para tests
lint1 :: List Int
lint1 = buildList [1 .. 9]

lint2 :: List Int
lint2 = buildList [0 .. 4]

lint3 :: List Int
lint3 = buildList (reverse [3 .. 7])

lint4 :: List Int
lint4 = buildList [-5 .. 5]

lint5 :: List Int
lint5 = buildList [-5 .. -1]

lint6 :: List Int
lint6 = buildList [0 .. 5]

lpar1 :: List (Int, Int)
lpar1 = buildList (zip [0 .. 4] (reverse [3 .. 7]))

lbool1 = buildList [True, False, True, False]
lbool2 = buildList [True, True, True]
lbool3 = buildList [False, False, False]

lstr1 = buildList [(buildList "Estructuras"), (buildList "de"), (buildList "datos")]
lstr2 = buildList "AABCCC"

-- observadores
isNilL :: List a -> Bool
isNilL Nil = True
isNilL l = False

headL :: List a -> a
headL Nil = error "lista vacia"
headL (Cons e l) = e

tailL :: List a -> List a
tailL Nil = error "lista vacia"
tailL (Cons e l) = l

{-
rehacer ejercicios de listas de la práctica 1 usando esta estructura
-}

-- dada una lista de enteros, devuelve la suma de todos sus elementos
sumatoria :: List Int -> Int
sumatoria Nil = 0
sumatoria (Cons x xs) = x + sumatoria xs

-- dada una lista de elementos de algun tipo, devuelve el largo
-- de esa lista,
-- es decir, la cantidad de elementos que posee
longitud :: List a -> Int
longitud Nil = 0
longitud (Cons x xs) = 1 + longitud xs

-- dada una lista de enteros, devuelve un numero que es el promedio
-- entre todos los elementos de la lista
-- ¿pudo resolverla utilizando recursion estructural?
promedio :: List Int -> Int
promedio Nil = error "lista vacia"
promedio xs = (sumatoria xs) `div` (longitud xs)

-- dada una lista de enteros, devuelve la lista de los sucesores
-- de cada entero
mapSucesor :: List Int -> List Int
mapSucesor Nil = Nil
mapSucesor (Cons x xs) = Cons (sucesor x) (mapSucesor xs)

-- dada una lista de pares de enteros, devuelve una nueva lista
-- en la que cada elemento es la suma de los elementos de cada par
mapSumaPar :: List (Int, Int) -> List Int
mapSumaPar Nil = Nil
mapSumaPar (Cons p ps) = Cons (sumaPar p) (mapSumaPar ps)

-- dada una lista de pares, devuelve una nueva lista en la que
-- cada elemento es el mayor de las componentes de cada par
mapMaxDelPar :: List (Int,Int) -> List Int
mapMaxDelPar Nil = Nil
mapMaxDelPar (Cons p ps) = Cons (maxDelPar p) (mapMaxDelPar ps)

-- dada una lista de booleanos, devuelve True si todos sus elementos
-- son True
todoVerdad :: List Bool -> Bool
todoVerdad Nil = True
todoVerdad (Cons x xs) = x && (todoVerdad xs)

-- dada una lista de booleanos, devuelve True si alguno
-- de sus elementos es True
algunaVerdad :: List Bool -> Bool
algunaVerdad Nil = False
algunaVerdad (Cons x xs) = x || (algunaVerdad xs)

-- dados un elemento e y una lista xs, devuelve True 
-- si existe un elemento en xs que sea igual a e
pertenece :: Eq a => a -> List a -> Bool
pertenece e Nil = False
pertenece e (Cons x xs) =
  if e == x
    then True
    else pertenece e xs

-- dados un elemento e y una lista xs, cuenta la cantidad de apariciones
-- de e en xs
apariciones :: Eq a => a -> List a -> Int
apariciones e Nil = 0
apariciones e (Cons x xs) =
  if e == x
    then 1 + apariciones e xs
    else apariciones e xs

-- dados un numero n y una lista xs, devuelve todos los elementos de xs
-- que son menores a n
filtrarMenoresA :: Int -> List Int -> List Int
filtrarMenoresA n Nil = Nil
filtrarMenoresA n (Cons x xs) =
  if x < n
    then Cons x (filtrarMenoresA n xs)
    else filtrarMenoresA n xs

-- dados un elemento y una lista, filtra (elimina) todas las ocurrencias 
-- de ese elemento en la lista
filtrarElemento :: Eq a => a -> List a -> List a
filtrarElemento n Nil = Nil
filtrarElemento n (Cons x xs) =
  if x == n
    then filtrarElemento n xs
    else Cons x (filtrarElemento n xs)

-- dada una lista de listas, devuelve la lista de sus longitudes
-- aplique esta funcion a la lista de strings ["Estructuras", "de", "datos"]
-- y observe el resultado
mapLongitudes :: List (List a) -> List Int
mapLongitudes Nil = Nil
mapLongitudes (Cons xs xss) = Cons (longitud xs) (mapLongitudes xss)

-- dados un numero n y una lista de listas, devuelve la lista
-- de aquellas listas que tienen mas de n elementos.
longitudMayorA :: Int -> List (List a) -> List (List a)
longitudMayorA n Nil = Nil
longitudMayorA n (Cons xs xss) =
  if (longitud xs) > n
    then Cons xs (longitudMayorA n xss)
    else longitudMayorA n xss

-- dado un elemento e y una lista xs, ubica a e entre medio de todos
-- los elementos de xs
-- Ejemplo:
-- intercalar ',' "abcde" == "a,b,c,d,e"
intercalar :: a -> List a -> List a
intercalar e Nil = Nil
intercalar e (Cons x Nil) = Cons x Nil
intercalar e (Cons x xs) = Cons x (Cons e (intercalar e xs))

-- dados una lista y un elemento, devuelve una lista con ese elemento
-- agregado al final de la lista
snoc :: List a -> a -> List a
snoc Nil e = Cons e Nil
snoc (Cons x xs) e = Cons x (snoc xs e)

-- dadas dos listas, devuelve la lista con todos los elementos
-- de la primera lista 
-- y todos los elementos de la segunda a continuacion
-- definida en Haskell como ++
append :: List a -> List a -> List a
append Nil ys = ys
append (Cons x xs) ys = Cons x (append xs ys)

append' :: List a -> List a -> List a
append' xs Nil = xs
append' xs (Cons y ys) = append' (snoc xs y) ys

-- dada una lista de listas, devuelve una unica lista con todos
-- sus elementos
aplanar :: List (List a) -> List a
aplanar Nil = Nil
aplanar (Cons xs xss) = append xs (aplanar xss)

-- dada una lista, devuelve la lista con los mismos elementos
-- de atras para adelante
-- definida en Haskell como reverse
reversa :: List a -> List a
reversa Nil = Nil
reversa (Cons x xs) = snoc (reversa xs) x

-- dadas dos listas de enteros, devuelve una lista
-- donde el elemento en la posicion n es el maximo
-- entre el elemento n de la primera lista y de la segunda lista,
-- teniendo en cuenta que las listas no necesariamente tienen
-- la misma longitud
zipMaximos :: List Int -> List Int -> List Int
zipMaximos xs Nil = xs
zipMaximos Nil ys = ys
zipMaximos (Cons x xs) (Cons y ys) = Cons (maximo x y) (zipMaximos xs ys)

-- dadas dos listas de enteros de igual longitud, devuelve
-- una lista de pares (min, max), donde min y max son el minimo
-- y el maximo entre los elementos de ambas listas en la misma posicion
zipSort :: List Int -> List Int -> List (Int, Int)
zipSort Nil Nil = Nil
zipSort (Cons x xs) (Cons y ys) = Cons (ordenar x y) (zipSort xs ys)
zipSort xs ys = error "listas de distinta longitud"

ordenar :: Int -> Int -> (Int, Int)
ordenar x y =
  if x < y
    then (x, y)
    else (y, x)

-- dado un numero n, devuelve una lista cuyos elementos son
-- los numeros comprendidos entre n y 1 (incluidos)
-- si el numero es inferior a 1, devuelve la lista vacia
cuentaRegresiva :: Int -> List Int
cuentaRegresiva n =
  if n > 0
    then creg n
    else Nil

creg :: Int -> List Int
creg 1 = Cons 1 Nil
creg n = Cons n (creg (n - 1))

-- dado un numero n, devuelve una lista cuyos elementos
-- son los numeros entre 1 y n (incluidos)
contarHasta :: Int -> List Int
contarHasta n =
  if n > 0
    then contarh 1 n
    else Nil

contarh :: Int -> Int -> List Int
contarh m n =
  if m == n
    then Cons m Nil
    else Cons m (contarh (m+1) n)

contarHasta' :: Int -> List Int
contarHasta' n = reversa (cuentaRegresiva n)

-- dado un numero n y un elemento e, devuelve una lista
-- en la que el elemento e repite n veces
replicarN :: Int -> a -> List a
replicarN n e =
  if n > 0
    then repli n e
    else Nil

repli :: Int -> a -> List a
repli 1 e = Cons e Nil
repli n e = Cons e (repli (n - 1) e)

-- dados dos numeros n y m, devuelve una lista
-- cuyos elementos son los numeros entre n y m (incluidos)
desdeHasta :: Int -> Int -> List Int
desdeHasta n m =
  if n <= m
    then dea n m
    else Nil

dea :: Int -> Int -> List Int
dea n m =
  if n == m
    then Cons n Nil
    else Cons n (dea (n + 1) m)

-- dados un numero n y una lista xs, devuelve una lista
-- con los primeros n elementos de xs
-- si xs posee menos de n elementos, devuelve la lista completa
takeN :: Int -> List a -> List a
takeN n xs =
  if n >= 0
    then takeN' n xs
    else Nil

takeN' :: Int -> List a -> List a
takeN' 0 xs = Nil
takeN' n Nil = Nil
takeN' n (Cons x xs) = Cons x (takeN' (n - 1) xs)

-- dados un numero n y una lista xs, devuelve una lista
-- sin los primeros n elementos de lista recibida
-- si la lista posee menos de n elementos, devuelve una lista vacia
dropN :: Int -> List a -> List a
dropN n xs =
  if n >= 0
    then dropN' n xs
    else Nil

dropN' :: Int -> List a -> List a
dropN' 0 xs = xs
dropN' n Nil = Nil
dropN' n (Cons x xs) = dropN' (n - 1) xs

-- dados un numero n y una lista xs, devuelve un par donde
-- la primera componente es la lista que resulta de aplicar takeN a xs,
-- y la segunda componente el resultado de aplicar dropN a xs
-- ¿conviene utilizar recursion?
splitN :: Int -> List a -> (List a, List a)
splitN n xs = (takeN n xs, dropN n xs)

-- dada una lista xs de enteros, devuelve una tupla de listas,
-- donde la primera componente contiene todos los numeros positivos
-- de xs, y la segunda todos los numeros negativos de xs
-- ¿conviene utilizar recursion?
-- considere utilizar funciones auxiliares
particionPorSigno :: List Int -> (List Int, List Int)
particionPorSigno xs = (positivos xs, negativos xs)

positivos :: List Int -> List Int
positivos Nil = Nil
positivos (Cons x xs) =
  if x > 0
    then Cons x (positivos xs)
    else positivos xs

negativos :: List Int -> List Int
negativos Nil = Nil
negativos (Cons x xs) =
  if x < 0
    then Cons x (negativos xs)
    else negativos xs

-- dada una lista xs de enteros, devuelve una tupla de listas,
-- donde la primera componente contiene todos los numeros pares de xs
-- y la segunda todos los numeros impares de xs
-- ¿conviene utilizar recursion?
-- considere utilizar funciones auxiliares
particionPorParidad :: List Int -> (List Int, List Int)
particionPorParidad xs = (pares xs, impares xs)

pares :: List Int -> List Int
pares Nil = Nil
pares (Cons x xs) =
  if esPar x
    then Cons x (pares xs)
    else pares xs

impares :: List Int -> List Int
impares Nil = Nil
impares (Cons x xs) =
  if esPar x
    then impares xs
    else Cons x (impares xs)

esPar :: Int -> Bool
esPar n = (n `mod` 2 ) == 0

-- dada una lista, devuelve cada sublista resultante de aplicar tail
-- en cada paso
-- ejemplo:
-- subtails "abc" == ["abc", "bc", "c",""]
subtails :: List a -> List (List a)
subtails Nil = Cons Nil Nil
subtails (Cons x xs) = Cons (Cons x xs) (subtails xs)

-- dada una lista xs, devuelve una lista de listas
-- donde cada sublista contiene elementos contiguos iguales de xs
-- ejemplo:
-- agrupar "AABCCC" = ["AA","B","CC"]
agrupar :: Eq a => List a -> List (List a)
agrupar Nil = Nil
agrupar (Cons x xs) = agru x Nil (Cons x xs)

agru :: Eq a => a -> List a -> List a -> List (List a)
agru e es Nil = Cons es Nil
agru e es (Cons x xs) =
  if x == e
    then agru e (Cons e es) xs
    else Cons es (agru x (Cons x Nil) xs)

-- v de Roman:
agruparR :: Eq a => List a -> List (List a)
agruparR  xs = agruparR' xs Nil

agruparR' :: Eq a => List a -> List a -> List (List a)
agruparR' Nil acc = Cons acc Nil
agruparR' (Cons x xs) Nil = agruparR' xs (Cons x Nil)
agruparR' (Cons x xs) (Cons a acc) =
  if (x == a)
    then agruparR' xs (Cons x (Cons a acc))
    else Cons (Cons a acc) (agruparR' xs (Cons x Nil))

-- v de Viso:
agruparV :: Eq a => List a -> List (List a)
agruparV Nil = Nil
agruparV (Cons x Nil) = Cons (Cons x Nil) Nil
agruparV (Cons x (Cons y xs)) =
  if x == y
    then agregarAlPrimero x (agruparV (Cons y xs))
    else Cons (Cons x Nil) (agruparV (Cons y xs))

agregarAlPrimero :: a -> List (List a) -> List (List a)
agregarAlPrimero x Nil = Cons (Cons x Nil) Nil
agregarAlPrimero x (Cons xs xss) = Cons (Cons x xs) xss

-- devuelve True si la primera lista es prefijo de la segunda
esPrefijo :: Eq a => List a -> List a -> Bool
esPrefijo Nil ys = True
esPrefijo xs Nil = False
esPrefijo (Cons x xs) (Cons y ys) =
  if x == y
    then esPrefijo xs ys
    else False

-- v de Román
esPrefijoR :: Eq a => List a -> List a -> Bool
esPrefijoR Nil ys = True
esPrefijoR xs Nil = False
esPrefijoR (Cons x xs) (Cons y ys) = (x == y) && (esPrefijoR xs ys)

-- devuelve True si la primera lista es sufijo de la segunda
esSufijo :: Eq a => List a -> List a -> Bool
esSufijo xs ys = esPrefijo (reversa xs) (reversa ys)

-- v de Ramiro
esSufijoR :: Eq a => List a -> List a -> Bool
esSufijoR Nil xs = True
esSufijoR xs Nil = False
esSufijoR xs ys =
  if (last' xs == last' ys)
    then esSufijoR (init' xs) (init' ys)
    else False

init' :: List a -> List a
init' Nil = error "lista vacia"
init' (Cons x Nil) = Nil
init' (Cons x xs) = Cons x (init' xs)

last' :: List a -> a
last' Nil = error "lista vacia"
last' (Cons x Nil) = x
last' (Cons x xs) = last' xs

-- propuesto en clase, no es parte de la guía
-- se resuelve en forma similar a agruparV
-- escribir la funcion partes de una lista, donde:
-- partes [1,2,3] = [[], [1], [2], [3], [1,2], [1,3], [2,3], [1,2,3]]
-- sin importar el orden de los elementos
partes :: List a -> List (List a)
partes Nil = Cons Nil Nil
partes (Cons x xs) = append (agregarATodos x (partes xs)) (partes xs)

agregarATodos :: a -> List (List a) -> List (List a)
agregarATodos e Nil = Nil
agregarATodos e (Cons xs xss) = Cons (Cons e xs) (agregarATodos e xss)


---------
-- mias
---------

-- construye una List a partir de una lista
buildList :: [a] -> List a
buildList [] = Nil
buildList (x:xs) = Cons x (buildList xs)

-- crea una lista normal a partir de una List, para poder "verla" mejor
unbuild :: List a -> [a]
unbuild Nil = []
unbuild (Cons x xs) = x : (unbuild xs)

-- dada una List (List a), la convierte en [[a]], para poder "verla" mejor
unbuildLoL :: List (List a) -> [[a]]
unbuildLoL Nil = []
unbuildLoL (Cons xs xss) = (unbuild xs) : (unbuildLoL xss)
