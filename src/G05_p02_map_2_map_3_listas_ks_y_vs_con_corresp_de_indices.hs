module G05_p02_map_2_map_3_listas_ks_y_vs_con_corresp_de_indices (
  Map, emptyM, assocM, lookupM, deleteM, domM
) where

data Map k v = M [k] [v]
  deriving (Show, Eq)

-- retorna un mapa vacio
-- orden 1
emptyM :: Map k v
emptyM = M [] []

-- asocia una clave k0 a un nuevo valor v0 en el mapa
-- orden N
assocM :: Eq k => Map k v -> k -> v -> Map k v
assocM (M ks vs) k0 v0 = M ks_con_k0 vs_con_v0
  where
    idx = buscarIndice ks k0
    ks_con_k0 =
      if idx > 0
        then ks
        else k0:ks
    vs_con_v0 =
      if idx > 0
        then reemplazarEnPos vs idx v0
        else v0:vs

buscarIndice :: Eq k => [k] -> k -> Int
buscarIndice ks k = buscarIndiceRec ks k 1

buscarIndiceRec :: Eq k => [k] -> k -> Int -> Int
buscarIndiceRec [] k0 idx = 0
buscarIndiceRec (k:ks) k0 idx =
  if k == k0
    then idx
    else buscarIndiceRec ks k0 (idx + 1)

reemplazarEnPos :: [v] -> Int -> v -> [v]
reemplazarEnPos [] n v0 = error "lista vacia"
reemplazarEnPos (v:vs) 1 v0 = v0:vs
reemplazarEnPos (v:vs) n v0 = v : (reemplazarEnPos vs (n - 1) v0)

-- retorna el valor v asociado a la clave k0
-- orden N
lookupM :: Eq k => Map k v -> k -> Maybe v
lookupM (M ks vs) k0 = recuperarEnPos vs (buscarIndice ks k0)

recuperarEnPos :: [v] -> Int -> Maybe v
recuperarEnPos [] idx = Nothing
recuperarEnPos (v:vs) 1 = Just v
recuperarEnPos (v:vs) idx = recuperarEnPos vs (idx - 1)

-- elimina el valor asociado a la clave k0
-- orden N
deleteM :: Eq k => Map k v -> k -> Map k v
deleteM (M ks vs) k0 = M ks_sin_k0 vs_sin_v_de_k0
  where
    idx = buscarIndice ks k0
    ks_sin_k0 =
      if idx > 0
        then borrarEnPos ks idx
        else ks
    vs_sin_v_de_k0 =
      if idx > 0
        then borrarEnPos vs idx
        else vs
        
borrarEnPos :: [a] -> Int -> [a]
borrarEnPos [] n = error "lista vacia"
borrarEnPos (x:xs) 1 = xs
borrarEnPos (x:xs) n = x : (borrarEnPos xs (n - 1))

-- retorna el conjunto de claves
-- orden 1
domM :: Eq k => Map k v -> [k]
domM (M ks vs) = ks
