module Adicionales10 where

import G05_p06_roman_adicionales_10_adt_archivo

{-
Ejercicios adicionales de tipos de datos Abstractos, arboles, recursion

Tiene un nivel que es una pizca mas picante que el de los barcos y las
bifurcaciones

Roman Garcia
-}

{-
Vamos a modelar un sistema de archivos, donde los archivos se encuentran en
carpetas y las carpetas pueden contener otras carpetas
-}
data FS =
  Carpeta Nombre
          [Archivo]
          [FS]

type Nombre = String

{-
Así, si nuestro FileSystem tiene la siguiente estructura de directorio:

Index.html         19276 bytes, TEXTO
Logo.jpg           56734 bytes, GRAFICO
Css/Style.css      5433 bytes,  TEXTO
Css/Flechita.jpg   34222 bytes, GRAFICO

lo vamos a representar de la siguiente forma:
-}
index = crearArchivo "Index.html" 19276 Texto

logo = crearArchivo "Logo.jpg" 56734 Grafico

style = crearArchivo "Style.css" 5433 Texto

flecha = crearArchivo "Flechita.jpg" 34222 Grafico

fileSystem = Carpeta "" [index, logo] [Carpeta "Css" [style, flecha] []]

{-
Esta estructura es un arbol, que puede tener una cantidad cualquiera de
hijos.
Cada nodo del arbol es una carpeta (o directorio), que tiene una lista
de archivos y una lista de carpetas (o subdirectorios)
-}

-- Las primeras son gratis:
-- Calcular el espacio ocupado por todos los archivos del file system
-- Como lo sufrieron Carlos, Lorena y Emiliano, podemos hacerlo de dos
-- formas:
-- VAMOS A VER LA DIFICIL EN LOS TRES PRIMEROS EJERCICIOS:

-- *** EJERCICIO 1:
--Test: espacioOcupado fileSystem -> 115665
espacioOcupado :: FS -> Int
espacioOcupado (Carpeta _ archs carpetas) =
  pesoArchivos archs + pesoCarpetas carpetas

-- Hacemos las recursion que estamos acostumbrados de listas
pesoArchivos :: [Archivo] -> Int
pesoArchivos [] = 0
pesoArchivos (a:as) = peso a + pesoArchivos as

-- Otra recursion de las de siempre
pesoCarpetas :: [FS] -> Int
pesoCarpetas [] = 0
pesoCarpetas (fs:fss) = espacioOcupado fs + pesoCarpetas fss

--AJA!!! Acá pasó algo interesante. Para calcular el peso de las
-- carpetas, usé una función que estoy tratando de hacer (espacioOcupado)

-- *** EJERCICIO 2:
-- Otra: determinar si en el fileSystem hay algun archivo con el nombre
-- dado
--Test: estaArchivoEnFS "pepe.jpg" fileSystem -> False
--Test: estaArchivoEnFS "Style.css" fileSystem -> TrueestaArchivoEnFS :: String -> FS -> Bool
estaArchivoEnFS nom (Carpeta _ archs carpetas) =
  estaArchivoEnLista nom archs || estaArchivoEnCarpetas nom carpetas

-- Las funciones auxiliares son sencillas
estaArchivoEnLista :: String -> [Archivo] -> Bool
estaArchivoEnLista nom [] = False
estaArchivoEnLista nom (a:as) =
  if (nombre a == nom)
    then True
    else estaArchivoEnLista nom as

-- Las funciones auxiliares son sencillas, pero OJO! que les puede
-- explotar la cabeza por la recursion que les queda!
estaArchivoEnCarpetas :: String -> [FS] -> Bool
estaArchivoEnCarpetas nom [] = False
estaArchivoEnCarpetas nom (fs:fss) =
  estaArchivoEnFS nom fs || estaArchivoEnCarpetas nom fss

-- *** EJERCICIO 3: una gotita de pura maldad:
--Calcular el peso del primer archivo que encontremos que tenga un
-- nombre dado
-- PRECONDICION: El archivo existe
-- Bueno, dividamos el trabajo. Que mi futuro yo lo encuentro y
-- simplemente le calculo en peso
--Test: pesoDe "Style.css" fileSystem -> 5433
--Test: pesoDe "pepe.css" fileSystem  -> BOOM!! (que está bien)
pesoDe :: String -> FS -> Int
pesoDe nom fs = peso (buscarArchivoEnFS nom fs)

-- Ahora viene lo trabajoso:
buscarArchivoEnFS :: String -> FS -> Archivo
buscarArchivoEnFS nom (Carpeta _ archs carpetas) =
  if (estaArchivoEnLista nom archs)
    then buscarArchivoEnLista nom archs
    else buscarArchivoEnCarpetas nom carpetas

-- UPA!! Parece ser una solución, pero necesita al menos tres funciones
-- auxiliares: estaArchivoEnLista (que está hecha arriba),
-- buscarArchivoEnLista y buscarArchivoEnCarpetas
-- Seguro no hay una forma más facil?
-- PRECONDICION: El archivo esta en la lista
buscarArchivoEnLista :: String -> [Archivo] -> Archivo
buscarArchivoEnLista nom (a:as) =
  if (nombre a == nom)
    then a
    else buscarArchivoEnLista nom as

-- Ver como se hace un doble pattern matching: un elemento de una lista
-- que lo decompongo para ver sus archivos y carpetas
buscarArchivoEnCarpetas :: String -> [FS] -> Archivo
buscarArchivoEnCarpetas nom (fs:fss) =
  if estaArchivoEnFS nom fs
    then buscarArchivoEnFS nom fs
    else buscarArchivoEnCarpetas nom fss

----------------------- FELICITACIONES!!!! ----------------------
------------ LLegaste hasta acá y todavia estás vivo ------------
{-
Ahora si, LA FORMA FACIL. El problema en las tres funciones que hicimos
es que nos metimos demasiado en el problema en particular.

Muchas veces vale la pena pensar si resolviendo algo mas general
solucionamos nuestro problema particular.

Por ejemplo, si tuvieramos una función que nos de todos los archivos
de un FileSystem, no sería todo MUUUUCHO mas simple?
-}
--Test: todosLosArchivos fileSystem ->  [A "Index.html" 19276 Texto,
--                                       A "Logo.jpg" 56734 Grafico,
--                                       A "Style.css" 5433 Texto,
--                                       A "Flechita.jpg" 34222 Grafico]
todosLosArchivos :: FS -> [Archivo]
todosLosArchivos (Carpeta _ archs carpetas) =
  archs ++ todosLosArchivosEnCarpetas carpetas

todosLosArchivosEnCarpetas :: [FS] -> [Archivo]
todosLosArchivosEnCarpetas [] = []
todosLosArchivosEnCarpetas (fs:fss) =
  todosLosArchivos fs ++ todosLosArchivosEnCarpetas fss

-- Ahora podemos disfrutar de las bondades de la funcion todosLosArchivos
-- *** EJERCICIO 1 FACIL
espacioOcupado' :: FS -> Int
espacioOcupado' fs = sumarPeso (todosLosArchivos fs)
  where
    sumarPeso [] = 0
    sumarPeso (a:as) = peso a + sumarPeso as

-- *** EJERCICIO 2 FACIL
estaArchivoEnFS' nom fs = esta nom (todosLosArchivos fs)
  where
    esta nom [] = False
    esta nom (a:as) = (nombre a == nom) || esta nom as

-- *** EJERCICIO 3 FACIL
pesoDe' nom fs = peso (buscar nom (todosLosArchivos fs))
  where
    buscar nom (a:as) =
      if (nombre a == nom)
        then a
        else buscar nom as
--OJO!! Esto sirve siempre y cuando no nos importe la estructura del
--arbol que hemos pasado una aplanadora

-- ************* EJERCICIOS PARA LA CASA *****************
--cantidadArchDeTipo :: TipoDeArchivo -> FS -> Int

--archivosQueEmpiezanCon :: String -> FS -> [Archivo]

--Desafio: Obtener la lista de los nombre de los archivos completos.
--  El nombre completo de "Style.css" es "\css\Style.css"
--nombresCompletos :: FS -> [String]
