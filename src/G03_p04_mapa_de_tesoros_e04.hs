import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe


data Dir
  = Izq
  | Der
  deriving (Show)

data Objeto
  = Tesoro
  | Chatarra
  deriving (Eq, Show)

data Mapa
  = Cofre [Objeto]
  | Bifurcacion [Objeto]
                Mapa
                Mapa
  deriving (Show)

-- datos para tests

mapa1 = Bifurcacion [Chatarra]
          (Bifurcacion [Chatarra, Chatarra]
            (Bifurcacion [Chatarra]
              (Cofre [Chatarra, Chatarra])
              (Cofre [Chatarra])
            )
            (Cofre [Tesoro, Chatarra])
          )
          (Bifurcacion [Chatarra]
            (Cofre [Chatarra, Chatarra])
            (Cofre [Chatarra])
          )

dir1 = [Izq, Der]

mapa2 = Bifurcacion [Chatarra]
          (Bifurcacion [Chatarra, Chatarra]
            (Cofre [Chatarra])
            (Cofre [Chatarra, Chatarra])
          )
          (Bifurcacion [Chatarra]
            (Bifurcacion [Tesoro, Tesoro]
              (Cofre [Chatarra])
              (Cofre [Chatarra, Chatarra])
            )
            (Cofre [Chatarra])
          )

dir2 = [Der, Izq]

mapa3 = Bifurcacion [Chatarra]
          (Bifurcacion [Chatarra, Chatarra]
            (Cofre [Chatarra])
            (Cofre [Chatarra, Chatarra])
          )
          (Cofre [Chatarra])

-- indica la cantidad de tesoros al final del camino
cantidadEn :: [Dir] -> Mapa -> Int
cantidadEn [] (Cofre os) = cantidadDeTesorosEn os
cantidadEn [] (Bifurcacion os m1 m2) = cantidadDeTesorosEn os
cantidadEn (Izq:ds) (Bifurcacion os m1 m2) = cantidadEn ds m1
cantidadEn (Der:ds) (Bifurcacion os m1 m2) = cantidadEn ds m2

cantidadDeTesorosEn :: [Objeto] -> Int
cantidadDeTesorosEn [] = 0
cantidadDeTesorosEn (o:os) =
  if (o == Tesoro)
    then 1 + cantidadDeTesorosEn os
    else cantidadDeTesorosEn os

-- inserta un objeto en la lista el final del camino
insertarEn :: Objeto -> [Dir] -> Mapa -> Mapa
insertarEn o [] (Cofre os) = Cofre (o:os)
insertarEn o [] (Bifurcacion os m1 m2) = Bifurcacion (o:os) m1 m2
insertarEn o (Izq:ds) (Bifurcacion os m1 m2) = Bifurcacion os (insertarEn o ds m1) m2
insertarEn o (Der:ds) (Bifurcacion os m1 m2) = Bifurcacion os m1 (insertarEn o ds m2)

-- recolecta todos los tesoros que hay por ese camino,
-- incluyendo los que haya al final
recolectarTesoros :: [Dir] -> Mapa -> [Objeto]
recolectarTesoros [] (Cofre os) = recolectarTesorosEn os
recolectarTesoros [] (Bifurcacion os m1 m2) = recolectarTesorosEn os
recolectarTesoros (Izq:ds) (Bifurcacion os m1 m2) = (recolectarTesorosEn os) ++ (recolectarTesoros ds m1)
recolectarTesoros (Der:ds) (Bifurcacion os m1 m2) = (recolectarTesorosEn os) ++ (recolectarTesoros ds m2)

recolectarTesorosEn :: [Objeto] -> [Objeto]
recolectarTesorosEn [] = []
recolectarTesorosEn (o:os) =
  if o == Tesoro
    then Tesoro : (recolectarTesorosEn os)
    else recolectarTesorosEn os


---------
-- mias
---------

-- genera un mapa a partir de una lista de listas de objetos
genMapa :: [[Objeto]] -> Mapa
genMapa oss =
  if (length oss < 1) || (((length oss) `mod` 2) == 0)
    then error "cantidad nodos debe ser impar y mayor que 0"
    else list2Mapa (tail oss) (Cofre (head oss))

list2Mapa :: [[Objeto]] -> Mapa -> Mapa
list2Mapa [] r = r
list2Mapa (os1:(os2:oss)) m = list2Mapa oss (agregarRal os1 os2 m)

agregarRal :: [Objeto] -> [Objeto] -> Mapa -> Mapa
agregarRal os1 os2 m = agregarEnPos (genDirs (sizeR m)) os1 os2 m

agregarEnPos :: [Char] -> [Objeto] -> [Objeto] -> Mapa -> Mapa
agregarEnPos ds os1 os2 (Cofre os)
  = Bifurcacion os (Cofre os1) (Cofre os2)
agregarEnPos [] os1 os2 (Bifurcacion os mi md) = error "ubicacion ya ocupada"
agregarEnPos (d:ds) os1 os2 (Bifurcacion os mi md) =
  if (d == 'i')
    then Bifurcacion os (agregarEnPos ds os1 os2 mi) md
    else Bifurcacion os mi (agregarEnPos ds os1 os2 md)

genDirs :: Int -> [Char]
genDirs n = map int2dir (nums n)

int2dir :: Int -> Char
int2dir i =
  if (i < 0)
    then 'i'
    else 'd'

-- cant de nodos del Mapa
sizeR :: Mapa -> Int
sizeR (Cofre os) = 1
sizeR (Bifurcacion os mi md) = 1 + sizeR mi + sizeR md

-- genera una lista de N [Objeto] (de long entre 0 y 3)
genNObjetos :: Int -> [[Objeto]]
genNObjetos 0 = []
genNObjetos n = (genObjetos (head (genNums 2 3))) : genNObjetos (n - 1)

-- genera N Objetos
genObjetos :: Int -> [Objeto]
genObjetos n = map int2Objeto (genNums n 1)

int2Objeto :: Int -> Objeto
int2Objeto 0 = Tesoro
int2Objeto 1 = Chatarra

-- genera N1 numeros aleatorios entre 0 y N2 (repeticiones permitidas)
genNums :: Int -> Int -> [Int]
genNums n1 n2 = map (`mod` (n2 + 1)) (nums n1)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]


-----------------------------------------------------
-- impresion de mapas
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Mapa -> IO ()
prettyprint t = putStrLn (unlines (prettyprint_helper t))
-- unlines concats a list with newlines

prettyprint_helper :: Mapa -> [String]
prettyprint_helper (Cofre o) = [show o]
prettyprint_helper (Bifurcacion o ti td)
  = (show o) : (prettyprint_subtree ti td)

prettyprint_subtree :: Mapa -> Mapa -> [String]
prettyprint_subtree ti td
  = ((pad "├──" "│  ") (prettyprint_helper td))
  ++ ((pad "╰──" "   ") (prettyprint_helper ti))

pad :: String -> String -> [String] -> [String]
pad first rest = zipWith (++) (first : repeat rest)
