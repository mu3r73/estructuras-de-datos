module G04_p06_stack_e06_stack_con_max_t_const (
  Stack, emptyS, isEmptyS, push, top, pop, maxS, list2stack
) where

data Stack a =
  S [a] -- elem
    [a] -- max
  deriving (Show, Eq)

-- crea una pila vacia
emptyS :: Stack a
emptyS = S [] []

-- dada una pila, indica si esta vacia
isEmptyS :: Stack a -> Bool
isEmptyS (S es ms) = esVacia es

esVacia :: [a] -> Bool -- isNil
esVacia [] = True
esVacia es = False

-- dados un elemento y una pila, agrega el elemento a la pila
push :: Ord a => a -> Stack a -> Stack a
push e (S es ms) = S (e:es) (agregarNuevoMax e ms)

agregarNuevoMax :: Ord a => a -> [a] -> [a]
agregarNuevoMax e [] = [e]
agregarNuevoMax e (m:ms) = (maximo e m) : (m:ms)

maximo :: Ord a => a -> a -> a
maximo n m =
  if n > m
    then n
    else m

-- dada un pila, devuelve el elemento del tope de la pila
top :: Stack a -> a
top (S es ms) = primero es

primero :: [a] -> a -- head
primero [] = error "lista vacia"
primero (x:xs) = x

-- dada una pila, la devuelve sin su primer elemento
pop :: Stack a -> Stack a
pop (S es ms) = S (sinPrimero es) (sinPrimero ms)

sinPrimero :: [a] -> [a] -- tail
sinPrimero [] = error "lista vacia"
sinPrimero (x:xs) = xs

-- devuelve el elemento maximo de la pila
maxS :: Ord a => Stack a -> a
maxS (S es ms) = primero ms


-- mias

list2stack :: Ord a => [a] -> Stack a
list2stack [] = emptyS
list2stack xs = push (last xs) (list2stack (init xs))
