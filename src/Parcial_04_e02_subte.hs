module Parcial_04_e02_subte (
  Subte, crear, mover, puedeMover, estacionActual,
  subir, bajar, nroPersonasT, hayPersonasT
) where

import G05_p04_arboles_de_busqueda_6_1_1_map_usando_BST as Map
import Parcial_04_persona as Persona
import Parcial_04_sentido as Sentido
import Parcial_04_e03_estacion

-- Ejercicio 2
-- Implementar el tipo abstracto Subte, dando una representación
-- adecuada y su correspondiente invariante de representación. Respetar
-- la interfaz y los órdenes de complejidad pedidos.

data Subte =
  S Int Int (Map Int Estacion) Tren
  deriving (Show)

type Tren = Estacion

{-
INV. REP.: Dado (S ce ea m t):
  * ce (cantidad de estaciones) > 0
  * ce >= ea (estación actual) > 0
  * m (Map Int Estacion):
      cada estación se asocia a una clave única, empezando desde 1,
      e incrementándola en 1 para estaciones sucesivas
  * t (tren)
-}


-- La interfaz del tipo Subte es la siguiente:

-- crea un subte vacío, dejando el tren en la primera estación
-- del recorrido
-- O(n * log n)
crear :: Int -> Subte
crear n =
  if n <= 0
    then error "cantidad de estaciones debe ser >= 0"
    else S n 1 (crearEstaciones n emptyM) vacia

crearEstaciones :: Int -> Map Int Estacion -> Map Int Estacion
crearEstaciones 0 m = m
crearEstaciones n m = crearEstaciones (n - 1) (assocM m n vacia)

-- mueve el tren en el sentido indicado
-- O(1)
-- PRECOND: el tren puede moverse en el sentido indicado
mover :: Sentido -> Subte -> Subte
mover Adelante (S ce ea m t) = S ce (ea + 1) m t
mover Atras (S ce ea m t) = S ce (ea - 1) m t

-- indica si el tren puede moverse en el sentido dado
-- O(1)
puedeMover :: Sentido -> Subte -> Bool
puedeMover Adelante (S ce ea m t) = ea < ce
puedeMover Atras (S ce ea m t) = ea > 1

-- retorna la estación actual del subte
-- O(log s)
estacionActual :: Subte -> Estacion
estacionActual (S ce ea m t) = fromJust (lookupM m ea)

fromJust :: Maybe a -> a
fromJust (Just x) = x

-- sube una persona de la estación actual al tren
-- O(log s)
-- PRECOND: hay al menos una persona del tipo indicado en la estación
-- actual
subir :: Persona -> Subte -> Subte
subir p s@(S ce ea m t) =
  S ce ea (assocM m ea (salir p (estacionActual s))) (entrar p t)

-- baja una persona del tren a la estación actual
-- O(log s)
-- PRECOND: hay al menos una persona del tipo indicado en el tren
bajar :: Persona -> Subte -> Subte
bajar p s@(S ce ea m t) =
  S ce ea (assocM m ea (entrar p (estacionActual s))) (salir p t)

-- retorna el número de personas del tipo indicado que se encuentran
-- dentro del tren
-- O(1)
nroPersonasT :: Persona -> Subte -> Int
nroPersonasT p (S ce ea m t) = nroPersonas p t

-- indica si hay personas del tipo dado dentro del tren
-- O(1)
hayPersonasT :: Persona -> Subte -> Bool
hayPersonasT p (S ce ea m t) = hayPersonas p t


-- ¿Cómo se podría implementar el Map utilizado, para garantizar los
-- órdenes de complejidad pedidos?

{-
Para garantizar los órdenes de complejidad pedidos, se puede implementar
el map usando un BST balanceado. Por ejemplo, un árbol AVL.
-}
