import G04_p01_conjunto_e02_conj_sin_repet
import G03_p01_arboles_binarios

-- datos para tests
l1 :: [Int]
l1 = [1 .. 10]

c1 :: Conjunto Int
c1 = list2set [2, 4, 6, 8]

c2 :: Conjunto Int
c2 = list2set [3, 6, 9]

c3 :: Conjunto Int
c3 = list2set [1, 2, 4, 8]

c4 :: Conjunto Int
c4 = list2set [1, 3, 9]

c5 :: Conjunto Int
c5 = list2set [1, 3, 5, 7, 9]

c6 :: Conjunto Int
c6 = list2set [4, 8]

tconj :: Tree (Conjunto Int)
tconj = treeFromList agregarTbal [c1, c2, c3, c4, c5, c6]

tint1 :: Tree Int
tint1 = treeFromList agregarTbal [1 .. 6]

tint2 :: Tree Int
tint2 = treeFromList agregarTbal [4 .. 9]

-- dados una lista y un conjunto, devuelve una lista
-- con todos los elementos que pertenecen al conjunto
losQuePertenecen :: Eq a => [a] -> Conjunto a -> [a]
losQuePertenecen [] c = []
losQuePertenecen (x:xs) c =
  if perteneceC x c
    then x : (losQuePertenecen xs c)
    else losQuePertenecen xs c

-- quita todos los elementos repetidos de la lista dada,
-- utilizando un conjunto como estructura auxiliar
sinRepetidos :: Ord a => [a] -> [a]
sinRepetidos xs = sinRepetConj xs vacioC

sinRepetConj :: Ord a => [a] -> Conjunto a -> [a]
sinRepetConj [] c = []
sinRepetConj (x:xs) c =
  if perteneceC x c
    then sinRepetConj xs c
    else x : (sinRepetConj xs (agregarC x c))

-- dado un arbol de conjuntos, devuelve un conjunto
-- con la union de todos los conjuntos del arbol
unirTodos :: Ord a => Tree (Conjunto a) -> Conjunto a
unirTodos EmptyT = vacioC
unirTodos (NodeT hi e hd) = unionC e (unionC (unirTodos hi) (unirTodos hd))

-- dados dos arboles, devuelve un conjunto
-- con los elementos que ambos arboles tienen en comun
interseccionArbol :: Ord a => Tree a -> Tree a -> Conjunto a
interseccionArbol EmptyT t2 = vacioC
interseccionArbol t1 EmptyT = vacioC
interseccionArbol (NodeT hi e hd) t2 =
  if perteneceT e t2
    then agregarC e (unionC (interseccionArbol hi t2) (interseccionArbol hd t2))
    else unionC (interseccionArbol hi t2) (interseccionArbol hd t2)
