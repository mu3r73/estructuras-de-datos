module G04_p03_stack_e02_stack_usando_listas (
  Stack, emptyS, isEmptyS, push, top, pop, list2stack
) where

data Stack a =
  S [a]
  deriving (Show, Eq)

-- crea una pila vacia
emptyS :: Stack a
emptyS = S []

-- dada una pila, indica si esta vacia
isEmptyS :: Stack a -> Bool
isEmptyS (S es) = esVacia es

esVacia :: [a] -> Bool -- isNil
esVacia [] = True
esVacia es = False

-- dados un elemento y una pila, agrega el elemento a la pila
push :: a -> Stack a -> Stack a
push e (S es) = S (e:es)

-- dada un pila, devuelve el elemento del tope de la pila
top :: Stack a -> a
top (S es) = primero es

primero :: [a] -> a -- head
primero [] = error "lista vacia"
primero (x:xs) = x

-- dada una pila, la devuelve sin su primer elemento
pop :: Stack a -> Stack a
pop (S es) = S (sinPrimero es)

sinPrimero :: [a] -> [a] -- tail
sinPrimero [] = error "lista vacia"
sinPrimero (x:xs) = xs


-- mias

list2stack :: [a] -> Stack a
list2stack [] = emptyS
list2stack xs = push (last xs) (list2stack (init xs))
