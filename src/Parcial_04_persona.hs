module Parcial_04_persona (
  Persona(Hombre, Mujer, Ninho)
) where

data Persona
  = Hombre
  | Mujer
  | Ninho
  deriving (Show, Eq)

instance Ord Persona where
  compare Ninho Ninho = EQ
  compare Ninho p = LT
  compare Mujer Mujer = EQ
  compare Mujer Ninho = GT
  compare Mujer p = LT
  compare Hombre Hombre = EQ
  compare Hombre p = GT

{-
instance Ord Persona where
  Ninho <= Ninho = True
  Ninho <= p = True
  Mujer <= Mujer = True
  Mujer <= Hombre = True
  Mujer <= Ninho = False
  Hombre <= Hombre = True
  Hombre <= p = False
-}
