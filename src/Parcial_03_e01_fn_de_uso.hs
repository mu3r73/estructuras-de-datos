import Parcial_03_e02_multiset_v1_usando_lista_simple
import G04_p03_stack_e02_stack_usando_listas

-- datos para tests

ms1 = list2ms [1 .. 5]
ms2 = list2ms [3 .. 7]
ms3 = list2ms [6 .. 8]

s = list2stack [ms1, ms2, ms3]


-- dada una stack de multiconjuntos, devuelve un multiconjunto con la
-- union de todos los multiconjuntos de la stack
unirTodos :: Eq a => Stack (MultiSet a) -> MultiSet a
unirTodos s =
  if isEmptyS s
    then emptyMS
    else unionMS (top s) (unirTodos (pop s))
