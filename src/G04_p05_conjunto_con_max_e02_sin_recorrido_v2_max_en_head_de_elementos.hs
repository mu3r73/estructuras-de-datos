module G04_p05_conjunto_con_max_e02_sin_recorrido_v2_max_en_head_de_elementos (
  Conjunto, vacioC, agregarC, perteneceC, cantidadC, borrarC, unionC,
  listaC, maximoC, list2set
) where

data Conjunto a =
  Set Int -- cant de elem
      [a] -- elem
  deriving (Show, Eq)

-- crea un conjunto vacio
vacioC :: Conjunto a
vacioC = Set 0 []

-- dados un elemento y un conjunto, agrega el elemento al conjunto
agregarC :: Ord a => a -> Conjunto a -> Conjunto a
agregarC e (Set 0 []) = Set 1 [e]
agregarC e (Set n (x:xs)) =
  if pertenece e (x:xs)
    then Set n (x:xs)
    else
      if e > x
        then Set (n+1) (e:(x:xs))
        else Set (n+1) (x:(e:xs))

pertenece :: Eq a => a -> [a] -> Bool
pertenece e [] = False
pertenece e (x:xs) = (e == x) || pertenece e xs

-- dados un elemento y un conjunto,
-- indica si el elemento pertenece al conjunto
perteneceC :: Eq a => a -> Conjunto a -> Bool
perteneceC e (Set n es) = pertenece e es

-- devuelve la cantidad de elementos distintos de un conjunto
cantidadC :: Eq a => Conjunto a -> Int
cantidadC (Set n es) = n

-- devuelve el conjunto sin el elemento dado
borrarC :: Ord a => a -> Conjunto a -> Conjunto a
borrarC e (Set n []) = Set n []
borrarC e (Set n (x:xs)) =
  if e == x
    then Set (n - 1) ((maximoL xs) : (borrarL (maximoL xs) xs))
    else
      if (pertenece e xs)
        then Set (n - 1) (x : (borrarL e xs))
        else Set n (x:xs)

borrarL :: Eq a => a -> [a] -> [a]
borrarL e [] = []
borrarL e (x:xs) =
  if e == x
    then xs
    else x : (borrarL e xs)

maximoL :: Ord a => [a] -> a
maximoL [] = error "lista vacia"
maximoL [x] = x
maximoL (x:xs) = maximo x (maximoL xs)

maximo :: Ord a => a -> a -> a
maximo n m =
  if n > m
    then n
    else m

-- dados dos conjuntos, devuelve un conjunto
-- con todos los elementos de ambos conjuntos
unionC :: Ord a => Conjunto a -> Conjunto a -> Conjunto a
unionC c (Set n es) = agregarListaAConj es c

agregarListaAConj :: Ord a => [a] -> Conjunto a -> Conjunto a
agregarListaAConj [] c = c
agregarListaAConj (x:xs) c = agregarC x (agregarListaAConj xs c)

-- dado un conjunto, devuelve una lista
-- con todos los elementos distintos del conjunto
listaC :: Eq a => Conjunto a -> [a]
listaC (Set n es) = es

-- devuelve el maximo elemento en un conjunto
maximoC :: Ord a => Conjunto a -> a
maximoC (Set n es) = primero es

primero :: [a] -> a -- head
primero [] = error "lista vacia"
primero (x:xs) = x


-- mias

list2set :: Ord a => [a] -> Conjunto a
list2set [] = vacioC
list2set (x:xs) = agregarC x (list2set xs)
