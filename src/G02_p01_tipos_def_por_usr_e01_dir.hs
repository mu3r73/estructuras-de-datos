module G02_p01_tipos_def_por_usr_e01_dir (
  Dir(Norte, Este, Sur, Oeste), opuesto, siguiente
) where

-- tipo de dato Dir, con las alternativas Norte, Sur, Este y Oeste
data Dir
  = Norte
  | Este
  | Sur
  | Oeste
  deriving (Show, Eq)

-- dada una direccion, devuelve su opuesta
opuesto :: Dir -> Dir
opuesto Norte = Sur
opuesto Este = Oeste
opuesto Sur = Norte
opuesto Oeste = Este

-- dada una direccion, devuelve su siguiente, en sentido horario
siguiente :: Dir -> Dir
siguiente Norte = Este
siguiente Este = Sur
siguiente Sur = Oeste
siguiente Oeste = Norte
