module G05_p06_adicionales_11_mini_tablero (
  MiniTablero, Dir(Este, Oeste), Color(Azul, Negro, Rojo, Verde),
  crearFila, mover, puedeMover,
  ponerT, sacarT, nroBolitasT, hayBolitasT,
  G05_p06_adicionales_11_mini_tablero.prettyprint
) where

import G05_p04_arboles_de_busqueda_6_1_1_map_usando_BST as Map
import G05_p02_map_3_celda


data MiniTablero =
  MkT (Map Coord Celda)
      Coord
  deriving (Show)

type Coord = Int

data Dir
  = Este
  | Oeste
  deriving (Show)


-- crea una fila de celdas vacias de tamanio n,
-- apuntando a la primera celda 
crearFila :: Int -> MiniTablero
crearFila n = 
  if n > 0
    then MkT (agregarCeldas n emptyM) 1
    else error "BOOM - tamaño del tablero debe ser > 0"

agregarCeldas :: Coord -> Map Coord Celda -> Map Coord Celda
agregarCeldas 0 m = m
agregarCeldas n m = agregarCeldas (n - 1) (assocM m n celdaVacia)

-- dada una direccion d, mueve el cabezal hacia d
-- parcial cuando no existe una celda en esa direccion
mover :: Dir -> MiniTablero -> MiniTablero
mover Este t@(MkT m c) =
  if (puedeMover Este t)
    then MkT m (c + 1)
    else error "BOOM - no se puede mover el cabezal hacia el este"
mover Oeste t@(MkT m c) =
  if (puedeMover Oeste t)
    then MkT m (c - 1)
    else error "BOOM - no se puede mover el cabezal hacia el oeste"

-- dada una direccion, indica si existe una celda en esa direccion
puedeMover :: Dir -> MiniTablero -> Bool
puedeMover Este (MkT m c) = isJust (lookupM m (c + 1))
puedeMover Oeste (MkT m c) = isJust (lookupM m (c - 1))

isJust :: Maybe a -> Bool
isJust Nothing = False
isJust mb = True

-- pone una bolita del color indicado en la celda actual
ponerT :: Color -> MiniTablero -> MiniTablero
ponerT col (MkT m c) = MkT (assocM m c cel_mod) c
  where
    cel = fromJust (lookupM m c)
    cel_mod = poner col cel

fromJust :: Maybe a -> a
fromJust (Just x) = x

-- saca una bolita del color indicado de la celda actual
sacarT :: Color -> MiniTablero -> MiniTablero
sacarT col (MkT m c) = MkT (assocM m c cel_mod) c
  where
    cel = fromJust (lookupM m c)
    cel_mod = sacar col cel

-- devuelve el numero de bolitas de un color en la celda actual
nroBolitasT :: Color -> MiniTablero -> Int
nroBolitasT col (MkT m c) = nroBolitas col (fromJust (lookupM m c))

-- indica si hay bolitas de un color en la celda actual
hayBolitasT :: Color -> MiniTablero -> Bool
hayBolitasT col (MkT m c) = hayBolitas col (fromJust (lookupM m c))


-----------------------------------------------------
-- impresion de arboles
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: MiniTablero -> IO ()
prettyprint (MkT m c) = Map.prettyprint m
