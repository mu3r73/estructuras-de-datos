module G04_p07_queue_usando_2_stacks (
  Queue, emptyQ, isEmptyQ, queue, firstQ, dequeue, list2queue
) where

import G04_p03_stack_e02_stack_usando_listas

data Queue a =
  Q (Stack a) (Stack a)
  deriving (Show)

-- crea una cola vacia
emptyQ :: Queue a
emptyQ = Q emptyS emptyS

-- dada una cola, indica si la cola esta vacia
isEmptyQ :: Queue a -> Bool
isEmptyQ (Q fs bs) = isEmptyS fs

-- dados un elemento y una cola, agrega ese elemento a la cola
queue :: a -> Queue a -> Queue a
queue e (Q fs bs) = rellenarBSSiEsNecesario (Q fs (push e bs))

rellenarBSSiEsNecesario :: Queue a -> Queue a
rellenarBSSiEsNecesario (Q fs bs) =
  if isEmptyS fs
    then rellenarBS (Q fs bs)
    else Q fs bs

rellenarBS :: Queue a -> Queue a
rellenarBS (Q fs bs) =
  if (isEmptyS bs)
    then Q fs bs
    else rellenarBS (Q (push (top bs) fs) (pop bs))

-- dada una cola, devuelve su primer elemento
firstQ :: Queue a -> a
firstQ (Q fs bs) = top fs

-- dada una cola, la devuelve sin su primer elemento
dequeue :: Queue a -> Queue a
dequeue (Q fs bs) = rellenarBSSiEsNecesario (Q (pop fs) bs)


-- mias

list2queue :: [a] -> Queue a
list2queue [] = emptyQ
list2queue xs = queue (last xs) (list2queue (init xs))
