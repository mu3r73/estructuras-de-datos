module G01_p01_conceptos_basicos_e04 (
  isEmpty, head', tail'
) where

-- dada una lista de elementos, si es vacia devuelve True,
-- si no devuelve False
isEmpty :: [a] -> Bool
isEmpty [] = True
isEmpty (x:xs) = False

-- dada una lista, devuelve su primer elemento
head' :: [a] -> a
head' [] = error "lista vacia"
head' (x:xs) = x

-- dada una lista, devuelve esa lista menos el primer elemento
tail' :: [a] -> [a]
tail' [] = error "lista vacia"
tail' (x:xs) = xs
