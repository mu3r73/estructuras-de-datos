module G04_p02_queue_e02_queue_usando_listas (
  largoQ, atender, unirQ
) where

import G04_p07_queue_usando_2_stacks
import G02_p01_tipos_def_por_usr_e02_persona

-- datos para tests

qint1 :: Queue Int
qint1 = list2queue [1..5]

qint2 :: Queue Int
qint2 = list2queue [6..8]

p1 = Pers "Titi" 22
p2 = Pers "Pepe" 20
p3 = Pers "Toto" 24
p4 = Pers "Mimi" 26

qpers :: Queue Persona
qpers = list2queue [p1, p2, p3, p4]

-- cuenta la cantidad de elementos de la cola
largoQ :: Queue a -> Int
largoQ q =
  if isEmptyQ q
    then 0
    else 1 + largoQ (dequeue q)

-- dada una cola de personas, devuelve su lista correspondiente,
-- donde el orden de la lista es de la cola
atender :: Queue Persona -> [Persona]
atender q =
  if isEmptyQ q
    then []
    else (firstQ q) : (atender (dequeue q))

-- inserta todos los elementos de la segunda cola en la primera
unirQ :: Queue a -> Queue a -> Queue a
unirQ q1 q2 =
  if isEmptyQ q2
    then q1
    else unirQ (queue (firstQ q2) q1) (dequeue q2)
