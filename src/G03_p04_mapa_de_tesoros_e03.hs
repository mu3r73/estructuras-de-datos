import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe


data Dir
  = Izq
  | Der
  deriving (Show)

data Objeto
  = Tesoro
  | Chatarra
  deriving (Eq, Show)

data Mapa
  = Cofre Objeto
  | Bifurcacion Objeto
                Mapa
                Mapa
  deriving (Show)

-- datos para tests

mapa1 = Bifurcacion Chatarra
          (Bifurcacion Chatarra
            (Bifurcacion Chatarra
              (Cofre Chatarra)
              (Cofre Chatarra)
            )
            (Cofre Tesoro)
          )
          (Bifurcacion Chatarra
            (Cofre Chatarra)
            (Cofre Chatarra)
          )

dir1 = [Izq, Der]

mapa2 = Bifurcacion Chatarra
          (Bifurcacion Chatarra
            (Cofre Chatarra)
            (Cofre Chatarra)
          )
          (Bifurcacion Chatarra
            (Bifurcacion Tesoro
              (Cofre Chatarra)
              (Cofre Chatarra)
            )
            (Cofre Chatarra)
          )

dir2 = [Der, Izq]

mapa3 = Bifurcacion Chatarra
          (Bifurcacion Chatarra
            (Cofre Chatarra)
            (Cofre Chatarra)
          )
          (Cofre Chatarra)

-- indica si hay un tesoro en alguna parte del mapa
hayTesoro :: Mapa -> Bool
hayTesoro (Cofre o) = esTesoro o
hayTesoro (Bifurcacion o m1 m2) = (esTesoro o) || (hayTesoro m1) || (hayTesoro m2)

esTesoro :: Objeto -> Bool
esTesoro o = (o == Tesoro)

-- indica si al final del camino hay un tesoro
-- nota: el final del camino es la lista vacia de direcciones
hayTesoroEn :: [Dir] -> Mapa -> Bool
hayTesoroEn [] (Cofre o) = esTesoro o
hayTesoroEn [] (Bifurcacion o m1 m2) = esTesoro o
hayTesoroEn (Izq:ds) (Bifurcacion o m1 m2) = hayTesoroEn ds m1
hayTesoroEn (Der:ds) (Bifurcacion o m1 m2) = hayTesoroEn ds m2
hayTesoroEn (d:ds) (Cofre o) = False

-- indica el camino al tesoro
-- precondicion: hay un solo tesoro en el mapa
caminoAlTesoro :: Mapa -> [Dir]
caminoAlTesoro (Cofre o) = []
caminoAlTesoro (Bifurcacion Tesoro m1 m2) = []
caminoAlTesoro (Bifurcacion o m1 m2) =
  if hayTesoro m1
    then Izq : (caminoAlTesoro m1)
    else Der : (caminoAlTesoro m2)


---------
-- mias
---------

-- genera un mapa a partir de una lista de listas de objetos
genMapa :: [Objeto] -> Mapa
genMapa os =
  if (length os < 1) || (((length os) `mod` 2) == 0)
    then error "cantidad nodos debe ser impar y mayor que 0"
    else list2Mapa (tail os) (Cofre (head os))

list2Mapa :: [Objeto] -> Mapa -> Mapa
list2Mapa [] r = r
list2Mapa (o1:(o2:os)) m = list2Mapa os (agregarRal o1 o2 m)

agregarRal :: Objeto -> Objeto -> Mapa -> Mapa
agregarRal o1 o2 m = agregarEnPos (genDirs (sizeR m)) o1 o2 m

agregarEnPos :: [Char] -> Objeto -> Objeto -> Mapa -> Mapa
agregarEnPos ds o1 o2 (Cofre o)
  = Bifurcacion o (Cofre o1) (Cofre o2)
agregarEnPos [] o1 o2 (Bifurcacion o mi md) = error "ubicacion ya ocupada"
agregarEnPos (d:ds) o1 o2 (Bifurcacion o mi md) =
  if (d == 'i')
    then Bifurcacion o (agregarEnPos ds o1 o2 mi) md
    else Bifurcacion o mi (agregarEnPos ds o1 o2 md)

genDirs :: Int -> [Char]
genDirs n = map int2dir (nums n)

int2dir :: Int -> Char
int2dir i =
  if (i < 0)
    then 'i'
    else 'd'

-- cant de nodos del Mapa
sizeR :: Mapa -> Int
sizeR (Cofre os) = 1
sizeR (Bifurcacion os mi md) = 1 + sizeR mi + sizeR md

-- genera N Objetos
genObjetos :: Int -> [Objeto]
genObjetos n = map int2Objeto (genNums n 1)

int2Objeto :: Int -> Objeto
int2Objeto 0 = Tesoro
int2Objeto 1 = Chatarra

-- genera N1 numeros aleatorios entre 0 y N2 (repeticiones permitidas)
genNums :: Int -> Int -> [Int]
genNums n1 n2 = map (`mod` (n2 + 1)) (nums n1)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]


-----------------------------------------------------
-- impresion de mapas
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Mapa -> IO ()
prettyprint t = putStrLn (unlines (prettyprint_helper t))
-- unlines concats a list with newlines

prettyprint_helper :: Mapa -> [String]
prettyprint_helper (Cofre o) = [show o]
prettyprint_helper (Bifurcacion o ti td)
  = (show o) : (prettyprint_subtree ti td)

prettyprint_subtree :: Mapa -> Mapa -> [String]
prettyprint_subtree ti td
  = ((pad "├──" "│  ") (prettyprint_helper td))
  ++ ((pad "╰──" "   ") (prettyprint_helper ti))

pad :: String -> String -> [String] -> [String]
pad first rest = zipWith (++) (first : repeat rest)
