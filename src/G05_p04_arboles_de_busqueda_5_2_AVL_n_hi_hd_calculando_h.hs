module G05_p04_arboles_de_busqueda_5_2_AVL_n_hi_hd_calculando_h (
  Tree, insertBST, deleteBST, perteneceBST, splitminBST, splitmaxBST,
  elMaximoMenorA, elMinimoMayorA,
  emptyBST, isEmptyBST, isAVL, avlFromList, prettyprint
) where

import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe

-- arbol binario
data Tree a
  = EmptyT
  | NodeT a
          (Tree a)
          (Tree a)
  deriving (Show)

-- invariantes:
-- AVLTree es un BST
-- => EmptyAVL es un BST
-- => NodeAVL n ti td es un BST si:
--    * todo elemento de ti es menor que n
--    * todo elemento de td es mayor que n
--    * ti y td son BST (AVL)
-- ademas, para ser AVL, debe estar balanceado
-- * balanceado = diferencia de altura entre hijos es <= 1

-- no guarda la altura, tiene que recalcularla para cada alta/baja/modif
-- => no cumple con la condicion O(log n)


-- datos para tests
{-
nota: para arboles aleatorios, usar avlFromList (shuffle <lista>),
      ej: avlFromList (shuffle [1..6]), -- sin repetidos
      y asignar expresion resultante a una var
-}

tchar1 :: Tree Char
tchar1 = avlFromList (shuffle ['a', 'c', 'e', 'g', 'i', 'k', 'm'])

tint1 :: Tree Int
tint1 = avlFromList (shuffle [1..7])


-- dado un BST, inserta un elemento en el arbol
insertBST :: Ord a => a -> Tree a -> Tree a
insertBST e EmptyT = NodeT e EmptyT EmptyT
insertBST e t@(NodeT n ti td) =
  if e == n
--    then t -- conserva valor anterior
    then NodeT e ti td -- reemplaza valor anterior
  else
    if e < n
      then balancearSiSeDesbalanceo (NodeT n (insertBST e ti) td)
      else balancearSiSeDesbalanceo (NodeT n ti (insertBST e td))


-- dado un BST, borra un elemento del arbol
deleteBST :: Ord a => a -> Tree a -> Tree a
deleteBST e EmptyT = EmptyT
deleteBST e (NodeT n ti td) =
  if e == n
    then
      balancearSiSeDesbalanceo (rearmarBST ti td)
    else
      if e < n
        then balancearSiSeDesbalanceo (NodeT n (deleteBST e ti) td)
        else balancearSiSeDesbalanceo (NodeT n ti (deleteBST e td))

-- rearma el AVL a partir de sus HI, HD anteriores
rearmarBST :: Ord a => Tree a -> Tree a -> Tree a
rearmarBST EmptyT td = td
rearmarBST ti EmptyT = ti
rearmarBST ti td = rearmarBSTmaxHI ti td

-- promueve maximo del subarbol izquierdo, usando splitmaxBST
rearmarBSTmaxHI :: Ord a => Tree a -> Tree a -> Tree a
rearmarBSTmaxHI ti td = NodeT m ti_sin_m td
  where (m, ti_sin_m) = splitmaxBST ti

-- promueve minimo del subarbol derecho, usando splitminBST
rearmarBSTminHD :: Ord a => Tree a -> Tree a -> Tree a
rearmarBSTminHD ti td = NodeT m ti td_sin_m
  where (m, td_sin_m) = splitminBST td


-- dado un BST, dice si el elemento pertenece o no al arbol
perteneceBST :: Ord a => a -> Tree a -> Bool
perteneceBST e EmptyT = False
perteneceBST e (NodeT n ti td) =
  if e == n
    then True
    else
      if e < n
        then perteneceBST e ti
        else perteneceBST e td


-- dado un BST, devuelve un par con el minimo elemento
-- y el arbol sin el mismo

-- sin fn aux, con where
splitminBST :: Ord a => Tree a -> (a, Tree a)
splitminBST EmptyT = error "arbol vacio"
splitminBST (NodeT n EmptyT td) = (n, td)
splitminBST (NodeT n ti td) =
  (m, balancearSiSeDesbalanceo (NodeT n ti_sin_m td))
    where (m, ti_sin_m) = splitminBST ti

-- con fn aux
splitminBST' :: Ord a => Tree a -> (a, Tree a)
splitminBST' t = (minBST t, sinminBST t)

minBST :: Ord a => Tree a -> a
minBST EmptyT = error "arbol vacio"
minBST (NodeT n EmptyT td) = n
minBST (NodeT n ti td) = minBST ti

sinminBST :: Ord a => Tree a -> Tree a
sinminBST EmptyT = error "arbol vacio"
sinminBST (NodeT n EmptyT td) = td
sinminBST (NodeT n ti td) =
  balancearSiSeDesbalanceo (NodeT n (sinminBST ti) td)


-- dado un BST, devuelve un par con el maximo elemento
-- y el arbol sin el mismo

-- sin fn aux, con where
splitmaxBST :: Ord a => Tree a -> (a, Tree a)
splitmaxBST EmptyT = error "arbol vacio"
splitmaxBST (NodeT n ti EmptyT) = (n, ti)
splitmaxBST (NodeT n ti td) =
  (m, balancearSiSeDesbalanceo (NodeT n ti td_sin_m))
    where (m, td_sin_m) = splitmaxBST td

-- con fn aux
splitmaxBST' :: Ord a => Tree a -> (a, Tree a)
splitmaxBST' t = (maxBST t, sinmaxBST t)

maxBST :: Ord a => Tree a -> a
maxBST EmptyT = error "arbol vacio"
maxBST (NodeT n ti EmptyT) = n
maxBST (NodeT n ti td) = maxBST td

sinmaxBST :: Ord a => Tree a -> Tree a
sinmaxBST EmptyT = error "arbol vacio"
sinmaxBST (NodeT n ti EmptyT) = ti
sinmaxBST (NodeT n ti td) =
  balancearSiSeDesbalanceo (NodeT n ti (sinmaxBST td))


-- dado un BST y un elemento, devuelve el maximo elemento
-- que sea menor al elemento dado
elMaximoMenorA :: Ord a => a -> Tree a -> a
elMaximoMenorA e t = fromJust (maxMenorQue e t Nothing)

maxMenorQue :: Ord a => a -> Tree a -> Maybe a -> Maybe a
maxMenorQue e EmptyT x0 = x0
maxMenorQue e (NodeT n ti td) x0 = 
  if e <= n
    then maxMenorQue e ti x0
    else maxMenorQue e td (Just n)

fromJust :: Maybe a -> a
fromJust (Just e) = e


-- dado un BST y un elemento, devuelve el minimo elemento
-- que sea mayor al elemento dado
elMinimoMayorA :: Ord a => a -> Tree a -> a
elMinimoMayorA e t = fromJust (minMayorQue e t Nothing)

minMayorQue :: Ord a => a -> Tree a -> Maybe a -> Maybe a
minMayorQue e EmptyT x0 = x0
minMayorQue e (NodeT n ti td) x0 =
  if e >= n
    then minMayorQue e td x0
    else minMayorQue e ti (Just n)


-- balancea un arbol AVL, si se desbalanceo
balancearSiSeDesbalanceo :: Tree a -> Tree a
balancearSiSeDesbalanceo t =
  if seDesbalanceo t
    then balancearAVL t
    else t

-- indica si el arbol AVL se desbalanceo
seDesbalanceo :: Tree a -> Bool
seDesbalanceo t = abs (balanceT t) > 1

-- balancea un arbol AVL
balancearAVL :: Tree a -> Tree a
balancearAVL t@(NodeT n ti td) =
  if bt == 2
    then
      if bti >= 0
        then rotarDer t -- izq izq
        else rotarDer (NodeT n (rotarIzq ti) td) -- izq der
    else -- bst == -2
      if btd == -1
        then rotarIzq t -- der der
        else rotarIzq (NodeT n ti (rotarDer td)) -- der izq
  where
    bt = balanceT (NodeT n ti td)
    bti = balanceT ti
    btd = balanceT td  

-- devuelve la altura de un arbol
alturaT :: Tree a -> Int
alturaT EmptyT = 0
alturaT (NodeT n ti td) = 1 + (max (alturaT ti) (alturaT td))

-- calcula el factor de balance de un arbol
balanceT :: Tree a -> Int
balanceT EmptyT = 0
balanceT (NodeT n ti td) = (alturaT ti) - (alturaT td)

-- rota un arbol hacia la derecha (sentido horario)
{-
         |n|              |ni|
        /   \            /    \
      |ni|  td    =>   tii    |n|
      /  \                    / \
    tii  tid                tid   td
-}
rotarDer :: Tree a -> Tree a
rotarDer (NodeT n (NodeT ni tii tid) td) =
  NodeT ni tii (NodeT n tid td)

-- rota un arbol hacia la izquierda (sentido anti-horario)
{-
      |n|                  |nd|
     /   \                /    \
    ti  |nd|      =>    |n|    tdd
        /  \            / \
      tdi  tdd         ti tdi
-}
rotarIzq :: Tree a -> Tree a
rotarIzq (NodeT n ti (NodeT nd tdi tdd)) =
  NodeT nd (NodeT n ti tdi) tdd


-- mias

-- retorna un arbol vacio
emptyBST :: Tree a
emptyBST = EmptyT

-- indica si el arbol es vacio
isEmptyBST :: Tree a -> Bool
isEmptyBST EmptyT = True
isEmptyBST t = False

-- indica si el arbol es un AVL
isAVL :: Ord a => Tree a -> Bool
isAVL EmptyT = True
isAVL t = (isBST t) && (esBalanceado t)

-- indica si un arbol es balanceado
esBalanceado :: Tree a -> Bool
esBalanceado EmptyT = True
esBalanceado t@(NodeT n ti td) =
  (abs (balanceT t) < 2)
  && (abs (balanceT ti) < 2)
  && (abs (balanceT td) < 2)

-- indica si el arbol es un BST
isBST :: Ord a => Tree a -> Bool
isBST EmptyT = True
isBST (NodeT n ti td) =
  (isBST ti)
  && (isBST td)
  && (todosMenoresBST ti n)
  && (todosMayoresBST td n)

todosMenoresBST :: Ord a => Tree a -> a -> Bool
todosMenoresBST EmptyT n = True
todosMenoresBST t n = (maxBST t) < n

todosMayoresBST :: Ord a => Tree a -> a -> Bool
todosMayoresBST EmptyT n = True
todosMayoresBST t n = (minBST t) > n

-- genera un AVL a partir de una lista
avlFromList :: Ord a => [a] -> Tree a
avlFromList = foldr insertBST EmptyT

-- hace shuffle de una lista
shuffle :: [a] -> [a]
shuffle xs = list2shuffle (genHorT (length xs)) xs []

list2shuffle :: [Char] -> [a] -> [a] -> [a]
list2shuffle ds [] ys = ys
list2shuffle (d:ds) (x:xs) ys =
  if d == 'h'
    then list2shuffle ds xs (x : ys)
    else list2shuffle ds xs (ys ++ [x])

genHorT :: Int -> [Char]
genHorT n = map int2ht (nums n)

int2ht :: Int -> Char
int2ht i =
  if i < 0
    then 'h'
    else 't'

-- genera N numeros aleatorios entre 0 y 9 (repeticiones permitidas)
genNums :: Int -> [Int]
genNums n = map (`mod` 10) (nums n)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]


-----------------------------------------------------
-- impresion de arboles
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Show a => Tree a -> IO ()
prettyprint t = putStrLn (unlines (prettyprint_helper t))
-- unlines concats a list with newlines

prettyprint_helper :: Show a => Tree a -> [String]
prettyprint_helper (EmptyT) = ["e"]
prettyprint_helper (NodeT e ti td)
  = (show e) : (prettyprint_subtree ti td)

prettyprint_subtree :: Show a => Tree a -> Tree a -> [String]
prettyprint_subtree ti td
  = ((pad "├──" "│  ") (prettyprint_helper td))
  ++ ((pad "╰──" "   ") (prettyprint_helper ti))

pad :: String -> String -> [String] -> [String]
pad first rest = zipWith (++) (first : repeat rest)
