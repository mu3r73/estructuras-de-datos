{-
Ejercicio 1:
Definir el tipos de datos Palo, con las alternativas corazon, diamante,
pica y trebol; Carta, como un palo y un numero (un renombre de Int);
y Mazo, como una lista de cartas.
-}

data Palo
  = Corazon
  | Diamante
  | Pica
  | Trebol
  deriving (Eq, Show)

data Carta
  = C Palo Numero
  deriving (Show)

type Numero = Int

data Mazo
  = M [Carta]
  deriving (Show)

-- datos para tests

cc1 = C Corazon 1
cc2 = C Corazon 2
cc12 = C Corazon 12
cc13 = C Corazon 13

cd3 = C Diamante 3
cd4 = C Diamante 4
cd11 = C Diamante 11
cd12 = C Diamante 12

cp5 = C Pica 5
cp6 = C Pica 6
cp10 = C Pica 10
cp11 = C Pica 11

ct7 = C Trebol 7
ct8 = C Trebol 8
ct9 = C Trebol 9
ct10 = C Trebol 10

mazo = [cc1, cc2, cd3, cd4, cp5, cp6, ct7, ct8, ct9, ct10, cp10, cp11, cd11, cd12, cc12, cc13]


{-
Ejercicio 2
Definir las funciones de acceso (o inversas de los constructores)
del tipo Carta:
-}

palo :: Carta -> Palo
palo (C p n) = p

numero :: Carta -> Numero
numero (C p n) = n

{-
Ejercicio 3
En la baraja francesa, el corazon es el palo de mayor valor,
seguido de la pica, el diamante y el trebol.
Al mismo tiempo, se considera que una carta es mayor a otra
si su numero es mayor, desempatando por palo de ser necesario.
Definir las siguientes funciones utilizando Pattern Matching:
-}

-- Dados dos palos indica si el primero es mayor que el segundo.
mayorPalo :: Palo -> Palo -> Bool
mayorPalo Corazon p = True
mayorPalo Pica p = p /= Corazon
mayorPalo Diamante p = (p /= Corazon) && (p /= Pica)
mayorPalo Trebol p = False

-- Dadas dos cartas indica si la primera es mayor que la segunda.
mayorCarta :: Carta -> Carta -> Bool
mayorCarta c1 c2 =
  if (numero c1) > (numero c2)
    then True
    else
      if (numero c1) == (numero c2)
        then mayorPalo (palo c1) (palo c2)
        else False

{-
Ejercicio 4
Definir las siguientes funciones:
-}

-- Dado un mazo se queda unicamente con las cartas del palo pedido.
filtrarPalo :: Palo -> Mazo -> Mazo
filtrarPalo p (M cs) = M (filtrarCartas p cs)

filtrarCartas :: Palo -> [Carta] -> [Carta]
filtrarCartas p [] = []
filtrarCartas p (c:cs) =
  if (palo c) == p
    then c : (filtrarCartas p cs)
    else filtrarCartas p cs

-- Dado un mazo devuelve la mayor carta en el.
mayorCartaDelMazo :: Mazo -> Carta
mayorCartaDelMazo (M cs) = mayorCartaDe cs

mayorCartaDe :: [Carta] -> Carta
mayorCartaDe [] = error "lista vacia"
mayorCartaDe (c:[]) = c
mayorCartaDe (c:cs) =
  if mayorCarta c (mayorCartaDe cs)
    then c
    else mayorCartaDe cs

-- Dado un mazo devuelve la lista con los numeros de las cartas en el.
numerosDelMazo :: Mazo -> [Numero]
numerosDelMazo (M cs) = numerosDeCartas cs

numerosDeCartas :: [Carta] -> [Numero]
numerosDeCartas [] = []
numerosDeCartas (c:cs) = (numero c) : (numerosDeCartas cs)
