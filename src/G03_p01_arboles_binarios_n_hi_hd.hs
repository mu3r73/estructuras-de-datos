-- variante cosmetica de G03_p01_arboles_binarios,
-- con el constructor NodeT a (Tree a) (Tree a)
-- en vez de NodeT (Tree a) a (Tree a)

module G03_p01_arboles_binarios_n_hi_hd (
  Tree(EmptyT, NodeT), sumarT, sizeT, mapDobleT, mapOpuestoT, mapLongitudT,
  perteneceT, aparicionesT, promedioEdadesT, engancharYSumaEnRaiz,
  contarLeaves, heightT, contarNoHojas, espejoT, listInOrder, listPreOrder, listPosOrder,
  concatenarListasT, levelN, listPerLevel, widthT,
  mapT, treeFromList, agregarTbal, agregarTal, shuffle, genNums, prettyprint
) where

import G02_p01_tipos_def_por_usr_e01_dir
import G02_p01_tipos_def_por_usr_e02_persona

import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe

-- arbol binario
data Tree a
  = EmptyT
  | NodeT a
          (Tree a)
          (Tree a)
  deriving (Show)

-- accesores
val :: Tree a -> a
val (NodeT n t1 t2) = n

hi :: Tree a -> Tree a
hi (NodeT n t1 t2) = t1

hd :: Tree a -> Tree a
hd (NodeT n t1 t2) = t2

-- datos para tests
{-
nota: para arboles aleatorios, usar treeFromList agregarTal <lista>,
      ej: treeFromList agregarTal (shuffle [1..6]), -- sin repetidos
      ej: treeFromList agregarTal (genNums 10), -- posibles repetidos
      y asignar expresion resultante a una var
-}

tint1 :: Tree Int
tint1 = treeFromList agregarTbal [1 .. 9]

tint2 :: Tree Int
tint2 = treeFromList agregarTbal [10 .. 15]

tdir = treeFromList agregarTbal [Norte, Este, Sur, Oeste, Sur, Este]
tstr = treeFromList agregarTbal ["alfa", "bravo", "charly", "delta", "eco", "foxtrot", "eco"]

p1 = Pers "Titi" 22
p2 = Pers "Pepe" 20
p3 = Pers "Toto" 24
p4 = Pers "Mimi" 26
tpers = treeFromList agregarTbal [p1, p2, p3, p4]

-- dado un arbol binario de enteros, devuelve la suma
-- entre sus elementos
sumarT :: Tree Int -> Int
sumarT EmptyT = 0
sumarT (NodeT n t1 t2) = n + (sumarT t1) + (sumarT t2)

-- dado un arbol binario, devuelve su cantidad de elementos,
-- es decir, el tamanio del arbol (size en ingles)
sizeT :: Tree a -> Int
sizeT EmptyT = 0
sizeT (NodeT e t1 t2) = 1 + (sizeT t1) + (sizeT t2)

-- dado un arbol de enteros, devuelve un arbol con el doble
-- de cada numero
mapDobleT :: Tree Int -> Tree Int
mapDobleT EmptyT = EmptyT
mapDobleT (NodeT n t1 t2) = NodeT (2 * n) (mapDobleT t1) (mapDobleT t2)

mapDobleT' :: Tree Int -> Tree Int
mapDobleT' t = mapT (* 2) t

-- dado un arbol de direcciones t, devuelve un arbol
-- con la direccion opuesta
-- para cada elemento de t
-- nota: Utilizar el tipo Dir ya definido
mapOpuestoT :: Tree Dir -> Tree Dir
mapOpuestoT EmptyT = EmptyT
mapOpuestoT (NodeT d t1 t2) = NodeT (opuesto d) (mapOpuestoT t1) (mapOpuestoT t2)

mapOpuestoT' :: Tree Dir -> Tree Dir
mapOpuestoT' t = mapT opuesto t

-- dado un arbol de palabras, devuelve un arbol con la longitud
-- de cada palabra
mapLongitudT :: Tree String -> Tree Int
mapLongitudT EmptyT = EmptyT
mapLongitudT (NodeT p t1 t2) = NodeT (length p) (mapLongitudT t1) (mapLongitudT t2)

mapLongitudT' :: Tree String -> Tree Int
mapLongitudT' t = mapT length t

-- dados un elemento y un arbol binario, devuelve True si existe
-- un elemento igual a ese en el arbol
perteneceT :: Eq a => a -> Tree a -> Bool
perteneceT e EmptyT = False
perteneceT e (NodeT x t1 t2) = (x == e) || (perteneceT e t1) || (perteneceT e t2)

-- dados un elemento e y un arbol binario, devuelve la cantidad
-- de elementos del arbol que son iguales a e
aparicionesT :: Eq a => a -> Tree a -> Int
aparicionesT e EmptyT = 0
aparicionesT e (NodeT x t1 t2) =
  if (x == e)
    then 1 + (aparicionesT e t1) + (aparicionesT e t2)
    else (aparicionesT e t1) + (aparicionesT e t2)

-- dado un arbol de personas, devuelve el promedio entre las edades
-- de todas las personas
-- definir las subtareas que sean necesarias para resolver esta funcion
-- nota: utilizar el tipo Persona ya definido
promedioEdadesT :: Tree Persona -> Int
promedioEdadesT EmptyT = error "arbol vacio"
promedioEdadesT t = (sumarEdades t) `div` (sizeT t)

sumarEdades :: Tree Persona -> Int
sumarEdades t = sumarT (mapT edad t)

-- dados dos arboles, construye un arbol t en el que ambos arboles
-- son hijos de t, y en la raiz de t se guarda la suma de todos
-- los elementos de los hijos de t
-- se utiliza recursion para definir esta funcion?
engancharYSumaEnRaiz :: Tree Int -> Tree Int -> Tree Int
engancharYSumaEnRaiz t1 t2 = NodeT (sumarT t1 + sumarT t2) t1 t2

-- dado un arbol, devuelve su cantidad de hojas
-- nota: una hoja (leaf en ingles) es un nodo que no tiene hijos
contarLeaves :: Tree a -> Int
contarLeaves EmptyT = 0
contarLeaves (NodeT e EmptyT EmptyT) = 1 -- hoja
contarLeaves (NodeT e t1 t2) = (contarLeaves t1) + (contarLeaves t2)

-- dado un arbol, devuelve los elementos que se encuentran en sus hojas
leaves :: Tree a -> [a]
leaves EmptyT = []
leaves (NodeT e EmptyT EmptyT) = [e] -- hoja
leaves (NodeT e t1 t2) = (leaves t1) ++ (leaves t2)

-- dado un arbol, devuelve su altura
-- nota: la altura (height en ingles) de un arbol es la cantidad maxima
-- de nodos entre la raiz y alguna de sus hojas
-- la altura de un arbol vacio es cero, y la de una hoja es 1
heightT :: Tree a -> Int
heightT EmptyT = 0
heightT (NodeT e EmptyT EmptyT) = 1 -- hoja
heightT (NodeT e t1 t2) = 1 + max (heightT t1) (heightT t2)

-- dado un arbol, devuelve el numero de nodos que no son hojas
-- como podria resolverla sin utilizar recursion?
-- primero definala con recursion, y despues sin ella
contarNoHojas :: Tree a -> Int
contarNoHojas EmptyT = 0
contarNoHojas (NodeT e EmptyT EmptyT) = 0 -- hoja
contarNoHojas (NodeT e t1 t2) = 1 + (contarNoHojas t1) + (contarNoHojas t2)

contarNoHojas' :: Tree a -> Int
contarNoHojas' t = sizeT t - contarLeaves t

-- dado un arbol, devuelve el arbol resultante de intercambiar
-- el hijo izquierdo con el derecho en cada nodo del arbol
espejoT :: Tree a -> Tree a
espejoT EmptyT = EmptyT
espejoT (NodeT e t1 t2) = NodeT e (espejoT t2) (espejoT t1)

-- dado un arbol, devuelve una lista que representa el resultado
-- de recorrerlo en modo in-order
-- nota: en el modo in-order, primero se procesan los elementos
-- del hijo izquierdo, luego la raiz, y luego los elementos
-- del hijo derecho
listInOrder :: Tree a -> [a]
listInOrder EmptyT = []
listInOrder (NodeT e t1 t2) = (listInOrder t1) ++ (e : (listInOrder t2))

-- dado un arbol, devuelve una lista que representa el resultado
-- de recorrerlo en modo pre-order
-- nota: en el modo pre-order, primero se procesa la raiz, luego
-- los elementos del hijo izquierdo, a continuacion los elementos
-- del hijo derecho
listPreOrder :: Tree a -> [a]
listPreOrder EmptyT = []
listPreOrder (NodeT e t1 t2) = e : ((listPreOrder t1) ++ (listPreOrder t2))

-- dado un arbol, devuelve una lista que representa el resultado
-- de recorrerlo en modo post-order
-- nota: en el modo post-order, primero se procesan los elementos
-- del hijo izquierdo, a continuacion los elementos del hijo derecho
-- y finalmente la raiz
listPosOrder :: Tree a -> [a]
listPosOrder EmptyT = []
listPosOrder (NodeT e t1 t2) = (listPosOrder t1) ++ ((listPosOrder t2) ++ [e])

-- dado un arbol de listas, devuelve la concatenacion
-- de todas esas listas
-- el recorrido debe ser in-order
concatenarListasT :: Tree [a] -> [a]
concatenarListasT EmptyT = []
concatenarListasT (NodeT l t1 t2) = (concatenarListasT t1) ++ (l ++ (concatenarListasT t2))

-- dados un numero n y un arbol, devuelve una lista con los nodos
-- de nivel n
-- nota: el primer nivel de un arbol (su raiz) es 0
levelN :: Int -> Tree a -> [a]
levelN n t = nivelN n 0 t

nivelN :: Int -> Int -> Tree a -> [a]
nivelN n m EmptyT = []
nivelN n m (NodeT e t1 t2) =
  if (n == m)
    then [e]
    else (nivelN n (m + 1) t1) ++ (nivelN n (m + 1) t2)

-- dado un arbol, devuelve una lista de listas en la que cada elemento
-- representa un nivel de dicho arbol
listPerLevel :: Tree a -> [[a]]
listPerLevel t = listarHastaNivelN 0 t

listarHastaNivelN :: Int -> Tree a -> [[a]]
listarHastaNivelN n t =
  if (n < heightT t)
    then (levelN n t) : (listarHastaNivelN (n + 1) t)
    else []

-- dado un arbol, devuelve su ancho (width en ingles), que es
-- la cantidad de nodos del nivel con mayor cantidad de nodos
widthT :: Tree a -> Int
widthT t = maximum (map length (listPerLevel t))
-- TODO: usar mapLongitud

-- devuelve todos los elementos encontrados en el camino
-- de todas las ramas derechas
ramaDerecha :: Tree a -> [a]
ramaDerecha EmptyT = []
ramaDerecha t = recorrerSinMostrar t

recorrerSinMostrar EmptyT = []
recorrerSinMostrar (NodeT e t1 t2) = (recorrerSinMostrar t1) ++ (recorrerMostrando t2)

recorrerMostrando EmptyT = []
recorrerMostrando (NodeT e t1 t2) = e : ((recorrerSinMostrar t1) ++ (recorrerMostrando t2))

ramaDerecha' :: Tree a -> [a]
ramaDerecha' EmptyT = []
ramaDerecha' t = recorrerMostrandoRamaDer True t

recorrerMostrandoRamaDer :: Bool -> Tree a -> [a]
recorrerMostrandoRamaDer b EmptyT = []
recorrerMostrandoRamaDer b (NodeT e t1 t2) =
  if b
    then e : ((recorrerMostrandoRamaDer False t1) ++ (recorrerMostrandoRamaDer True t2))
    else ((recorrerMostrandoRamaDer False t1) ++ (recorrerMostrandoRamaDer True t2))

-- version que sólo recorre la rama más derecha
ramaMasDerecha :: Tree a -> [a]
ramaMasDerecha EmptyT = []
ramaMasDerecha (NodeT e t1 t2) = e : (ramaMasDerecha t2)

-- dado un arbol, devuelve todos los caminos,
-- es decir, los caminos desde la raiz hasta las hojas
todosLosCaminos :: Tree a -> [[a]]
todosLosCaminos EmptyT = []
todosLosCaminos (NodeT e EmptyT EmptyT) = [[e]]
todosLosCaminos (NodeT e t1 t2) = agregarATodos e ((todosLosCaminos t1) ++ (todosLosCaminos t2))

agregarATodos :: a -> [[a]] -> [[a]]
agregarATodos e [] = []
agregarATodos e (xs:xss) = (e : xs) : (agregarATodos e xss)


----------
--- mias
----------

-- map generico para arboles binarios
mapT :: (a -> b) -> Tree a -> Tree b
mapT f EmptyT = EmptyT
mapT f (NodeT n t1 t2) = NodeT (f n) (mapT f t1) (mapT f t2)

-- crea un arbol binario a partir de una lista,
-- usando la funcion fn para agregar cada elemento
-- (fn debe ser agregarTbal o agregarTal)
treeFromList :: (a -> Tree a -> Tree a) -> [a] -> Tree a
treeFromList fn xs = list2tree fn xs EmptyT

list2tree :: (a -> Tree a -> Tree a) -> [a] -> Tree a -> Tree a
list2tree fn [] t = t
list2tree fn (x:xs) t = list2tree fn xs (fn x t)

-- agrega un nodo a un arbol, tratando de mantener las ramas
-- aprox del mismo tama#o
agregarTbal :: a -> Tree a -> Tree a
agregarTbal n EmptyT = NodeT n EmptyT EmptyT
agregarTbal n (NodeT e t1 t2) =
  if (sizeT t1) <= (sizeT t2)
    then NodeT e (agregarTbal n t1) t2
    else NodeT e t1 (agregarTbal n t2)

-- agrega un nodo a un arbol, en una posicion aleatoria
agregarTal :: a -> Tree a -> Tree a
agregarTal n t = agregarEnPos (genDirs (sizeT t)) n t

agregarEnPos :: [Char] -> a -> Tree a -> Tree a
agregarEnPos ds e EmptyT = NodeT e EmptyT EmptyT
agregarEnPos [] e (NodeT n ti td) = error "ubicacion ya ocupada"
agregarEnPos (d:ds) e (NodeT n ti td) =
  if (d == 'i')
    then NodeT n (agregarEnPos ds e ti) td
    else NodeT n ti (agregarEnPos ds e td)

genDirs :: Int -> [Char]
genDirs n = map int2dir (nums n)

int2dir :: Int -> Char
int2dir i =
  if (i < 0)
    then 'i'
    else 'd'

-- hace shuffle de una lista
shuffle :: [a] -> [a]
shuffle xs = list2shuffle (genHorT (length xs)) xs []

list2shuffle :: [Char] -> [a] -> [a] -> [a]
list2shuffle ds [] ys = ys
list2shuffle (d:ds) (x:xs) ys =
  if d == 'h'
    then list2shuffle ds xs (x : ys)
    else list2shuffle ds xs (ys ++ [x])

genHorT :: Int -> [Char]
genHorT n = map int2ht (nums n)

int2ht :: Int -> Char
int2ht i =
  if (i < 0)
    then 'h'
    else 't'

-- genera N numeros aleatorios entre 0 y 9 (repeticiones permitidas)
genNums :: Int -> [Int]
genNums n = map (`mod` 10) (nums n)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]


-----------------------------------------------------
-- impresion de arboles
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Show a => Tree a -> IO ()
prettyprint t = putStrLn (unlines (prettyprint_helper t))
-- unlines concats a list with newlines

prettyprint_helper :: Show a => Tree a -> [String]
prettyprint_helper (EmptyT) = ["e"]
prettyprint_helper (NodeT e ti td)
  = (show e) : (prettyprint_subtree ti td)

prettyprint_subtree :: Show a => Tree a -> Tree a -> [String]
prettyprint_subtree ti td
  = ((pad "├──" "│  ") (prettyprint_helper td))
  ++ ((pad "╰──" "   ") (prettyprint_helper ti))

pad :: String -> String -> [String] -> [String]
pad first rest = zipWith (++) (first : repeat rest)
