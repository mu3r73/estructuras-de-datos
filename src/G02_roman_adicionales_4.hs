module Adicionales04 where

{-
Ejercicios adicionales parecidos al primer parcial
Practica 1 + Practica 2
Roman Garcia
-}

{-
Ejercicio 1: Un jardinero tiene semillas que usa para su trabajo.
De cada semilla se conoce el tipo de planta que es, la estación en que se
planta (verano, otoño, invierno, primavera), la estación en que florece y
la cantidad que tienen en gramos.
Nuestro jardinero maneja tres tipos de plantas: enredaderas, flores y cesped.
Una cosecha de semillas es una lista de todas las semillas
cosechadas en un año dado. Definir los tipos enunciados 
-}
data Semilla
  = S Tipo
      Estacion
      Estacion
      Peso
  deriving (Show)

data Tipo
  = Enredadera
  | Flor
  | Cesped
  deriving (Eq, Show)

data Estacion
  = Verano
  | Otonio
  | Invierno
  | Primavera
  deriving (Eq, Show)

data Cosecha
  = C Anio
      [Semilla]
  deriving (Show)

type Peso = Int

type Anio = Int

-- datos para pruebas
s1 = S Enredadera Verano Otonio 8
s2 = S Flor Verano Invierno 7
s3 = S Flor Primavera Primavera 10
s4 = S Enredadera Verano Otonio 7
s5 = S Cesped Invierno Invierno 2
s6 = S Flor Verano Primavera 5

c1 = C 2017 [s1, s2, s3, s4, s5]
c2 = C 2016 [s6, s5, s4, s3]
c3 = C 2015 [s2, s4, s6]
c4 = C 2014 [s5, s3, s1]

cs = [c1, c2, c3, c4]

{-
Ejercicio 2: Definir las funciones de acceso: 
-}

tipo :: Semilla -> Tipo
tipo (S t e1 e2 p) = t

sePlantaEn :: Semilla -> Estacion
sePlantaEn (S t e1 e2 p) = e1

floreceEn :: Semilla -> Estacion
floreceEn (S t e1 e2 p) = e2

peso :: Semilla -> Peso
peso (S t e1 e2 p) = p

anio :: Cosecha -> Anio
anio (C a ss) = a

semillas :: Cosecha -> [Semilla]
semillas (C a ss) = ss

{-
Ejercicio 3: Definir usando Pattern Matching: 
-}

--Dada una estacion, calcula cual es la proxima estacion
proximaEstacion :: Estacion -> Estacion
proximaEstacion Verano = Otonio
proximaEstacion Otonio = Invierno
proximaEstacion Invierno = Primavera
proximaEstacion Primavera = Verano

-- Dadas dos estaciones, determinar si una está primero que otra
estaPrimero :: Estacion -> Estacion -> Bool
estaPrimero Verano e = True
estaPrimero Otonio e = (e /= Verano)
estaPrimero Invierno e = (e /= Verano) && (e /= Otonio)
estaPrimero Primavera e = False

-- Devuelve true si la planta florece a la siguiente estacion a la que se planta
floreceEnLaProximaEstacion :: Semilla -> Bool
floreceEnLaProximaEstacion (S t e1 e2 p) = (proximaEstacion e1) == e2 

-- Dada una cosecha, agregarle dos semillas
agregarDosSemillas :: Semilla -> Semilla -> Cosecha -> Cosecha
agregarDosSemillas s1 s2 (C a ss) = C a (agregarSemilla s1 (agregarSemilla s2 ss))

agregarSemilla :: Semilla -> [Semilla] -> [Semilla]
agregarSemilla s [] = [s]
agregarSemilla s ss = s:ss

{-
Ejercicio 4: Definir las siguientes funciones. Utilizar funciones auxiliares
si resulta conveniente 
-}

-- Dada una Cosecha, devolver las semillas de un tipo dado
semillasDeTipo :: Tipo -> Cosecha -> [Semilla]
semillasDeTipo t (C a ss) = semillasTipo t ss

semillasTipo :: Tipo -> [Semilla] -> [Semilla]
semillasTipo t [] = []
semillasTipo t (s:ss) =
  if (tipo s) == t
    then s : (semillasTipo t ss)
    else semillasTipo t ss

-- Dada una lista de Cosechas, devolver una lista de pares, donde la primera
-- componente es el ano y la segunda la cantidad de semillas cosechadas
cantSemillasPorAnio :: [Cosecha] -> [(Anio, Int)]
cantSemillasPorAnio [] = []
cantSemillasPorAnio (c:cs) = (anio c, length (semillas c)) : (cantSemillasPorAnio cs)

-- Dada una cosecha, devolver cuantos gramos de semillas se tienen
gramosSemillas :: Cosecha -> Int
gramosSemillas (C a ss) = pesoTotal ss

pesoTotal :: [Semilla] -> Int
pesoTotal [] = 0
pesoTotal (s:ss) = (peso s) + (pesoTotal ss)

-------------------------- Extra: ----------------------------------
-- Dada una Cosecha, determinar si las semillas estan ordenadas. Una semilla
-- está ordenada con respecto a otra, si la estacion que se siembra la
-- primera semilla es anterior o la misma a la otra segunda semilla
cosechaOrdenada :: Cosecha -> Bool
cosechaOrdenada (C a ss) = semillasOrdenadas ss

semillasOrdenadas [] = True
semillasOrdenadas (s:[]) = True
semillasOrdenadas (s1:s2:ss) =
  estaOrdenadaRespectoA s1 s2
  && semillasOrdenadas (s2:ss)

estaOrdenadaRespectoA :: Semilla -> Semilla -> Bool
estaOrdenadaRespectoA s1 s2 =
  (sePlantaEn s1) == (sePlantaEn s2)
  || proximaEstacion (sePlantaEn s1) == (sePlantaEn s2)
