module G04_p04_queue_con_long_constante (
  Queue, emptyQ, isEmptyQ, queue, firstQ, dequeue, lenQ, list2queue
) where

data Queue a =
  Q Int [a]
  deriving (Show, Eq)

-- crea una cola vacia
emptyQ :: Queue a
emptyQ = Q 0 []

-- dada una cola, indica si la cola esta vacia
isEmptyQ :: Queue a -> Bool
isEmptyQ (Q n es) = (n == 0)

-- dados un elemento y una cola, agrega ese elemento a la cola
queue :: a -> Queue a -> Queue a
queue e (Q n es) = Q (n + 1) (es ++ [e])

-- dada una cola, devuelve su primer elemento
firstQ :: Queue a -> a
firstQ (Q n es) = primero es

primero :: [a] -> a -- head
primero [] = error "lista vacia"
primero (x:xs) = x

-- dada una cola, la devuelve sin su primer elemento
dequeue :: Queue a -> Queue a
dequeue (Q n es) = Q (n - 1) (sinPrimero es)

sinPrimero :: [a] -> [a] -- tail
sinPrimero [] = error "lista vacia"
sinPrimero (x:xs) = xs

-- devuelve la cantidad de elementos
lenQ :: Queue a -> Int
lenQ (Q n es) = n


-- mias

list2queue :: [a] -> Queue a
list2queue [] = emptyQ
list2queue xs = queue (last xs) (list2queue (init xs))
