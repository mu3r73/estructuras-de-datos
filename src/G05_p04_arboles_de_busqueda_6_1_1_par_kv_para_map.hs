-- Federico's KeyValuePair, with names shortened, and hindented
module G05_p04_arboles_de_busqueda_6_1_1_par_kv_para_map (
  ParKV, parKV, getK, getV
) where

data ParKV k v =
  KV (k, v)
  deriving (Show)

parKV :: Ord k => k -> v -> ParKV k v
parKV k v = KV (k, v)

getK :: Ord k => ParKV k v -> k
getK (KV (k, v)) = k

getV :: Ord k => ParKV k v -> v
getV (KV (k, v)) = v

instance Eq k => Eq (ParKV k v) where
  KV (k1, v1) == KV (k2, v2) = (k1 == k2)

instance Ord k => Ord (ParKV k v) where
  KV (k1, v1) <= KV (k2, v2) = (k1 <= k2)
