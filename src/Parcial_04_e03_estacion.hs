module Parcial_04_e03_estacion (
  Estacion, vacia, entrar, salir, nroPersonas, hayPersonas,
) where

import Parcial_04_persona as Persona
import Parcial_04_sentido as Sentido
import G05_p04_arboles_de_busqueda_6_1_1_map_usando_BST as Map

-- Ejercicio 3
-- Implementar el tipo abstracto Estación, utilizando la siguiente
-- representación e invariante:
data Estacion =
  E (Map Persona Int)
  deriving (Show)

{-
INV. REP.: Existe una clave para cada Persona con un valor no negativo.
-}


-- La interfaz del tipo Estación es la siguiente:

-- crea una estación vacía
-- O(1)
vacia :: Estacion
vacia = E (initPers (initPers (initPers emptyM Hombre) Mujer) Ninho)

initPers :: Map Persona Int -> Persona -> Map Persona Int
initPers m p = assocM m p 0

-- ingresa una persona del tipo pedido a la estación
-- O(1)
entrar :: Persona -> Estacion -> Estacion
entrar p e@(E m) = E (assocM m p ((nroPersonas p e) + 1))

-- egresa una persona del tipo pedido de la estación
-- O(1)
-- PRECOND: hay al menos una persona del tipo indicado en la estación
salir :: Persona -> Estacion -> Estacion
salir p e@(E m) = E (assocM m p ((nroPersonas p e) - 1))

-- retorna el número de personas del tipo indicado que se encuentran en
-- la estación
-- O(1)
nroPersonas :: Persona -> Estacion -> Int
nroPersonas p (E m) = fromJust (lookupM m p)

fromJust :: Maybe a -> a
fromJust (Just x) = x

-- indica si hay personas del tipo pedido en la estación
-- O(1)
hayPersonas :: Persona -> Estacion -> Bool
hayPersonas p e = (nroPersonas p e) > 0


-- ¿Cómo se podría implementar el Map utilizado, para garantizar los
-- órdenes de complejidad pedidos?

{-
Al ser sólo 3 tipos de Persona distintos (Hombre, Mujer, Niño), las
operaciones tendrán complejidad constante -> O(1) para cualquier
implementación de Map de las vistas en la práctica 5.

Elijo la implementación que usa árboles AVL, para reusar el mismo Map
que uso para implementar Subte.
-}
