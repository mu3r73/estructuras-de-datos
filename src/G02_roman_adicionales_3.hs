module Adicionales03 where
{-
Ejercicios adicionales parecidos a un primer parcial
Practica 1 + Practica 2
Roman Garcia
-}

{-
Ejercicio 1: Definir el tipo de datos Marca con las alternativas Ford,
Fiat, Crevrolet, Toyota; el combustible puede ser nafta, gasoil, gas.
Para este ejercicio, de un auto nos interesa saber la marca, el combustible
que usa, su velocidad maxima y peso.
-}

data Marca
  = Ford
  | Fiat
  | Chevrolet
  | Toyota
  deriving (Eq, Show)

data Combustible
  = Nafta
  | Gasoil
  | Gas 
  deriving (Eq, Show)

data Auto
  = A Marca
      Combustible
      Velocidad
      Peso
  deriving (Show)

type Velocidad = Int

type Peso = Int

-- datos para pruebas
a1 = A Chevrolet Nafta 140 600
a2 = A Toyota Gasoil 100 700
a3 = A Fiat Nafta 160 800
a4 = A Chevrolet Gas 140 800
a5 = A Ford Nafta 160 700
a6 = A Fiat Gasoil 120 600
a7 = A Chevrolet Nafta 160 500
a8 = A Toyota Gas 100 500

as = [a1, a2, a3, a4, a5, a6, a7, a8]

{-
Ejercicio 2: Definir las funciones de acceso:
-}

marca :: Auto -> Marca
marca (A m c v p) = m

combustible :: Auto -> Combustible
combustible (A m c v p) = c

velocidadMaxima :: Auto -> Velocidad
velocidadMaxima (A m c v p) = v

peso :: Auto -> Peso
peso (A m c v p) = p

{-
Ejercicio 3: El combustible más economico es el gas, seguido por
el gasoil y la nafta. Se considera que un auto es más eficiente
que otro si usa un combustible más economico o si a igual combustible
su velocidad es mayor. Definir:
-}

esMasEconomico :: Combustible -> Combustible -> Bool
esMasEconomico Gas c = True
esMasEconomico Gasoil c = c /= Gas
esMasEconomico c1 c2 = False

esMasEficiente :: Auto -> Auto -> Bool
esMasEficiente a1 a2 =
  (esMasEconomico (combustible a1) (combustible a2))
  || ((combustible a1) == (combustible a2)
      && (velocidadMaxima a1) > (velocidadMaxima a2))

{-
Ejercicio 4: Definir las siguientes funciones:
-}

-- Dada una lista de autos, se queda solamente con los que tengan
-- un peso entre los dos dados
filtrarPorPeso :: Peso -> Peso -> [Auto] -> [Auto]
filtrarPorPeso p1 p2 [] = []
filtrarPorPeso p1 p2 (a:as) =
  if (pesoEntre p1 p2 a)
    then a : (filtrarPorPeso p1 p2 as)
    else filtrarPorPeso p1 p2 as

pesoEntre :: Peso -> Peso -> Auto -> Bool
pesoEntre p1 p2 a =
  (peso a) >= p1
  && (peso a) <= p2

-- Dada una lista de autos, se queda solamente con los que usen
-- un determinado tipo de combustible
filtrarPorCombustible :: Combustible -> [Auto] -> [Auto]
filtrarPorCombustible c [] = []
filtrarPorCombustible c (a:as) =
  if (combustible a) == c
    then a : (filtrarPorCombustible c as)
    else filtrarPorCombustible c as

-- Devuelve los autos a Gas que pesan entre 800 y 1000 kilos
losElegidos :: [Auto] -> [Auto]
losElegidos as = filtrarPorPeso 800 1000 (filtrarPorCombustible Gas as)

-- Determinar si una lista de autos está ordenada por eficiencia, esto
-- es, los autos más economicos primero
estaListaOrdenada :: [Auto] -> Bool
estaListaOrdenada [] = True
estaListaOrdenada (a:[]) = True
estaListaOrdenada (a1:a2:as) =
  (esMasEconomico (combustible a1) (combustible a2))
  && (estaListaOrdenada (a2:as))

-- Devuelve la cantidad de autos a nafta de la lista
cantAutosANafta :: [Auto] -> Int
cantAutosANafta as = length (filtrarPorCombustible Nafta as)

-- Determinar si el auto más eficiente de una lista de autos
-- puede ir a mas de 130 km/h
elMasEficienteEsRapido :: [Auto] -> Bool
elMasEficienteEsRapido as = (velocidadMaxima (elMasEficiente as)) > 130

elMasEficiente :: [Auto] -> Auto
elMasEficiente [] = error "lista vacia"
elMasEficiente (a:[]) = a
elMasEficiente (a1:(a2:as)) =
  if (esMasEficiente a1 a2)
    then elMasEficiente (a1:as)
    else elMasEficiente (a2:as)
