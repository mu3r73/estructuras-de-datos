module G04_roman_adicionales_9_p02_queue_con_tamanio_maximo (
  Queue, emptyQ, isEmptyQ, queue, firstQ, dequeue, list2queue,
  isFull, maxSize
) where

data Queue a =
  Q Int [a]
  deriving (Show)

-- dada una cola, indica si la cola esta vacia
isEmptyQ :: Queue a -> Bool
isEmptyQ (Q m es) = esVacia es

esVacia :: [a] -> Bool -- isNil
esVacia [] = True
esVacia es = False

-- dados un elemento y una cola, agrega ese elemento a la cola
queue :: a -> Queue a -> Queue a
queue e (Q m es) =
  if isFull (Q m es)
    then error "cola llena"
    else Q m (es ++ [e])

-- dada una cola, devuelve su primer elemento
firstQ :: Queue a -> a
firstQ (Q m es) = primero es

primero :: [a] -> a -- head
primero [] = error "lista vacia"
primero (x:xs) = x

-- dada una cola, la devuelve sin su primer elemento
dequeue :: Queue a -> Queue a
dequeue (Q m es) = Q m (sinPrimero es)

sinPrimero :: [a] -> [a] -- tail
sinPrimero [] = error "lista vacia"
sinPrimero (x:xs) = xs

-- dado un numero, crea una pila vacia con el tamanio maximo dado
emptyQ :: Int -> Queue a
emptyQ m = Q m []

-- devuelve true si la cola llego a su tamanio maximo
isFull :: Queue a -> Bool
isFull (Q m es) = length es == m

-- devuelve el tamanio maximo que puede tener la cola
maxSize :: Queue a -> Int
maxSize (Q m es) = m


-- mias

list2queue :: [a] -> Queue a
list2queue xs = list2queueRec (length xs) xs

list2queueRec :: Int -> [a] -> Queue a
list2queueRec m [] = emptyQ m
list2queueRec m xs = queue (last xs) (list2queueRec m (init xs))
