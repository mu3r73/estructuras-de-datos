module G04_p05_conjunto_con_max_e01_con_recorrido (
  Conjunto, vacioC, agregarC, perteneceC, cantidadC, borrarC, unionC,
  listaC, maximoC, list2set
) where

data Conjunto a =
  Set Int
      [a]
  deriving (Show, Eq)

-- crea un conjunto vacio
vacioC :: Conjunto a
vacioC = Set 0 []

-- dados un elemento y un conjunto, agrega el elemento al conjunto
agregarC :: Eq a => a -> Conjunto a -> Conjunto a
agregarC e (Set n es) =
  if pertenece e es
    then Set n es
    else Set (n + 1) (e : es)

pertenece :: Eq a => a -> [a] -> Bool
pertenece e [] = False
pertenece e (x:xs) = (e == x) || pertenece e xs

-- dados un elemento y un conjunto,
-- indica si el elemento pertenece al conjunto
perteneceC :: Eq a => a -> Conjunto a -> Bool
perteneceC e (Set n es) = pertenece e es

-- devuelve la cantidad de elementos distintos de un conjunto
cantidadC :: Eq a => Conjunto a -> Int
cantidadC (Set n es) = n

-- devuelve el conjunto sin el elemento dado
borrarC :: Eq a => a -> Conjunto a -> Conjunto a
borrarC e (Set n es) =
  if (pertenece e es)
    then Set (n - 1) (borrar e es)
    else Set n es

borrar :: Eq a => a -> [a] -> [a]
borrar e [] = []
borrar e (x:xs) =
  if e == x
    then xs
    else x : (borrar e xs)

-- dados dos conjuntos, devuelve un conjunto
-- con todos los elementos de ambos conjuntos
unionC :: Eq a => Conjunto a -> Conjunto a -> Conjunto a
unionC c (Set n es) = agregarListaAConj es c

agregarListaAConj :: Eq a => [a] -> Conjunto a -> Conjunto a
agregarListaAConj [] c = c
agregarListaAConj (x:xs) c = agregarC x (agregarListaAConj xs c)

-- dado un conjunto, devuelve una lista
-- con todos los elementos distintos del conjunto
listaC :: Eq a => Conjunto a -> [a]
listaC (Set n es) = es

-- devuelve el maximo elemento en un conjunto
maximoC :: Ord a => Conjunto a -> a
maximoC (Set n es) = maximoL es

maximoL :: Ord a => [a] -> a
maximoL [] = error "lista vacia"
maximoL [x] = x
maximoL (x:xs) = maximo x (maximoL xs)

maximo :: Ord a => a -> a -> a
maximo n m =
  if n > m
    then n
    else m


-- mias

list2set :: Eq a => [a] -> Conjunto a
list2set [] = vacioC
list2set (x:xs) = agregarC x (list2set xs)
