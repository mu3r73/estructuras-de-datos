-- expresion aritmetica
data Exp
  = Constante Int
  | ConsExpUnaria OpUnaria
                  Exp
  | ConsExpBinaria OpBinaria
                   Exp
                   Exp
  deriving (Show)

data OpUnaria =
  Neg
  deriving(Show)

data OpBinaria
  = Suma
  | Resta
  | Mult
  | Div
  deriving(Show)

-- dada una expresion, la evalua y retorna su valor
-- que casos hacen que eval sea una funcion parcial?
eval :: Exp -> Int
eval (Constante n) = n
eval (ConsExpUnaria o e) = evalOpUnaria o (eval e)
eval (ConsExpBinaria o e1 e2) = evalOpBinaria o (eval e1) (eval e2)
-- eval es parcial porque eval (ConsExpBinaria Div e 0) no está definida

evalOpUnaria :: OpUnaria -> Int -> Int
evalOpUnaria Neg n = -n

evalOpBinaria :: OpBinaria -> Int -> Int -> Int
evalOpBinaria Suma n1 n2 = n1 + n2
evalOpBinaria Resta n1 n2 = n1 - n2
evalOpBinaria Mult n1 n2 = n1 * n2
evalOpBinaria Div n1 n2 = n1 `div` n2

-- dada una expresion, la simplifica segun los siguientes criterios:
{-
a) 0 + x = x + 0 = x
b) x - 0 = x
c) 0 - x = -x
d) x * 1 = 1 * x = x
e) x * 0 = 0 * x = 0
f) x / 1 = x
g) 0 / x = 0, x /= 0
-}
simplificar :: Exp -> Exp
simplificar (Constante n) = Constante n
simplificar (ConsExpUnaria o e) =
  ConsExpUnaria o (simplificar e)
simplificar (ConsExpBinaria Suma e1 e2) =
  armarSuma (simplificar e1) (simplificar e2)
simplificar (ConsExpBinaria Resta e1 e2) =
  armarResta (simplificar e1) (simplificar e2)
simplificar (ConsExpBinaria Mult e1 e2) =
  armarMult (simplificar e1) (simplificar e2)
simplificar (ConsExpBinaria Div e1 e2) =
  armarDiv (simplificar e1) (simplificar e2)

armarSuma :: Exp -> Exp -> Exp
armarSuma (Constante 0) e = e
armarSuma e (Constante 0) = e
armarSuma e1 e2 = ConsExpBinaria Suma e1 e2

armarResta :: Exp -> Exp -> Exp
armarResta e (Constante 0) = e
armarResta (Constante 0) e = ConsExpUnaria Neg e
armarResta e1 e2 = ConsExpBinaria Resta e1 e2

armarMult :: Exp -> Exp -> Exp
armarMult (Constante 0) e = Constante 0
armarMult e (Constante 0) = Constante 0
armarMult (Constante 1) e = e
armarMult e (Constante 1) = e
armarMult e1 e2 = ConsExpBinaria Mult e1 e2

armarDiv :: Exp -> Exp -> Exp
armarDiv (Constante 0) e = Constante 0
armarDiv e (Constante 0) = error "division por 0"
armarDiv e (Constante 1) = e
armarDiv e1 e2 = ConsExpBinaria Div e1 e2

{-
~ Explicacion de Roman ~

El instinto natural es hacer:

simplificar (Exp Mul (Nro 1) e) = e

Pero lo que sucede es que si la expresion de la izquierda
simplifica a Nro 1, pero lo hace en varios pasos,
no vamos a capturar ese caso, porque en ese instante no es Nro 1,
sino algo que reduce a Nro 1.

Entonces la solucion es simplificar primero
y hacer el pattern matching despues.
-}

---------------------------------------------------
-- mostrar expresiones algebraicas (c) Roman 2017
---------------------------------------------------

-- hace reducción de parentesis solamente en las constantes
mostrar :: Exp -> String
mostrar (Constante n) = show n
mostrar (ConsExpUnaria Neg (Constante n)) = "-" ++ show n
mostrar (ConsExpUnaria Neg e) = "-(" ++ mostrar e ++ ")"
mostrar (ConsExpBinaria op e1 e2) =
  "(" ++ mostrar e1 ++ mostrarOpBin op ++ mostrar e2 ++ ")"

mostrarOpBin :: OpBinaria -> String
mostrarOpBin Suma = "+"
mostrarOpBin Resta = "-"
mostrarOpBin Mult = "*"
mostrarOpBin Div = "/"
