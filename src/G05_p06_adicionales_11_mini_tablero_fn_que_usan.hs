import G05_p06_adicionales_11_mini_tablero
import G05_p02_map_2_map_2_lista_de_pares_k_v_sin_claves_repetidas

-- dada una direccion d, mueve el cabezal hacia el borde d
irAlExtremo :: Dir -> MiniTablero -> MiniTablero
irAlExtremo dir t =
  if puedeMover dir t
    then irAlExtremo dir (mover dir t)
    else t

-- dados un numero n y una fila, pone n bolitas de cada color
-- en todas las celdas de la fila
llenarHasta :: Int -> MiniTablero -> MiniTablero
llenarHasta n t = llenarHastaRec n (irAlExtremo Oeste t)

llenarHastaRec :: Int -> MiniTablero -> MiniTablero
llenarHastaRec n t =
  if (puedeMover Este t)
    then llenarHastaRec n (mover Este (llenarCeldaActual n t))
    else llenarCeldaActual n t

llenarCeldaActual :: Int -> MiniTablero -> MiniTablero
llenarCeldaActual 0 t = t
llenarCeldaActual n t = llenarCeldaActual (n - 1) (ponerUnoDeCada t)

ponerUnoDeCada :: MiniTablero -> MiniTablero
ponerUnoDeCada t = ponerT Verde (ponerT Rojo (ponerT Negro (ponerT Azul t)))

-- dado un tablero, indica cuantas celdas posee
contarCeldas :: MiniTablero -> Int
contarCeldas t = contarCeldasRec (irAlExtremo Oeste t) 1

contarCeldasRec t n =
  if (puedeMover Este t)
    then contarCeldasRec (mover Este t) (n + 1)
    else n

-- dado un tablero, indica si todas sus celdas se encuentran vacias
noHayBolitas :: MiniTablero -> Bool
noHayBolitas t = noHayBolitasRec (irAlExtremo Oeste t)

noHayBolitasRec :: MiniTablero -> Bool
noHayBolitasRec t =
  if (puedeMover Este t)
    then (not (hayBolitasEnCelda t)) && (noHayBolitasRec (mover Este t))
    else not (hayBolitasEnCelda t)

hayBolitasEnCelda :: MiniTablero -> Bool
hayBolitasEnCelda t
  = (hayBolitasT Azul t)
  || (hayBolitasT Negro t)
  || (hayBolitasT Rojo t)
  || (hayBolitasT Verde t)

-- dado un tablero, arma un Map en donde indica para cada color
-- cuantas bolitas hay en total en el tablero
cantidadesDeBolitas :: MiniTablero -> Map Color Int
cantidadesDeBolitas t =
  cantidadesDeBolitasRec (irAlExtremo Oeste t) initContadores

initContadores :: Map Color Int
initContadores =
  (initCol Verde (initCol Rojo (initCol Negro (initCol Azul emptyM))))

initCol :: Color -> Map Color Int -> Map Color Int
initCol col m = assocM m col 0

cantidadesDeBolitasRec :: MiniTablero -> Map Color Int -> Map Color Int
cantidadesDeBolitasRec t m =
  if puedeMover Este t
    then cantidadesDeBolitasRec (mover Este t) (agregarCantBolitasEnCelda t m)
    else agregarCantBolitasEnCelda t m

agregarCantBolitasEnCelda :: MiniTablero -> Map Color Int -> Map Color Int
agregarCantBolitasEnCelda t m =
  agregarCant Verde t (agregarCant Rojo t (agregarCant Negro t (agregarCant Azul t m)))

agregarCant :: Color -> MiniTablero -> Map Color Int -> Map Color Int
agregarCant col t m = assocM m col ((fromJust (lookupM m col)) + (nroBolitasT col t))

fromJust :: Maybe a -> a
fromJust (Just x) = x
