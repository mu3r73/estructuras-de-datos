module G05_p02_map_2_map_1_lista_de_pares_k_v_con_claves_repetidas (
  Map, emptyM, assocM, lookupM, deleteM, domM
) where

data Map k v = M [(k, v)]
  deriving (Show, Eq)

-- retorna un mapa vacio
-- orden 1
emptyM :: Map k v
emptyM = M []

-- asocia una nueva clave k a un nuevo valor v en el mapa
-- orden 1
assocM :: Eq k => Map k v -> k -> v -> Map k v
assocM (M ps) k v = M ((k, v) : ps)

-- retorna el primer valor v asociado a la clave k
-- orden N
lookupM :: Eq k => Map k v -> k -> Maybe v
lookupM (M ps) k = lookupMrec ps k

lookupMrec :: Eq k => [(k, v)] -> k -> Maybe v
lookupMrec [] k0 = Nothing
lookupMrec ((k, v) : ps) k0 =
  if (k == k0)
    then Just v
    else lookupMrec ps k0

-- elimina el primer valor asociado a la clave k
-- orden N
deleteM :: Eq k => Map k v -> k -> Map k v
deleteM (M ps) k = M (deleteMrec ps k)

deleteMrec :: Eq k => [(k, v)] -> k -> [(k, v)]
deleteMrec [] k0 = []
deleteMrec ((k, v) : ps) k0 =
  if k == k0
    then ps
    else (k, v) : (deleteMrec ps k0)

-- retorna el conjunto de claves
-- orden N^2
domM :: Eq k => Map k v -> [k]
domM (M ps) = domMrec ps

domMrec :: Eq k => [(k, v)] -> [k]
domMrec [] = []
domMrec ((k, v) : ps) = k : (domMrec ps)
