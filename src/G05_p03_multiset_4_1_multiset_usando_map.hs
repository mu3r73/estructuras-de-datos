module G05_p03_multiset_4_1_multiset_usando_map (
  MultiSet, emptyMS, addMS, occursMS, unionMS, inter
) where

import G05_p02_map_2_map_1_lista_de_pares_k_v_sin_claves_repetidas

data MultiSet a = MS (Map a Int)

-- invariante: para todo x en domM map, lookUp x map > 0

-- crea un multiconjunto vacio
emptyMS :: MultiSet a
emptyMS = emptyM

-- dados un elemento y un multiconjunto,
-- agrega una ocurrencia de ese elemento al multiconjunto
addMS :: Ord a => a -> MultiSet a -> MultiSet a
addMS x (MS m) = M (add x m)

add :: a -> Map a Int -> Map a Int
add x m = assocM m x (fromMaybe (lookupM x m) + 1)

fromMaybe :: Maybe Int -> Int
fromMaybe Nothing = 0
fromMaybe (Just n) = n

-- dados un elemento y un multiconjunto, indica la cantidad
-- de apariciones de ese elemento en el multiconjunto
occursMS :: a -> MultiSet a -> Int
occursMS x (MS m) = occursM x m

occursM :: a -> Map a Int -> Int
occursM x m = fromMaybe (lookupM x m)

-- dados dos multiconjuntos, devuelve un multiconjunto
-- con todos los elementos de ambos multiconjuntos
unionMS :: Ord a => MultiSet a -> MultiSet a -> MultiSet a
unionMS (MS m1) (MS m2) = MS (union m1 m2)

union :: Map a Int -> Map a Int -> Map a Int
union m1 m2 = unionSobreDom (domM m1) m1 m2

unionSobreDom :: [a] -> Map a Int -> Map a Int -> Map a Int
unionSobreDom [] m1 m2 = m2
unionSobreDom (x:xs) m1 m2 =
  assocM (unionSobreDom xs m1 m2) x (occursM x m1 + occursM x m2))

-- dados dos multiconjuntos, devuelve el multiconjunto de elementos
-- que ambos multiconjuntos tienen en comun
intersMS :: Ord a => MultiSet a -> MultiSet a -> MultiSet a
intersMS (MS m1) (MS m2) = MS (inters m1 m2)

inters :: Map a Int -> Map a Int -> Map a Int
inters m1 m2 = intersSobreDom (domM m1) m1 m2

intersSobreDom :: [a] -> Map a Int -> Map a Int -> Map a Int
intersSobreDom [] m1 m2 = []
intersSobreDom (x:xs) m1 m2 =
  if (min (occursM x m1) (occursM x m2)) > 0
    then assocM (unionSobreDom xs m1 m2) x min (occursM x m1) (occursM x m2)
    else unionSobreDom xs m1 m2

-- dado un multiconjunto, devuelve una lista con todos los elementos
-- del conjunto y su cantidad de ocurrencias
multiSetToList :: MultiSet a -> [(Int, a)]
multiSetToList (MS m) = recupParesSobreDom (domM m) m

recupParesSobreDom :: [a] -> MutiSet a -> [(Int, a)]
recupParesSobreDom [] m = []
recupParesSobreDom (x:xs) m =
  (occursM x m, x) : (recupParesSobreDom xs m)
