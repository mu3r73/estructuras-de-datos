module G05_p04_arboles_de_busqueda_6_1_1_map_usando_BST (
  Map, emptyM, assocM, lookupM, deleteM, domM,
  G05_p04_arboles_de_busqueda_6_1_1_map_usando_BST.prettyprint
) where

import G05_p04_arboles_de_busqueda_5_3_AVL_n_hi_hd_guardando_h as BST
import G05_p04_arboles_de_busqueda_6_1_1_par_kv_para_map

import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe


data Map k v = M (Tree (ParKV k (Maybe v)))
  deriving (Show)

-- datos para tests
{-
nota: para mapas aleatorios, usar mapFromList (shuffle <lista>),
      ej: mapFromList (shuffle [('a', 1), ('b', 2), ('c', 3), ('d', 4), ('e', 5), ('f', 6)]),
      y asignar expresion resultante a una var
-}

map1 :: Map Char Int
map1 =
  mapFromList
     [ ('a', 3)
     , ('c', 11)
     , ('e', 7)
     , ('g', 5)
     , ('i', 9)
     , ('k', 13)
     , ('m', 1)
     ]

map2 :: Map Char Int
map2 =
  mapFromList
    (shuffle
       [ ('a', 1)
       , ('c', 9)
       , ('e', 3)
       , ('g', 11)
       , ('i', 5)
       , ('k', 13)
       , ('m', 7)
       ])


-- retorna un mapa vacio
-- orden 
emptyM :: Map k v
emptyM = M emptyBST

-- asocia una nueva clave k0 a un nuevo valor v0 en el mapa
-- orden 
assocM :: Ord k => Map k v -> k -> v -> Map k v
assocM (M t) k v = M (insertBST (parKV k (Just v)) t)

-- retorna el valor v asociado a la clave k0
-- orden 
lookupM :: Ord k => Map k v -> k -> Maybe v
lookupM (M t) k = v_de_kv (recuperarBST (parKV k Nothing) t)
  where
    v_de_kv Nothing = Nothing
    v_de_kv (Just kv) = getV kv

-- elimina el valor asociados a la clave k0
-- orden 
deleteM :: Ord k => Map k v -> k -> Map k v
deleteM (M t) k = M (deleteBST (parKV k Nothing) t)

-- retorna el conjunto de claves
-- orden 1
domM :: Ord k => Map k v -> [k]
domM (M t) = getKs (inOrderBST t)

getKs :: Ord k => [ParKV k (Maybe v)] -> [k]
getKs [] = []
getKs (kv:kvs) = (getK kv) : (getKs kvs)


-- mias

-- genera un map a partir de una lista de (k, v)
mapFromList :: Ord k => [(k, v)] -> Map k v
mapFromList [] = emptyM
mapFromList ((k, v) : ps) = assocM (mapFromList ps) k v

-- hace shuffle de una lista
shuffle :: [(k, v)] -> [(k, v)]
shuffle xs = list2shuffle (genHorT (length xs)) xs []

list2shuffle :: [Char] -> [(k, v)] -> [(k, v)] -> [(k, v)]
list2shuffle ds [] ys = ys
list2shuffle (d:ds) (x:xs) ys =
  if d == 'h'
    then list2shuffle ds xs (x : ys)
    else list2shuffle ds xs (ys ++ [x])

genHorT :: Int -> [Char]
genHorT n = map int2ht (nums n)

int2ht :: Int -> Char
int2ht i =
  if i < 0
    then 'h'
    else 't'

-- genera N numeros aleatorios entre 0 y 9 (repeticiones permitidas)
genNums :: Int -> [Int]
genNums n = map (`mod` 10) (nums n)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]


-----------------------------------------------------
-- impresion de arboles
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Show k => Show v => Map k v -> IO ()
prettyprint (M t) = BST.prettyprint t
