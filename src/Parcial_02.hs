import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe

{-
Ejercicio 1:
Se tienen los siguientes tipos de datos para representar una red fluvial:
-}

data Rio
  = Delta Caudal
          [Barco]
  | Bifurcacion Caudal
                [Barco]
                Rio
                Rio
  deriving (Show)

data Barco
  = Pesquero
  | Carguero
  | Petrolero
  deriving (Eq, Show)

type Caudal = Int

-- datos para tests
{-
nota: para rios aleatorios, usar genRio (genNBarcos <cant_nodos>),
      ej: genRio (genNBarcos 9),
      y asignar expresion resultante a una var
-}
rio1 = Bifurcacion 15 []
        (Bifurcacion 65 [Pesquero]
          (Bifurcacion 35 [Petrolero, Pesquero]
            (Delta 90 [Pesquero, Petrolero])
            (Delta 30 [Petrolero, Carguero])
          )
          (Delta 30 [Pesquero])
        )
        (Bifurcacion 40 [Pesquero, Pesquero]
          (Delta 5 [Carguero])
          (Delta 45 [Petrolero])
        )

rio2 = Bifurcacion 45 [Petrolero]
        (Bifurcacion 10 [Petrolero, Carguero]
          (Delta 35 [Pesquero])
          (Delta 85 [])
        )
        (Bifurcacion 25 []
          (Bifurcacion 25 [Carguero, Pesquero]
            (Delta 50 [Petrolero])
            (Delta 85 [Carguero, Carguero, Pesquero])
          )
          (Delta 55 [Pesquero])
        )


{-
1.1. Definir las funciones de acceso correspondientes.
-}

-- de Rio
caudal :: Rio -> Caudal
caudal (Delta c bs) = c
caudal (Bifurcacion c bs ri rd) = c

barcos :: Rio -> [Barco]
barcos (Delta c bs) = bs
barcos (Bifurcacion c bs ri rd) = bs

rioIzq :: Rio -> Rio
rioIzq (Bifurcacion c bs ri rd) = ri
-- es parcial porque no esta definida si el rio es delta

rioDer :: Rio -> Rio
rioDer (Bifurcacion c bs ri rd) = rd
-- es parcial porque no esta definida si el rio es delta

esDelta :: Rio -> Bool
esDelta (Delta c bs) = True
esDelta r = False

esBifurcacion :: Rio -> Bool
esBifurcacion (Bifurcacion c bs ri rd) = True
esBifurcacion r = False

-- de Barco
esPesquero :: Barco -> Bool
esPesquero Pesquero = True
esPesquero b = False

esCarguero :: Barco -> Bool
esCarguero Carguero = True
esCarguero b = False

esPetrolero :: Barco -> Bool
esPetrolero Petrolero = True
esPetrolero b = False


{-
1.2. Definir las siguientes funciones:
-}

-- dado un rio, retorna el caudal total de toda la red fluvial
-- que de el se desprende
caudalTotal :: Rio -> Caudal
caudalTotal (Delta c bs) = c
caudalTotal (Bifurcacion c bs ri rd)
  = c + caudalTotal ri + caudalTotal rd

-- dada una clase de barco y un rio, retorna la lista
-- con todos los barcos de ese tipo en la red fluvial
todosLosBarcos :: Barco -> Rio -> [Barco]
todosLosBarcos b (Delta c bs) = barcosTipo b bs
todosLosBarcos b (Bifurcacion c bs ri rd)
  = (barcosTipo b bs) ++ (todosLosBarcos b ri) ++ (todosLosBarcos b rd)

barcosTipo :: Barco -> [Barco] -> [Barco]
barcosTipo b1 [] = []
barcosTipo b1 (b:bs) =
  if (b == b1)
    then b : (barcosTipo b1 bs)
    else barcosTipo b1 bs

-- dada una clase de baco y un rio, quita todos
-- los barcos de esa clase de la red fluvial
hundirBarcos :: Barco -> Rio -> Rio
hundirBarcos b (Delta c bs) = Delta c (barcosSinTipo b bs)
hundirBarcos b (Bifurcacion c bs ri rd)
  = Bifurcacion c (barcosSinTipo b bs) (hundirBarcos b ri) (hundirBarcos b rd)

barcosSinTipo :: Barco -> [Barco] -> [Barco]
barcosSinTipo b [] = []
barcosSinTipo b1 (b:bs) =
  if (b == b1)
    then barcosSinTipo b1 bs
    else b : (barcosSinTipo b1 bs)


{-
Ejercicio 2 (Recuperatorio):
Definir la función:
-}

-- dada una lista de rios, retorna el rio con
-- mayor cantidad de barcos en su red fluvial
masTransitado :: [Rio] -> Rio
masTransitado (r:[]) = r
masTransitado (r:rs) = elMasTransitado r (masTransitado rs)
-- parcial porque no está definida para []

elMasTransitado :: Rio -> Rio -> Rio
elMasTransitado r1 r2 =
  if (barcosTotales r1) > (barcosTotales r2)
    then r1
    else r2

barcosTotales :: Rio -> Int
barcosTotales (Delta c bs) = length bs
barcosTotales (Bifurcacion c bs ri rd) =
  length bs + (barcosTotales ri) + (barcosTotales rd)


---------
-- mias
---------

-- genera un rio a partir de una lista de listas de barcos
genRio :: [[Barco]] -> Rio
genRio bss =
  if (length bss < 1) || (((length bss) `mod` 2) == 0)
    then error "cantidad nodos debe ser impar y mayor que 0"
    else list2rio (tail bss) (Delta (head (genNums 2 99)) (head bss))

list2rio :: [[Barco]] -> Rio -> Rio
list2rio [] r = r
list2rio (bs1:(bs2:bss)) r = list2rio bss (agregarRal bs1 bs2 r)

agregarRal :: [Barco] -> [Barco] -> Rio -> Rio
agregarRal bs1 bs2 r = agregarEnPos (genDirs (sizeR r)) bs1 bs2 r

agregarEnPos :: [Char] -> [Barco] -> [Barco] -> Rio -> Rio
agregarEnPos ds bs1 bs2 (Delta c bs)
  = Bifurcacion c bs (Delta (head (genNums 2 99)) bs1) (Delta (head (genNums 2 99)) bs2)
agregarEnPos [] bs1 bs2 (Bifurcacion c bs ri rd) = error "ubicacion ya ocupada"
agregarEnPos (d:ds) bs1 bs2 (Bifurcacion c bs ri rd) =
  if (d == 'i')
    then Bifurcacion c bs (agregarEnPos ds bs1 bs2 ri) rd
    else Bifurcacion c bs ri (agregarEnPos ds bs1 bs2 rd)

genDirs :: Int -> [Char]
genDirs n = map int2dir (nums n)

int2dir :: Int -> Char
int2dir i =
  if (i < 0)
    then 'i'
    else 'd'

-- cant de nodos del rio
sizeR :: Rio -> Int
sizeR (Delta c bs) = 1
sizeR (Bifurcacion c bs ri rd) = 1 + sizeR ri + sizeR rd

-- genera una lista de N [Barco] (de long entre 0 y 3)
genNBarcos :: Int -> [[Barco]]
genNBarcos 0 = []
genNBarcos n = (genBarcos (head (genNums 2 3))) : genNBarcos (n - 1)

-- genera N barcos
genBarcos :: Int -> [Barco]
genBarcos n = map int2barco (genNums n 2)

int2barco :: Int -> Barco
int2barco 0 = Pesquero
int2barco 1 = Carguero
int2barco 2 = Petrolero

-- genera N1 numeros aleatorios entre 0 y N2 (repeticiones permitidas)
genNums :: Int -> Int -> [Int]
genNums n1 n2 = map (`mod` (n2 + 1)) (nums n1)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]


-----------------------------------------------------
-- impresion de rios
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Rio -> IO ()
prettyprint t = putStrLn (unlines (prettyprint_helper t))
-- unlines concats a list with newlines

prettyprint_helper :: Rio -> [String]
prettyprint_helper (Delta c o) = [(show c) ++ " " ++ (show o)]
prettyprint_helper (Bifurcacion c o ti td)
  = ((show c) ++ " " ++ (show o)) : (prettyprint_subtree ti td)

prettyprint_subtree :: Rio -> Rio -> [String]
prettyprint_subtree ti td
  = ((pad "├──" "│  ") (prettyprint_helper td))
  ++ ((pad "╰──" "   ") (prettyprint_helper ti))

pad :: String -> String -> [String] -> [String]
pad first rest = zipWith (++) (first : repeat rest)
