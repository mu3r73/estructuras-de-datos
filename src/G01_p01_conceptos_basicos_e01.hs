module G01_p01_conceptos_basicos_e01 (
  sucesor, sumar, maximo
) where

-- dado un numero, devuelve su sucesor
sucesor :: Int -> Int
sucesor n = n + 1

-- dados dos numeros, devuelve su suma, utilizando la operacion +
sumar :: Int -> Int -> Int
sumar n m = n + m

-- dados dos numeros, devuelve el mayor de estos
maximo :: Int -> Int -> Int
maximo n m =
  if n > m
    then n
    else m
