data AppendList a
  = Nil
  | Unit a
  | AppendList (AppendList a)
               (AppendList a)
  deriving (Show)

-- datos para tests

l1 :: AppendList Int
l1 = AppendList
      (AppendList
        (Unit 1)
        (AppendList
          (Unit 2)
          (Unit 3)
        )
      )
      (AppendList
        (AppendList
          (Unit 4)
          (Unit 5)
        )
        (Unit 6)
      )

l2 :: AppendList Int
l2 = AppendList
      (AppendList
        Nil
        (AppendList
          Nil
          Nil
        )
      )
      (AppendList
        (AppendList
          Nil
          Nil
        )
        Nil
      )

l3 :: AppendList Int
l3 = AppendList
      (AppendList
        Nil
        (AppendList
          Nil
          Nil
        )
      )
      (AppendList
        (AppendList
          Nil
          (Unit 5)
        )
        Nil
      )

l4 :: AppendList Int
l4 = AppendList
      (AppendList
        Nil
        (AppendList
          (Unit 2)
          Nil
        )
      )
      (AppendList
        (AppendList
          Nil
          Nil
        )
        Nil
      )

-- indica si la lista esta vacia
esNilL :: AppendList a -> Bool
esNilL Nil = True
esNilL (Unit a) = False
esNilL (AppendList al1 al2) = (esNilL al1) && (esNilL al2)

-- devuelve el primer elemento de la lista
-- nota: el primer elemento es el nodo que se encuentra mas a la izquierda
headL :: AppendList a -> a
headL Nil = error "lista vacia"
headL (Unit e) = e
headL (AppendList al1 al2) =
  if esNilL al1
    then headL al2
    else headL al1

-- elimina el primer elemento de la lista
tailL :: AppendList a -> AppendList a
tailL Nil = error "lista vacia"
tailL (Unit e) = Nil
tailL (AppendList al1 al2) =
  if esNilL al1
    then AppendList al1 (tailL al2)
    else AppendList (tailL al1) al2

-- devuelve la cantidad de elementos de la lista
-- nota: observar que los elementos solo se encuentran en las hojas
lengthL :: AppendList a -> Int
lengthL Nil = 0
lengthL (Unit e) = 1
lengthL (AppendList al1 al2) = (lengthL al1) + (lengthL al2)

-- devuelve el ultimo elemento de la lista
-- nota: el ultimo elemento es el nodo que se encuentra mas a la derecha
lastL :: AppendList a -> a
lastL Nil = error "lista vacia"
lastL (Unit e) = e
lastL (AppendList al1 al2) =
  if esNilL al2
    then lastL al1
    else lastL al2

-- indica si un elemento pertenece a la lista
perteneceL :: Eq a => a -> AppendList a -> Bool
perteneceL e Nil = False
perteneceL e (Unit x) = (e == x)
perteneceL e (AppendList al1 al2) = (perteneceL e al1) || (perteneceL e al2)

-- simplifica el arbol, eliminando las ramas que son Nil en cada Append
simplificarL :: AppendList a -> AppendList a
simplificarL (AppendList al1 al2) = armarLista (simplificarL al1) (simplificarL al2)
simplificarL al = al

armarLista :: AppendList a -> AppendList a -> AppendList a
armarLista Nil Nil = Nil
armarLista Nil al = al
armarLista al Nil = al
armarLista al1 al2 = AppendList al1 al2
