module G01_p01_conceptos_basicos_e02 (
  negar, andLogico, orLogico, primera, segunda, sumaPar, maxDelPar
) where

import G01_p01_conceptos_basicos_e01

-- dado un booleano, si es True devuelve False, y si es False devuelve True
-- definida en Haskell como not
negar :: Bool -> Bool
negar True = False
negar b = True

{-
negarPre :: Bool -> Bool
negarPre True = False
negarPre False = True
-}

negar' :: Bool -> Bool
negar' b =
  if b
    then False
    else True

-- dados dos booleanos, si ambos son True devuelve True, si no devuelve False
-- definida en Haskell como &&
andLogico :: Bool -> Bool -> Bool
andLogico True True = True
andLogico b1 b2 = False

-- dados dos booleanos, si alguno de ellos es True devuelve True,
-- si no devuelve False
-- definida en Haskell como ||
orLogico :: Bool -> Bool -> Bool
orLogico False False = False
orLogico b1 b2 = True

-- dado un par de numeros, devuelve la primera componente
-- definida en Haskell como fst
primera :: (Int, Int) -> Int
primera (x, y) = x

-- dado un par de numeros, devuelve la segunda componente
-- definida en Haskell como snd
segunda :: (Int, Int) -> Int
segunda (x, y) = y

-- dado un par de numeros, devuelve su suma
sumaPar :: (Int, Int) -> Int
sumaPar (x, y) = x + y

{-
sumaParPre1 :: (Int, Int) -> Int
sumaParPre1 (x, y) = sumar x y

sumaParPre2 :: (Int, Int) -> Int
sumaParPre2 p = primera p + segunda p

sumaParPre3 :: (Int, Int) -> Int
sumaParPre3 p = sumar (primera p) (segunda p)
-}

-- dado un par de numeros, devuelve el mayor de estos
maxDelPar :: (Int, Int) -> Int
maxDelPar (x, y) = maximo x y

{-
maxDelParPre1 :: (Int, Int) -> Int
maxDelParPre1 (x, y) =
  if x > y
    then x
    else y
-}
