module Adicionales06 where
{-
Ejercicios adicionales
Como el segundo parcial
Roman Garcia
-}

import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe
import Text.Printf

{-
Red electrica del CIU  
-}

data Red
  = Zapatilla Amperaje
              [Dispositivo]
  | Conexion Amperaje
             [Dispositivo]
             Red
             Red
  deriving (Show)

data Dispositivo =
  Dis TipoDispositivo
      Consumo
  deriving (Show)

data TipoDispositivo
  = PC -- avg 300W
  | Notebook -- avg 50W
  | AireAcond -- avg 750W -- abreviado del original: AireAcondicionado
  deriving (Show, Eq)

type Amperaje = Double

type Consumo = Double

-- datos para tests
{-
nota: para redes aleatorias, usar genRed (genNDispositivos <cant_nodos>),
      ej: genRed (genNDispositivos 9),
      y asignar expresion resultante a una var
-}

-- redes que aguantan
r1 = Conexion 6.51 [Dis AireAcond 596.89,Dis Notebook 43.56]
      (Conexion 3.76 [Dis Notebook 63.66]
        (Zapatilla 1.87 [Dis PC 284.91])
        (Zapatilla 1.10 [Dis Notebook 43.25]))
      (Conexion 3.23 [Dis Notebook 42.95,Dis Notebook 42.36]
        (Zapatilla 1.42 [Dis Notebook 46.09,Dis Notebook 53.35,Dis Notebook 37.99])
        (Zapatilla 1.59 [Dis Notebook 45.91,Dis Notebook 59.36,Dis Notebook 60.12]))

r2 = Conexion 9.67 [Dis PC 224.85,Dis Notebook 43.24]
      (Conexion 5.43 [Dis PC 240.70,Dis Notebook 56.10]
        (Zapatilla 2.17 [Dis Notebook 38.72,Dis Notebook 48.61])
        (Zapatilla 2.23 [Dis Notebook 63.92,Dis Notebook 47.99]))
      (Conexion 6.54 [Dis AireAcond 705.49]
        (Zapatilla 1.85 [Dis Notebook 57.66])
        (Conexion 4.27 [Dis Notebook 37.90]
          (Zapatilla 2.21 [Dis Notebook 64.67,Dis Notebook 42.91,Dis Notebook 35.74])
          (Zapatilla 1.77 [Dis Notebook 39.08])))

-- red que no aguanta
r3 = Conexion 8.56 []
      (Conexion 3.07 [Dis Notebook 37.84]
        (Conexion 1.64 [Dis Notebook 58.6]
          (Zapatilla 0.89 [Dis Notebook 55.11,Dis Notebook 60.35])
          (Zapatilla 0.80 [Dis Notebook 48.12,Dis Notebook 58.50]))
        (Zapatilla 1.44 [Dis PC 296.03]))
      (Conexion 5.43 []
        (Zapatilla 0.83 [Dis Notebook 49.62,Dis Notebook 44.33])
        (Conexion 4.28 [Dis AireAcond 984.13]
          (Zapatilla 0.85 [Dis Notebook 49.73])
          (Zapatilla 0.84 [Dis Notebook 47.22])))


{-
1. Escribir TODAS los funciones de acceso correspondientes (proyectores) 
-}

-- de Red
amperaje :: Red -> Amperaje
amperaje (Zapatilla a ds) = a
amperaje (Conexion a ds ri rd) = a

dispositivos :: Red -> [Dispositivo]
dispositivos (Zapatilla a ds) = ds
dispositivos (Conexion a ds ri rd) = ds

redIzq :: Red -> Red
redIzq (Zapatilla a ds) = error "zapatilla no tiene subredes"
redIzq (Conexion a ds ri rd) = ri

redDer :: Red -> Red
redDer (Zapatilla a ds) = error "zapatilla no tiene subredes"
redDer (Conexion a ds ri rd) = rd

esZapatilla :: Red -> Bool
esZapatilla (Zapatilla a ds) = True
esZapatilla r = False

esConexion :: Red -> Bool
esConexion (Amperaje ds ri rd) = True
esConexion r = False

-- de Dispositivo
tipo :: Dispositivo -> TipoDispositivo
tipo (Dis t c) = t

consumo :: Dispositivo -> Consumo
consumo (Dis t c) = c

-- de TipoDispositivo
esPC :: TipoDispositivo -> Bool
esPC PC = True
esPC td = False

esNotebook :: TipoDispositivo -> Bool
esNotebook Notebook = True
esNotebook td = False

esAireAcond :: TipoDispositivo -> Bool
esAireAcond AireAcond = True
esAireAcond td = False


{-
2. Definir las siguientes funciones 
-}

-- Dada una Red, devuelve el consumo total del los dispositivos conectados a ella
consumoTotal :: Red -> Consumo
consumoTotal (Zapatilla a ds) = consumoDeDispositivos ds
consumoTotal (Conexion a ds ri rd)
  = (consumoDeDispositivos ds) + (consumoTotal ri) + (consumoTotal rd)

consumoDeDispositivos :: [Dispositivo] -> Consumo
consumoDeDispositivos [] = 0
consumoDeDispositivos (d:ds) = (consumo d) + (consumoDeDispositivos ds)

-- Dado un tipo de dispositivo y un consumo, devolver todos los dispositivos de la red de ese tipo
-- que tengan un consumo mayor al dado
dispositivosQueConsumenMasQue :: TipoDispositivo -> Consumo -> Red -> [Dispositivo]
dispositivosQueConsumenMasQue t c (Zapatilla a ds)
  = filtrarDispositivosConTipoYConsumo t c ds
dispositivosQueConsumenMasQue t c (Conexion a ds ri rd)
  = (filtrarDispositivosConTipoYConsumo t c ds)
  ++ (dispositivosQueConsumenMasQue t c ri)
  ++ (dispositivosQueConsumenMasQue t c rd)

filtrarDispositivosConTipoYConsumo :: TipoDispositivo -> Consumo -> [Dispositivo] -> [Dispositivo]
filtrarDispositivosConTipoYConsumo t c [] = []
filtrarDispositivosConTipoYConsumo t c (d:ds) =
  if (tipo d == t) && (consumo d > c)
    then d : (filtrarDispositivosConTipoYConsumo t c ds)
    else filtrarDispositivosConTipoYConsumo t c ds

-- Dada una red, quita todos los aires acondicionados de la misma
desconectarACs:: Red -> Red
desconectarACs (Zapatilla a ds) = Zapatilla a (quitarACs ds)
desconectarACs (Conexion a ds ri rd)
  = Conexion a (quitarACs ds) (desconectarACs ri) (desconectarACs rd)

quitarACs :: [Dispositivo] -> [Dispositivo]
quitarACs [] = []
quitarACs (d:ds) =
  if tipo d == AireAcond
    then quitarACs ds
    else d : (quitarACs ds)

{-
3. Uno más intersante:
Supongamos que la red funciona a 220V y tenemos conectados tres dispositivos, cuyo consumo
maximo es de 2000W. Entonces, el cable al que estan conectados deberia ser capaz de
transportar al menos 9.09A.
Verificar que la red pueda transportar todo el consumo sin prenderse fuego! 
-}

redAguanta :: Red -> Bool
redAguanta (Zapatilla a ds) = amperajeSuficiente a (consumoDeDispositivos ds)
redAguanta (Conexion a ds ri rd)
  = (amperajeSuficiente a (consumoTotal (Conexion a ds ri rd)))
  && (redAguanta ri)
  && (redAguanta rd)

-- potencia(W) = tension(V) x intensidad(A)
-- o sea
-- consumo(W) = voltaje(V) x amperaje(A)
amperajeSuficiente :: Amperaje -> Consumo -> Bool
amperajeSuficiente a c = a >= (c / 220)


---------
-- mias
---------

-- genera una red a partir de una lista de listas de dispositivos
genRed :: [[Dispositivo]] -> Red
genRed dss =
  if (length dss < 1) || (((length dss) `mod` 2) == 0)
    then error "cantidad nodos debe ser impar y mayor que 0"
    else list2red (tail dss) (Zapatilla (head (genDNums 2 6)) (head dss))

list2red :: [[Dispositivo]] -> Red -> Red
list2red [] r = r
list2red (ds1:(ds2:dss)) r = list2red dss (agregarRal ds1 ds2 r)

agregarRal :: [Dispositivo] -> [Dispositivo] -> Red -> Red
agregarRal ds1 ds2 r = agregarEnPos (genDirs (sizeR r)) ds1 ds2 r

agregarEnPos :: [Char] -> [Dispositivo] -> [Dispositivo] -> Red -> Red
agregarEnPos ds ds1 ds2 (Zapatilla a bs)
  = Conexion a bs (Zapatilla (head (genDNums 2 6)) ds1) (Zapatilla (head (genDNums 2 6)) ds2)
agregarEnPos [] ds1 ds2 (Conexion a bs ri rd) = error "ubicacion ya ocupada"
agregarEnPos (d:ds) ds1 ds2 (Conexion a bs ri rd) =
  if (d == 'i')
    then Conexion a bs (agregarEnPos ds ds1 ds2 ri) rd
    else Conexion a bs ri (agregarEnPos ds ds1 ds2 rd)

genDirs :: Int -> [Char]
genDirs n = map int2dir (nums n)

int2dir :: Int -> Char
int2dir i =
  if (i < 0)
    then 'i'
    else 'd'

-- cant de nodos del rio
sizeR :: Red -> Int
sizeR (Zapatilla a bs) = 1
sizeR (Conexion a bs ri rd) = 1 + sizeR ri + sizeR rd

-- genera una lista de N [Dispositivo] (de long entre 0 y 3)
genNDispositivos :: Int -> [[Dispositivo]]
genNDispositivos 0 = []
genNDispositivos n = (genDispositivos (head (genNums 2 3))) : genNDispositivos (n - 1)

-- genera N dispositivos
genDispositivos :: Int -> [Dispositivo]
genDispositivos n = map int2Dispositivo (genNums n 20)

int2Dispositivo :: Int -> Dispositivo
int2Dispositivo n =
  if (n < 2)
    then Dis PC (200 + (head (genDNums 2 200))) -- [200, 400], avg 300
    else
      if (n < 20)
        then Dis Notebook (35 + (head (genDNums 2 30))) -- [35, 65], avg 50
        else Dis AireAcond (500 + (head (genDNums 2 500))) -- [500, 1000], avg 750

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]

-- genera N1 numeros aleatorios entre 0 y N2 (repeticiones permitidas)
genNums :: Int -> Int -> [Int]
genNums n1 n2 = take n1 $ randomRs (0, n2) (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]

-- genera N1 reales aleatorios entre 0 y N2 (repeticiones permitidas)
genDNums :: Int -> Double -> [Double]
genDNums n1 n2 = take n1 $ randomRs (0, n2) (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Double]


-----------------------------------------------------
-- impresion de redes
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Red -> IO ()
prettyprint t = putStrLn (unlines (prettyprint_helper t))
-- unlines concats a list with newlines

prettyprint_helper :: Red -> [String]
prettyprint_helper (Zapatilla a ds) = ["z " ++ (rounded_dbl a) ++ " " ++ (rounded_devs ds)]
prettyprint_helper (Conexion a ds ri rd)
  = ("c " ++ (rounded_dbl a) ++ " " ++ (rounded_devs ds)) : (prettyprint_subtree ri rd)

rounded_dbl :: Double -> String
rounded_dbl d = printf "%.2f" d :: String

rounded_devs :: [Dispositivo] -> String
rounded_devs ds = "[" ++ (rounded_devs_r ds) ++ "]"

rounded_devs_r :: [Dispositivo] -> String
rounded_devs_r [] = ""
rounded_devs_r (d:[]) = rounded_dev d
rounded_devs_r (d:ds) = (rounded_dev d) ++ "," ++ (rounded_devs_r ds)

rounded_dev :: Dispositivo -> String
rounded_dev (Dis t c) = "d " ++ (show t) ++ " " ++ (rounded_dbl c)

prettyprint_subtree :: Red -> Red -> [String]
prettyprint_subtree ri rd
  = ((pad "├──" "│  ") (prettyprint_helper rd))
  ++ ((pad "╰──" "   ") (prettyprint_helper ri))

pad :: String -> String -> [String] -> [String]
pad first rest = zipWith (++) (first : repeat rest)
