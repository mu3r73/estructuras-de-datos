module G04_p02_queue_e02_queue_usando_listas (
  Queue, emptyQ, isEmptyQ, queue, firstQ, dequeue, list2queue
) where

data Queue a =
  Q [a]
  deriving (Show)

-- crea una cola vacia
emptyQ :: Queue a
emptyQ = Q []

-- dada una cola, indica si la cola esta vacia
isEmptyQ :: Queue a -> Bool
isEmptyQ (Q es) = esVacia es

esVacia :: [a] -> Bool -- isNil
esVacia [] = True
esVacia es = False

-- dados un elemento y una cola, agrega ese elemento a la cola
queue :: a -> Queue a -> Queue a
queue e (Q es) = Q (es ++ [e])

-- dada una cola, devuelve su primer elemento
firstQ :: Queue a -> a
firstQ (Q es) = primero es

primero :: [a] -> a -- head
primero [] = error "lista vacia"
primero (x:xs) = x

-- dada una cola, la devuelve sin su primer elemento
dequeue :: Queue a -> Queue a
dequeue (Q es) = Q (sinPrimero es)

sinPrimero :: [a] -> [a] -- tail
sinPrimero [] = error "lista vacia"
sinPrimero (x:xs) = xs


-- mias

list2queue :: [a] -> Queue a
list2queue [] = emptyQ
list2queue xs = queue (last xs) (list2queue (init xs))
