import G04_p03_stack_e02_stack_usando_listas

-- datos para tests

l = [1..5]

expr1 :: String
expr1 = "(2 + 3) * 2" -- V

expr2 :: String
expr2 = "2 + 3) * 2" -- F

expr3 :: String
expr3 = "(2 + 3 * 2" -- F

expr4 :: String
expr4 = "(2 + (3) * 2" -- F

expr5 :: String
expr5 = "2 + 3 * 2" -- V

-- dada una lista, devuelve una pila,
-- sin alterar el orden de los elementos
apilar :: [a] -> Stack a
apilar [] = emptyS
apilar xs = push (last xs) (apilar (init xs))

{-
Toma un string que representa una expresion aritmetica,
por ejemplo "(2 + 3) ∗ 2",
y verifica que la cantidad de parentesis que abren
se corresponda con los que cierran.
Para hacer esto utilice una stack.
Cada vez que encuentra un parentesis que abre, lo apila.
Si encuentra un parentesis que cierra, desapila un elemento.
Si al terminar de recorrer el string se desapilaron
tantos elementos como los que se apilaron,
ni mas ni menos, entonces los parentesis estan balaceados.
-}
balanceado :: String -> Bool
balanceado cs = estaBalanceado cs emptyS

estaBalanceado :: String -> Stack Char -> Bool
estaBalanceado [] s = isEmptyS s
estaBalanceado ('(':cs) s = estaBalanceado cs (push '(' s)
estaBalanceado (')':cs) s =
  if (isEmptyS s)
    then False
    else estaBalanceado cs (pop s)
estaBalanceado (c:cs) s = estaBalanceado cs s
