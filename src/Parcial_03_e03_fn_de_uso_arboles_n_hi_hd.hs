import Parcial_03_e02_multiset_v1_usando_lista_simple
import G03_p01_arboles_binarios_n_hi_hd

-- datos para tests

tint1 :: Tree Int
tint1 = treeFromList agregarTbal [1 .. 9]

tint2 :: Tree Int
tint2 = treeFromList agregarTbal [5 .. 14]


-- dados dos arboles, devuelve un multiconjunto con los elementos que
-- ambos arboles tienen en comun, respetando la cantidad de copias
-- de cada uno
intersectTree :: Eq a => Tree a -> Tree a -> MultiSet a
intersectTree t1 t2 = interMS (tree2ms t1) (tree2ms t2)

tree2ms :: Eq a => Tree a -> MultiSet a
tree2ms EmptyT = emptyMS
tree2ms (NodeT e ti td) = addMS e (unionMS (tree2ms ti) (tree2ms td))
