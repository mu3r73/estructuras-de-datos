module G02_p01_tipos_def_por_usr_e03_pokemon (
  Pokemon(Poke), TipoDePokemon(Agua, Fuego, Planta), Energia,
  Entrenador(Entr), Nombre, esDeTipo, elementoGanador, leGanaA,
  capturarPokemon, cantidadDePokemons, cantidadDePokemonsDeTipo,
  lePuedeGanar, puedenPelear, esExperto, fiestaPokemon
) where

-- tipos de datos Pokemon, como un TipoDePokemon (agua, fuego o planta)
-- y un porcentaje de energia
data TipoDePokemon
  = Agua
  | Fuego
  | Planta
  deriving (Eq, Show)

type Energia = Int

data Pokemon =
  Poke TipoDePokemon
       Energia
  deriving (Show)

-- observadores
tipo :: Pokemon -> TipoDePokemon
tipo (Poke t e) = t

energia :: Pokemon -> Energia
energia (Poke t e) = e

-- devuelve True si el pokemon es del tipo indicado, y False en cc
esDeTipo :: TipoDePokemon -> Pokemon -> Bool
esDeTipo t (Poke t1 e1) = (t1 == t)

esDeTipo' :: TipoDePokemon -> Pokemon -> Bool
esDeTipo' t p = (t == (tipo p))

-- tipo de datos Entrenador, como un nombre y una lista de Pokemon
type Nombre = String

data Entrenador =
  Entr Nombre
       [Pokemon]
  deriving (Show)

-- observadores
nombre :: Entrenador -> Nombre
nombre (Entr n ps) = n

pokemons :: Entrenador -> [Pokemon]
pokemons (Entr n ps) = ps

-- datos para tests
p1 = Poke Agua 15
p2 = Poke Fuego 20
p3 = Poke Planta 18
p4 = Poke Fuego 14
p5 = Poke Agua 13
p6 = Poke Planta 17
p7 = Poke Planta 19
p8 = Poke Fuego 16
p9 = Poke Fuego 12

e1 = Entr "Red" [p1, p2, p3]
e2 = Entr "Blue" [p4, p5, p6]
e3 = Entr "Yellow" [p7, p8]
e4 = Entr "Ash" [p9]

-- dado un TipoDePokemon, devuelve el elemento que le gana a ese
-- agua le gana a fuego, fuego a planta y planta a agua
elementoGanador :: TipoDePokemon -> TipoDePokemon
elementoGanador Agua = Planta
elementoGanador Fuego = Agua
elementoGanador Planta = Fuego

-- dados dos pokemon, indica si el primero le puede ganar al segundo
-- se considera que gana si su elemento es opuesto al del otro pokemon
-- si poseen el mismo elemento, se considera que no gana
leGanaA :: Pokemon -> Pokemon -> Bool
leGanaA (Poke t1 e1) (Poke t2 e2) = t1 == (elementoGanador t2)

leGanaA' :: Pokemon -> Pokemon -> Bool
leGanaA' p1 p2 = (tipo p1) == (elementoGanador (tipo p2))

-- agrega un pokemon a la lista de pokemon del entrenador
capturarPokemon :: Pokemon -> Entrenador -> Entrenador
capturarPokemon p (Entr n ps) = Entr n (p:ps)

capturarPokemon' :: Pokemon -> Entrenador -> Entrenador
capturarPokemon' p e = Entr (nombre e) (p : (pokemons e))

-- devuelve la cantidad de pokemons que posee el entrenador
cantidadDePokemons :: Entrenador -> Int
cantidadDePokemons (Entr n ps) = length ps

cantidadDePokemons' :: Entrenador -> Int
cantidadDePokemons' e = length (pokemons e)

-- devuelve la cantidad de pokemons de determinado tipo
-- que posee el entrenador
cantidadDePokemonsDeTipo :: TipoDePokemon -> Entrenador -> Int
cantidadDePokemonsDeTipo t (Entr n ps) = length (filtrarPokemonsDeTipo t ps)

filtrarPokemonsDeTipo :: TipoDePokemon -> [Pokemon] -> [Pokemon]
filtrarPokemonsDeTipo t [] = []
filtrarPokemonsDeTipo t (p:ps) =
  if (esDeTipo t p)
    then p : (filtrarPokemonsDeTipo t ps)
    else filtrarPokemonsDeTipo t ps
{-
cantidadDePokemonsDeTipo :: TipoDePokemon -> Entrenador -> Int
cantidadDePokemonsDeTipo t (Entr n ps) = length (filter (esDeTipo t) ps)
-}

-- dados un entrenador y un pokemon, devuelve True si el entrenador
-- posee un pokemon que le gane ese pokemon
lePuedeGanar :: Entrenador -> Pokemon -> Bool
lePuedeGanar (Entr n ps) p = algunoLeGanaA p ps

algunoLeGanaA :: Pokemon -> [Pokemon] -> Bool
algunoLeGanaA p0 [] = False
algunoLeGanaA p0 (p:ps) = (leGanaA p p0) || (algunoLeGanaA p0 ps)
{-
lePuedeGanar :: Entrenador -> Pokemon -> Bool
lePuedeGanar (Entr n ps) p = any (leGanaA p) ps
-}

-- dados un tipo de pokemon y dos entrenadores, devuelve True
-- si ambos entrenadores tiene al menos un pokemon de ese tipo
-- y que tenga energia para pelear
puedenPelear :: TipoDePokemon -> Entrenador -> Entrenador -> Bool
puedenPelear t e1 e2 = (puedePelear t e1) && (puedePelear t e2)

puedePelear :: TipoDePokemon -> Entrenador -> Bool
puedePelear t (Entr n []) = False
puedePelear t (Entr n ps) = algunPokemonOK t ps

algunPokemonOK :: TipoDePokemon -> [Pokemon] -> Bool
algunPokemonOK t [] = False
algunPokemonOK t (p:ps) = (pokemonOK t p) || (algunPokemonOK t ps)

pokemonOK :: TipoDePokemon -> Pokemon -> Bool
pokemonOK t (Poke t1 e1) = (t == t1) && (e1 > 0)
{-
puedenPelear :: TipoDePokemon -> Entrenador -> Entrenador -> Bool
puedenPelear t e1 e2 = (puedePelear t e1) && (puedePelear t e2)

puedePelear TipoDePokemon -> Entrenador -> Bool
puedePelear t (Entr n ps) = any (pokemonOK t) ps

pokemonOK TipoDePokemon -> Pokemon -> Bool
pokemonOK t (Poke t1 e1) = (t == t1) && (e1 > 0)
-}

-- dado un entrenador, devuelve True si ese entrenador
-- posee al menos un pokemon de cada tipo posible
esExperto :: Entrenador -> Bool
esExperto e =
  (algunPokemonDeTipo Agua e) &&
  (algunPokemonDeTipo Fuego e) &&
  (algunPokemonDeTipo Planta e)

algunPokemonDeTipo :: TipoDePokemon -> Entrenador -> Bool
algunPokemonDeTipo t e = (cantidadDePokemonsDeTipo t e) > 0

-- dada una lista de entrenadores, devuelve una lista
-- con todos los pokemon de cada entrenador
fiestaPokemon :: [Entrenador] -> [Pokemon]
fiestaPokemon [] = []
fiestaPokemon ((Entr n ps):es) = ps ++ fiestaPokemon es
