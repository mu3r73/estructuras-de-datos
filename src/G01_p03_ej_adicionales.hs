module G01_p03_ej_adicionales (
  particionPorSigno, particionPorParidad, subtails, agrupar, esPrefijo, esSufijo
) where

import G01_p02_recursion_e01_sobre_listas

-- dada una lista xs de enteros, devuelve una tupla de listas,
-- donde la primera componente contiene todos los numeros positivos de xs
-- y la segunda todos los numeros negativos de xs
-- ¿conviene utilizar recursion?
-- considere utilizar funciones auxiliares
particionPorSigno :: [Int] -> ([Int], [Int])
particionPorSigno xs = (positivos xs, negativos xs)

positivos :: [Int] -> [Int]
positivos [] = []
positivos (x:xs) =
  if x > 0
    then x : (positivos xs)
    else positivos xs

negativos :: [Int] -> [Int]
negativos [] = []
negativos (x:xs) =
  if x < 0
    then x : (negativos xs)
    else negativos xs

-- dada una lista xs de enteros, devuelve una tupla de listas,
-- donde la primera componente contiene todos los numeros pares de xs
-- y la segunda todos los numeros impares de xs
-- ¿conviene utilizar recursion?
-- considere utilizar funciones auxiliares
particionPorParidad :: [Int] -> ([Int], [Int])
particionPorParidad xs = (pares xs, impares xs)

pares :: [Int] -> [Int]
pares [] = []
pares (x:xs) =
  if esPar x
    then x : (pares xs)
    else pares xs

impares :: [Int] -> [Int]
impares [] = []
impares (x:xs) =
  if esPar x
    then impares xs
    else x : (impares xs)

esPar :: Int -> Bool
esPar n = (n `mod` 2 ) == 0

-- dada una lista, devuelve cada sublista resultante de aplicar tail en cada paso
-- ejemplo:
-- subtails "abc" == ["abc", "bc", "c",""]
subtails :: [a] -> [[a]]
subtails [] = [[]]
subtails (x:xs) = (x:xs) : subtails xs

-- dada una lista xs, devuelve una lista de listas
-- donde cada sublista contiene elementos contiguos iguales de xs
-- ejemplo:
-- agrupar "AABCCC" = ["AA","B","CC"]
agrupar :: Eq a => [a] -> [[a]]
agrupar [] = []
agrupar (x:xs) = agru x [] (x:xs)

agru :: Eq a => a -> [a] -> [a] -> [[a]]
agru e es [] = [es]
agru e es (x:xs) =
  if x == e
    then agru e (e:es) xs
    else es : agru x [x] xs

-- v de Roman:
agruparR :: Eq a => [a] -> [[a]]
agruparR  xs = agruparR' xs []

agruparR' :: Eq a => [a] -> [a] -> [[a]]
agruparR' [] acc = [acc]
agruparR' (x:xs) [] = agruparR' xs [x]
agruparR' (x:xs) (a:acc) =
  if (x == a)
    then agruparR' xs (x:a:acc)
    else (a:acc) : agruparR' xs [x]

-- v de Viso:
agruparV :: Eq a => [a] -> [[a]]
agruparV [] = []
agruparV [x] = [[x]]
agruparV (x:(y:xs)) =
  if x == y
    then agregarAlPrimero x (agruparV (y : xs))
    else [x] : (agruparV (y : xs))

agregarAlPrimero :: a -> [[a]] -> [[a]]
agregarAlPrimero x [] = [[x]]
agregarAlPrimero x (xs:xss) = (x : xs) : xss

-- devuelve True si la primera lista es prefijo de la segunda
esPrefijo :: Eq a => [a] -> [a] -> Bool
esPrefijo [] ys = True
esPrefijo xs [] = False
esPrefijo (x:xs) (y:ys) =
  if x == y
    then esPrefijo xs ys
    else False

-- v de Román
esPrefijoR :: Eq a => [a] -> [a] -> Bool
esPrefijoR [] ys = True
esPrefijoR xs [] = False
esPrefijoR (x:xs) (y:ys) = (x == y) && (esPrefijoR xs ys)

-- devuelve True si la primera lista es sufijo de la segunda
esSufijo :: Eq a => [a] -> [a] -> Bool
esSufijo xs ys = esPrefijo (reversa xs) (reversa ys)

-- v de Ramiro
esSufijoR :: Eq a => [a] -> [a] -> Bool
esSufijoR [] xs = True
esSufijoR xs [] = False
esSufijoR xs ys =
  if (last' xs == last' ys)
    then esSufijoR (init' xs) (init' ys)
    else False

init' :: [a] -> [a]
init' [] = error "lista vacia"
init' (x:[]) = []
init' (x:xs) = x : (init' xs)

last' :: [a] -> a
last' [] = error "lista vacia"
last' (x:[]) = x
last' (x:xs) = last' xs

-- propuesto en clase, no es parte de la guía
-- se resuelve en forma similar a agruparV
-- escribir la funcion partes de una lista, donde:
-- partes [1,2,3] = [[], [1], [2], [3], [1,2], [1,3], [2,3], [1,2,3]]
-- sin importar el orden de los elementos
partes :: [a] -> [[a]]
partes [] = [[]]
partes (x:xs) = (agregarATodos x (partes xs)) ++ (partes xs)

agregarATodos :: a -> [[a]] -> [[a]]
agregarATodos e [] = []
agregarATodos e (xs:xss) = (e : xs) : (agregarATodos e xss)
