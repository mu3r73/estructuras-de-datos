import Parcial_04_persona
import Parcial_04_sentido
import Parcial_04_e02_subte
import Parcial_04_e03_estacion

-- Ejercicio 1

-- Como usuario del tipo abstracto Subte, definir las siguientes
-- funciones:

-- mueve el tren hasta la estación terminal en el sentido indicado
irATerminal :: Sentido -> Subte -> Subte
irATerminal x s =
  if puedeMover x s
    then irATerminal x (mover x s)
    else s

-- retorna la cantidad de estaciones del subte en las que no hay
-- personas de ningún tipo
nroEstacionesVacias :: Subte -> Int
nroEstacionesVacias s = nroEstacionesVaciasRec (irATerminal Atras s) 0

nroEstacionesVaciasRec :: Subte -> Int -> Int
nroEstacionesVaciasRec s n =
  if puedeMover Adelante s
    then nroEstacionesVaciasRec (mover Adelante s)
                                (n + contarSiEsVacia (estacionActual s))
    else n + contarSiEsVacia (estacionActual s)

contarSiEsVacia :: Estacion -> Int
contarSiEsVacia e =
  if hayPersonasDeAlgunTipo e
    then 0
    else 1

hayPersonasDeAlgunTipo :: Estacion -> Bool
hayPersonasDeAlgunTipo e
  = (hayPersonas Hombre e)
  || (hayPersonas Mujer e)
  || (hayPersonas Ninho e)

-- indica si hay niños en alguna estación del subte
hayNinhos :: Subte -> Bool
hayNinhos s = hayNinhosRec (irATerminal Atras s)

hayNinhosRec :: Subte -> Bool
hayNinhosRec s =
  if puedeMover Adelante s
    then hayPersonas Ninho (estacionActual s)
      || hayNinhosRec (mover Adelante s)
    else hayPersonas Ninho (estacionActual s)
