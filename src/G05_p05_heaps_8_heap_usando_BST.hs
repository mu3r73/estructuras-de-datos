module G05_p05_heaps_8_heap_usando_BST (
  Heap, emptyH, isEmptyH, insertH, findMin, deleteMin, splitMin,
  heapFromList,
  G05_p05_heaps_8_heap_usando_BST.prettyprint
) where

import G05_p04_arboles_de_busqueda_5_3_AVL_n_hi_hd_guardando_h as BST

import System.Random
import Data.Time.Clock.POSIX
import System.IO.Unsafe


data Heap a = H (Tree a)
  deriving (Show)

-- invariantes:
-- H (Tree a) es un (min-)Heap si:
-- ????????????????

-- datos para tests
{-
nota: para mapas aleatorios, usar mapFromList (shuffle <lista>),
      ej: heapFromList (shuffle ['a', 'c', 'd', 'f', 'h', 'k', 'm']),
      y asignar expresion resultante a una var
-}

hchar1 :: Heap Char
hchar1 = heapFromList (shuffle ['a', 'c', 'd', 'f', 'h', 'k', 'm'])

hint1 :: Heap Int
hint1 = heapFromList (shuffle [1 .. 7])


-- retorna un heap vacio
emptyH :: Heap a
emptyH = H emptyBST

-- indica si el heap esta vacio
isEmptyH :: Heap a -> Bool
isEmptyH (H t) = isEmptyBST t

-- agrega un nuevo elemento e al heap
insertH :: Ord a => a -> Heap a -> Heap a
insertH e (H t) = H (insertBST e t)

-- retorna el elemento minimo del heap
findMin :: Ord a => Heap a -> a -- Parcial en emptyH
findMin h = fst (splitMin h)

-- borra el minimo elemento del heap
deleteMin :: Ord a => Heap a -> Heap a -- Parcial en emptyH
deleteMin h = snd (splitMin h)

-- retorna un par con el min elem de un heap y
-- el heap resultante de borrar ese elem
splitMin :: Ord a => Heap a -> (a, Heap a) -- Parcial en emptyH
splitMin (H t) = (min, H t_sin_min)
  where
    (min, t_sin_min) = splitminBST t


-- mias

-- genera un heap a partir de una lista
heapFromList :: Ord a => [a] -> Heap a
heapFromList [] = emptyH
heapFromList (x:xs) = insertH x (heapFromList xs)

-- hace shuffle de una lista
shuffle :: [a] -> [a]
shuffle xs = list2shuffle (genHorT (length xs)) xs []

list2shuffle :: [Char] -> [a] -> [a] -> [a]
list2shuffle ds [] ys = ys
list2shuffle (d:ds) (x:xs) ys =
  if d == 'h'
    then list2shuffle ds xs (x : ys)
    else list2shuffle ds xs (ys ++ [x])

genHorT :: Int -> [Char]
genHorT n = map int2ht (nums n)

int2ht :: Int -> Char
int2ht i =
  if i < 0
    then 'h'
    else 't'

-- genera N numeros aleatorios entre 0 y 9 (repeticiones permitidas)
genNums :: Int -> [Int]
genNums n = map (`mod` 10) (nums n)

-- genera N enteros aleatorios
nums :: Int -> [Int]
nums n = take n $ randoms (mkStdGen (round ((unsafePerformIO getPOSIXTime) * 1000000000) :: Int)) :: [Int]


-----------------------------------------------------
-- impresion de arboles
-- adaptado de: http://stackoverflow.com/a/19442407
-----------------------------------------------------

prettyprint :: Show a => Heap a -> IO ()
prettyprint (H t) = BST.prettyprint t
