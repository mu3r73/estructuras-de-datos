import G03_p01_arboles_binarios

-- datos para tests

tint1 :: Tree Int
tint1 = treeFromList agregarTal (shuffle [1..6])

tint2 :: Tree Int
tint2 = treeFromList agregarTal (shuffle [4..9])

-- dada una lista, quita su ultimo elemento
-- orden N
init' :: [a] -> [Maybe a]
init' [] = [Nothing]
init' (x:[]) = []
init' (x:xs) = (Just x) : (init' xs)

-- dada una lista, devuelve su ultimo elemento
-- orden N
last' :: [a] -> Maybe a
last' [] = Nothing
last' (x:[]) = Just x
last' (x:xs) = last' xs

-- dados un elemento y una lista, devuelve la posicion de la lista
-- en la que se encuentra dicho elemento
-- orden N
indiceDe :: Eq a => a -> [a] -> Maybe Int
indiceDe e xs = posicionEnLista e xs 1

posicionEnLista :: Eq a => a -> [a] -> Int -> Maybe Int
posicionEnLista e [] n = Nothing
posicionEnLista e (x:xs) n =
  if (e == x)
    then Just n
    else posicionEnLista e xs (n + 1)

-- dadas una lista de pares (clave, valor) y una clave, devuelve
-- el valor asociado a la clave
-- orden N
valorParaClave :: Eq k => [(k, v)] -> k -> Maybe v
valorParaClave [] k0 = Nothing
valorParaClave ((k, v) : ps) k0 =
  if (k == k0)
    then Just v
    else valorParaClave ps k0

-- dada una lista de elementos, devuelve el maximo
-- orden N
maximum' :: Ord a => [a] -> Maybe a
maximum' [] = Nothing
maximum' xs = Just (maxRec xs)

maxRec :: Ord a => [a] -> a
maxRec (x:[]) = x
maxRec (x:xs) = max' x (maxRec xs)

max' :: Ord a => a -> a -> a
max' x y =
  if x > y
    then x
    else y

-- dado un arbol, devuelve su elemento minimo
-- precondicion: el arbol no es vacio
-- orden N
minT :: Ord a => Tree a -> Maybe a
minT EmptyT = Nothing
minT t = Just (minTrec t)

minTrec :: Ord a => Tree a -> a
minTrec (NodeT EmptyT e EmptyT) = e -- hoja
minTrec (NodeT ti e EmptyT) = min' e (minTrec ti)
minTrec (NodeT EmptyT e td) = min' e (minTrec td)
minTrec (NodeT ti e td) = min' e (min' (minTrec ti) (minTrec td))

min' :: Ord a => a -> a -> a
min' x y =
  if x < y
    then x
    else y

-- dada una lista de maybes, retorna los valores de los elementos
-- que sean Just,
-- pero si se encuentra un Nothing, el valor resultante es Nothing
fromJusts :: [Maybe a] -> Maybe [a] 
fromJusts ms =
  if hasNothings ms
    then Nothing
    else Just (fromJustL ms)

hasNothings :: [Maybe a] -> Bool
hasNothings [] = False
hasNothings (Nothing : ms) = True
hasNothings (m:ms) = hasNothings ms

-- precondicion: la lista no contiene Nothing
fromJustL :: [Maybe a] -> [a]
fromJustL [] = []
fromJustL (Nothing : ms) = error "lista contiene Nothing"
fromJustL ((Just x) : ms) = x : (fromJustL ms)
